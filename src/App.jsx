import "./App.scss";
import AppRouter from "@/routers/AppRouter.jsx";
import { LocalizationProvider } from "@mui/x-date-pickers";
import { AdapterMoment } from "@mui/x-date-pickers/AdapterMoment";
import store from "@/Store";
import { Provider } from "react-redux";
import Init from "@/Components/Init";
import ManuallyProvideCustomColor from "@/Components/ManuallyProvideCustomColor";

import BasicInfoProvider from "@/Components/BasicInfoContext/BasicInfoContext";

function App() {
  return (
    <>
      <Provider store={store}>
        <LocalizationProvider dateAdapter={AdapterMoment}>
          <Init>
            <BasicInfoProvider>
              <ManuallyProvideCustomColor>
                <AppRouter />
              </ManuallyProvideCustomColor>
            </BasicInfoProvider>
          </Init>
        </LocalizationProvider>
      </Provider>
    </>
  );
}

export default App;
