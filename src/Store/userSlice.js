import { createSlice } from "@reduxjs/toolkit";
import { refreshToken } from "@/Services/User";
import { getCookie, RemoveCookie, setToken } from "@/Utilities/cookie";
import { jwtDecode } from "jwt-decode";

var AuthTokenName = "jwt";
var RefreshTokenName = "refresh";

const InitialState = {
  isLoggedin: false,
  token: "",
  refreshToken: "",
  name: "",
  membership_number: undefined,
  group: []
};

export const userSlice = createSlice({
  name: "user",
  initialState: InitialState,
  reducers: {
    setUserByToken: state => {
      let token = getCookie(AuthTokenName);
      // 沒有token或是cookie時間到了
      if (!token) {
        return;
      }

      const jwtUser = jwtDecode(token);

      // 再檢查一次時間，可能有不同步問題
      const currentDate = new Date();
      const currentTimestamp = currentDate.getTime();
      let exp = jwtUser.exp;

      if (exp * 1000 < currentTimestamp) {
        return;
      }

      state.name = jwtUser["username"];
      state.token = token;
      state.refreshToken = getCookie(RefreshTokenName);
      state.membership_number = jwtUser["user_membership_number"];
      state.group = jwtUser["group"].split(",");
      state.isLoggedin = true;
    },
    login: (state, action) => {
      const data = action.payload;
      state.token = data.access;
      state.refreshToken = data.refresh;
      setToken(data.access, AuthTokenName);
      setToken(data.refresh, RefreshTokenName);
    },
    logout: state => {
      RemoveCookie(AuthTokenName);
      state = Object.assign(InitialState, {});
      return state;
    },
    refresh: state => {
      let token = getCookie(RefreshTokenName);

      let data = { refresh: token };
      refreshToken(data).then(res => {
        setToken(res.data.access, AuthTokenName);
        setUserByToken(state);
      });
    }
  }
});

export const { setUserByToken, login, logout } = userSlice.actions;

export default userSlice.reducer;
