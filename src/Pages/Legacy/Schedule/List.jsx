import { useState, useEffect } from "react";
import {
  anonymousGetLegacyScheduleList,
  anonymousGetLegacyScheduleListByLeaderId,
  anonymousGetLegacyScheduleListByMemberId,
  getLegacyScheduleList,
  getLegacyScheduleListByLeaderId,
  getLegacyScheduleListByMemberId
} from "@/Services/Legacy";

import PropTypes from "prop-types";

import "../css/table.css";

import { useSearchParams } from "react-router-dom";
import { UserIsLoggedIn } from "@/Components/Authentication/UserStatus.jsx";
import { useSelector } from "react-redux";

function get_static_root() {
  return import.meta.env.VITE_STATIC_ROOT;
}

function get_member_photo_url(member_id) {
  return get_static_root() + "/image/member/" + member_id + ".jpg";
}

function get_leader_history_url(memberno) {
  return "?leader_id=" + memberno;
}

function get_member_history_url(memberno) {
  return "?member_id=" + memberno;
}

async function getLeaderScheduleListBasedOnUserStatus(size, page, leaderId) {
  if (UserIsLoggedIn()) {
    return getLegacyScheduleListByLeaderId(size, page, leaderId);
  } else {
    return anonymousGetLegacyScheduleListByLeaderId(size, page, leaderId);
  }
}

async function getScheduleListBasedOnUserStatus(size, page, memberId) {
  if (UserIsLoggedIn()) {
    return getLegacyScheduleListByMemberId(size, page, memberId);
  } else {
    return anonymousGetLegacyScheduleListByMemberId(size, page, memberId);
  }
}

function ScheduleDate(props) {
  ScheduleDate.propTypes = {
    date_start: PropTypes.string,
    day: PropTypes.number
  };
  const { date_start, day } = props;
  let date_end = new Date(date_start);
  date_end.setDate(date_end.getDate() + day - 1);
  // convert date_end to yyyy-mm-dd
  date_end = date_end.toISOString().slice(0, 10);
  return (
    <>
      {date_start.substring(5)}
      <br />
      ~
      <br />
      {date_end.substring(5)}
    </>
  );
}

const MemberColumn = props => {
  MemberColumn.propTypes = { member: PropTypes.object };
  const { member } = props;
  const [show_member_name, setShowMemberName] = useState(false);

  return (
    <div style={{ position: "relative", display: "inline" }}>
      {show_member_name && <div id={"member-name"}>{member["name"]}</div>}
      <img
        height={"30"}
        src={get_member_photo_url(member["memberno"])}
        alt=""
        onMouseOver={() => {
          setShowMemberName(true);
        }}
        onMouseOut={() => {
          setShowMemberName(false);
        }}
        onClick={() => {
          window.location.href = get_member_history_url(member["memberno"]);
        }}
      />
    </div>
  );
};

const LeaderColumn = props => {
  LeaderColumn.propTypes = { member: PropTypes.object };
  const { member } = props;
  return (
    <tr>
      <td>
        <img width={"30"} src={get_member_photo_url(member["memberno"])} alt="Member" />
      </td>
      <td>
        <div className="greytext">
          {member.name}
          {UserIsLoggedIn() && <a href={get_leader_history_url(member["memberno"])}>[帶隊記錄]</a>}
          <br />
          {member.email}
          <br />
          {member.mobile}
        </div>
      </td>
    </tr>
  );
};

function Pagination(props) {
  Pagination.propTypes = {
    first_button_click: PropTypes.func,
    prev_button_click: PropTypes.func,
    page_button_click: PropTypes.func,
    next_button_click: PropTypes.func,
    last_button_click: PropTypes.func,
    size_change: PropTypes.func,
    current_page: PropTypes.number,
    total_pages: PropTypes.number,
    total_count: PropTypes.number
  };

  if (props.total_pages <= 1) {
    return null;
  }

  let page_list = [];
  let start_page = props.current_page - 2 > 0 ? props.current_page - 2 : 1;
  let end_page = props.current_page + 2 < props.total_pages ? props.current_page + 2 : props.total_pages;

  if (end_page - start_page < 4) {
    if (start_page === 1) {
      end_page = props.total_pages > 5 ? 5 : props.total_pages;
    }
    if (end_page === props.total_pages) {
      start_page = props.total_pages - 4 > 0 ? props.total_pages - 4 : 1;
    }
  }

  for (let i = start_page; i <= end_page; i++) {
    page_list.push(i);
  }

  let size_list = [10, 20, 50, 100];

  return (
    <>
      <button onClick={props.first_button_click}>First</button>
      <button onClick={props.prev_button_click}>Prev</button>
      {page_list.map(page => {
        return (
          <button key={page} onClick={props.page_button_click}>
            {page === props.current_page ? <b>{page}</b> : <>{page}</>}
          </button>
        );
      })}
      <button onClick={props.next_button_click}>Next</button>
      <button onClick={props.last_button_click}>Last</button>
      <p>
        Page {props.current_page} of {props.total_pages}, total: {props.total_count} &emsp; 每頁顯示：
        <select
          onChange={event => {
            props.size_change(parseInt(event.target.value));
            props.first_button_click();
          }}
        >
          {size_list.map(size => {
            return (
              <option key={size} value={size}>
                {size}
              </option>
            );
          })}
        </select>
        筆
      </p>
    </>
  );
}

export default function List() {
  const user = useSelector(state => state.user);
  const [schedule_list, setScheduleList] = useState([]);
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(10);
  const [total_count, setTotalCount] = useState(0);
  const [total_pages, setTotalPages] = useState(0);
  const [searchParams] = useSearchParams();
  const [subTitle, setSubTitle] = useState("");

  useEffect(() => {
    // TODO 這裡要重構
    // 有三種情境：搜尋會員記錄、搜尋領隊記錄、不過濾
    // 這三種情境都要考慮登入狀態
    const GetScheduleList = async (size, page) => {
      let result;
      if (searchParams.get("leader_id") !== null) {
        result = await getLeaderScheduleListBasedOnUserStatus(size, page, searchParams.get("leader_id"));
        const leader_name = result.data["schedule_list"][0]["leader_list"].filter(leader => {
          return leader["memberno"] === searchParams.get("leader_id");
        })[0]["name"];
        setSubTitle(leader_name + "帶隊記錄");
      } else if (searchParams.get("member_id") !== null) {
        result = await getScheduleListBasedOnUserStatus(size, page, searchParams.get("member_id"));
        const member_name = result.data["schedule_list"][0]["member_list"].filter(member => {
          return member["memberno"] === searchParams.get("member_id");
        })[0]["name"];
        setSubTitle(member_name + "參加記錄");
      } else {
        if (UserIsLoggedIn()) {
          result = await getLegacyScheduleList(size, page);
        } else {
          result = await anonymousGetLegacyScheduleList(size, page);
        }
      }
      setScheduleList(result.data["schedule_list"]);
      setTotalCount(parseInt(result.data["total_count"]));
      setTotalPages(parseInt(result.data["total_pages"]));
    };

    GetScheduleList(size, page).then(() => {});
  }, [page, searchParams, size]);

  function first_button_click() {
    setPage(1);
  }

  function prev_button_click() {
    setPage(page - 1 > 0 ? page - 1 : 1);
  }

  function page_button_click(event) {
    setPage(parseInt(event.target.innerHTML));
  }

  function next_button_click() {
    setPage(page + 1 < total_pages ? page + 1 : total_pages);
  }

  function last_button_click() {
    setPage(total_pages);
  }

  return (
    <>
      <h1>活動行程 {subTitle}</h1>
      {schedule_list.length === 0 ? (
        <div>Loading...</div>
      ) : (
        <>
          <table className={"mainTable"}>
            <thead>
              <tr className={"title-m"}>
                <th style={{ width: "60px" }}>年度</th>
                <th style={{ width: "60px" }}>日期</th>
                <th style={{ width: "40px" }}>天數</th>
                <th>內容</th>
                <th>簡介</th>
                <th style={{ width: "150px" }}>領隊</th>
                <th style={{ width: "250px" }}>隊員</th>
              </tr>
            </thead>
            <tbody>
              {schedule_list.map(schedule => {
                return (
                  <tr key={schedule.id}>
                    <td>{schedule["date_start"].substring(0, 4)}</td>
                    <td>
                      <ScheduleDate date_start={schedule["date_start"]} day={schedule["day"]}></ScheduleDate>
                    </td>
                    <td>{schedule.day}</td>
                    <td>{schedule.title}</td>
                    <td>{schedule["introduction"]}</td>
                    <td>
                      <table>
                        <tbody>
                          {schedule["leader_list"].map(leader => {
                            return <LeaderColumn key={leader.id} member={leader} />;
                          })}
                        </tbody>
                      </table>
                    </td>
                    <td>
                      隊員人數：{schedule["member_list"].length}
                      <br />
                      {schedule["member_list"].map(member => {
                        return <MemberColumn key={member.id} member={member} />;
                      })}
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
          <Pagination
            first_button_click={first_button_click}
            prev_button_click={prev_button_click}
            page_button_click={page_button_click}
            next_button_click={next_button_click}
            last_button_click={last_button_click}
            size_change={setSize}
            current_page={page}
            total_pages={total_pages}
            total_count={total_count}
          />
        </>
      )}
    </>
  );
}
