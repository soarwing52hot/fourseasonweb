import { useEffect, useState } from "react";
import { getAnnouncementList } from "@/Services/Announcement";
import { useBasicInfo } from "@/Components/BasicInfoContext/BasicInfoContext";
import { useSelector } from "react-redux";
import { Grid, Select, FormControl, MenuItem, InputLabel, Button } from "@mui/material";

import { useNavigate } from "react-router-dom";
import AnnounceCard from "../Components/AnnounceCard";
import usePagination from "@/hooks/usePagination";
import PageButton from "@/Components/PageButton";

export default function AnnouncementList() {
  const [announcementList, setAnnouncementList] = useState([]);
  const [selectedType, setSelectedType] = useState([]);

  const { pages, setNewPages } = usePagination({});
  const { announcementTypes } = useBasicInfo();
  const user = useSelector(state => state.user);
  const history = useNavigate();
  // 先用只要在任一幹部群組就可以發布公告
  const canMakeAnnounce = user?.group.length > 0;

  useEffect(() => {
    getList();
  }, [selectedType]);

  async function getList(page = 1) {
    const res = await getAnnouncementList({ announce_type: selectedType, page: page });
    const data = res.data;
    setAnnouncementList(data.results);
    setNewPages(data);
  }

  function changeType(e) {
    setSelectedType(e.target.value);
  }

  function publish() {
    history("/Announcement/Form/new");
  }

  return (
    <div className="content-area">
      <h1 className="title-bar">公告</h1>
      <Grid container spacing={2}>
        <Grid item xs={8} md={11}>
          <FormControl fullWidth>
            <InputLabel>公告種類</InputLabel>
            <Select value={selectedType} label="公告種類" onChange={changeType}>
              <MenuItem value={""}>全類型</MenuItem>
              {announcementTypes.map(e => (
                <MenuItem key={e} value={e.value}>
                  {e.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        {canMakeAnnounce && (
          <Grid item xs={4} md={1}>
            <Button variant="contained" color="success" onClick={publish}>
              新增公告
            </Button>
          </Grid>
        )}
      </Grid>
      {announcementList.map(e => (
        <AnnounceCard announce={e} key={e.id} />
      ))}
      <PageButton pages={pages} func={getList} />
    </div>
  );
}
