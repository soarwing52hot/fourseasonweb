import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import { getAnnouncement, getAvailableAnnouncementTypes } from "@/Services/Announcement";
import InputForm from "./Componenets/InputForm";

export default function AnnouncementForm() {
  let { id } = useParams();
  const isNew = id == "new";
  const [announcement, setAnnnouncement] = useState({});
  const [announcementTypes, setAnnouncementTypes] = useState([]);
  useEffect(() => {
    _getAvailableAnnouncementTypes();
    if (isNew) {
      return;
    }

    _getAnnouncementDetail(id);
  }, []);

  async function _getAnnouncementDetail(id) {
    let res = await getAnnouncement(id);
    setAnnnouncement(res.data);
  }

  async function _getAvailableAnnouncementTypes() {
    let res = await getAvailableAnnouncementTypes();
    setAnnouncementTypes(res.data);
  }

  const initialForm = {
    title: announcement?.title || "",
    announce_type: announcement?.announce_type || "",
    content: announcement?.content || ""
  };

  return (
    <div className="content-area">
      <h1 className="title-bar">發布公告</h1>
      <InputForm initialForm={initialForm} announcementTypes={announcementTypes} />
    </div>
  );
}
