import { useParams, useNavigate } from "react-router-dom";
import { useFormik } from "formik";
import * as yup from "yup";

import { Grid, Button, TextField, Select, FormControl, MenuItem, InputLabel } from "@mui/material";
import { postAnnouncement, patchAnnouncement } from "@/Services/Announcement";
import PropTypes from "prop-types";

export default function InputForm(props) {
  InputForm.propTypes = { initialForm: PropTypes.object, announcementTypes: PropTypes.array };
  let { id } = useParams();
  const isNew = id == "new";
  const history = useNavigate();

  const schema = yup.object().shape({
    title: yup.string().required("必須有標題"),
    announce_type: yup.string().required(),
    content: yup.string().required()
  });

  function submitForm(e) {
    if (isNew) {
      postAnnouncement(e);
    } else {
      patchAnnouncement(id, e);
    }
    history(`/Announcement/List/`);
  }

  const formik = useFormik({
    initialValues: props.initialForm,
    validationSchema: schema,
    enableReinitialize: true,
    onSubmit: submitForm
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container rowSpacing={1}>
        <Grid item xs={12}>
          <TextField
            className="mb-3"
            fullWidth
            multiline
            id="title"
            name="title"
            label="主旨"
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.title && Boolean(formik.errors.title)}
            helperText={formik.touched.title && formik.errors.title}
          />
        </Grid>
        <Grid item xs={12}>
          <FormControl fullWidth className="mb-3">
            <InputLabel>公告種類</InputLabel>
            <Select id="announce_type" name="announce_type" value={formik.values.announce_type} label="公告種類" onChange={formik.handleChange}>
              {props.announcementTypes.map(e => (
                <MenuItem key={e.value} value={e.value}>
                  {e.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12}>
          <TextField
            fullWidth
            multiline
            rows={5}
            id="content"
            name="content"
            label="公告內容"
            className="mb-3"
            value={formik.values.content}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.content && Boolean(formik.errors.content)}
            helperText={formik.touched.content && formik.errors.content}
          />
        </Grid>
        <Grid item xs={12}>
          <Button color="primary" variant="contained" fullWidth type="submit">
            發布
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
