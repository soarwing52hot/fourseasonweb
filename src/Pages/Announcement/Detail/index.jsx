import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import AnnounceCard from "../Components/AnnounceCard";
import { getAnnouncement } from "@/Services/Announcement";

export default function AnnouncementDetail() {
  let { id } = useParams();
  const [announce, setAnnounce] = useState({});

  useEffect(() => {
    Promise.all([_getAnnounce()]);
  }, []);

  async function _getAnnounce() {
    const res = await getAnnouncement(id);
    setAnnounce(res.data);
  }
  return (
    <>
      <div className="content-area">
        <AnnounceCard announce={announce} />
      </div>
    </>
  );
}
