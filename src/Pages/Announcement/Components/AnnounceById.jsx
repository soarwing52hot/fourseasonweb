import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import AnnounceCard from "./AnnounceCard";
import { getAnnouncement } from "@/Services/Announcement";

export default function AnnounceById(props) {
  AnnounceById.propTypes = {
    id: PropTypes.number
  };
  const [announce, setAnnounce] = useState({});

  useEffect(() => {
    _getAnnounce();
  }, [props.id]);

  async function _getAnnounce() {
    if (!props.id) return;
    const res = await getAnnouncement(props.id);
    setAnnounce(res.data);
  }
  return <>{props.id ? <AnnounceCard announce={announce} /> : <p>無公告</p>}</>;
}
