import { Grid, TextField, Card, CardContent, Button } from "@mui/material";
import { useBasicInfo } from "@/Components/BasicInfoContext/BasicInfoContext";
import { useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import PropTypes from "prop-types";
import moment from "moment";

export default function AnnounceCard(props) {
  const history = useNavigate();
  AnnounceCard.propTypes = {
    announce: PropTypes.object
  };
  const announce = props.announce;
  const { announcementTypes } = useBasicInfo();
  const user = useSelector(state => state.user);
  function typeToName(type) {
    let r = announcementTypes.find(e => e.value == type);
    return r?.label;
  }

  const countArr = announce?.content?.split("\n").length;

  function toEdit() {
    history(`../Announcement/Form/${announce.id}`);
  }

  return (
    <Card style={{ marginBlock: "10px" }} key={announce.id}>
      <CardContent>
        <h4>
          <b>{`[${typeToName(announce.announce_type)}] ${announce.title}`}</b>
        </h4>
        <p className="mb-2 text-muted">
          {moment(announce.creation_date).format("YYYY/MM/DD HH:mm:ss")}
          <span style={{ fontSize: 12 }}>
            <i> {`(最後修改時間:${moment(announce.modify_date).format("YYYY/MM/DD HH:mm:ss")})`}</i>
          </span>
        </p>
        <Grid container>
          <Grid item md={11} xs={9}>
            <p>{`公告編號: ${announce.id}`}</p>
            <p>{`發文者：${announce.writer?.username}`}</p>
          </Grid>
          {user?.membership_number == announce.writer?.membership_number && (
            <Grid item md={1} xs={2}>
              <Button variant="contained" color="success" onClick={toEdit}>
                修改
              </Button>
            </Grid>
          )}
        </Grid>
        <TextField fullWidth multiline value={announce.content} readOnly rows={countArr} />
      </CardContent>
    </Card>
  );
}
