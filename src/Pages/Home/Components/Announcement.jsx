import { Grid } from "@mui/material";

import { useEffect, useState } from "react";
import { getAnnouncementList } from "@/Services/Announcement";

import moment from "moment";

export function Announcement() {
  const [announcementList, setAnnouncementList] = useState([]);

  useEffect(() => {
    getList();
  }, []);

  async function getList(selectedType) {
    const res = await getAnnouncementList({ announce_type: selectedType });
    setAnnouncementList(res.data.results.slice(0, 2));
  }

  function announceRow(announce) {
    return (
      <Grid container key={announce.id}>
        <Grid item xs={5} sm={3}>
          {moment(announce.creation_date).format("YYYY-MM-DD")}
        </Grid>
        <Grid item xs={7} sm={9}>
          <b>{announce.title}</b>
        </Grid>
      </Grid>
    );
  }
  return (
    <>
      <div style={{ marginBottom: "10px", marginTop: "10px" }}>{announcementList.map(announceRow)}</div>
    </>
  );
}
