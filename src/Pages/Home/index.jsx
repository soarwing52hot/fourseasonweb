import Grid from "@mui/material/Unstable_Grid2";
import Card from "@mui/material/Card";
import CardHeader from "@mui/material/CardHeader";
import CardMedia from "@mui/material/CardMedia";

import { ImgCover, Img1, Img2 } from "@/Style/images";
import { Announcement } from "./Components/Announcement";
import { Link } from "react-router-dom";

function Home() {
  return (
    <>
      <div className="cover">
        <img className="cover-img" src={ImgCover} alt="Background Image" />
        <div className="color-overlay" />
        <h1 className="cover-text">
          WELCOME
          <br />
          台灣四季溯溪協會
        </h1>
      </div>
      <div className="content-area title-bar">
        <Grid container spacing={2} sx={{ my: 2 }}>
          <Grid xs={12} md={6} as={Link} to="/Camp/List">
            <Card>
              <CardMedia component="img" image={Img1} className="zone-img" style={{ objectPosition: "0 20%" }} />
              <CardHeader title="營隊資訊" />
              <p>體驗營/溪降體驗營/初溯營/嚮導營</p>
            </Card>
          </Grid>
          <Grid xs={12} md={6} as={Link} to="/Activity/List">
            <Card>
              <CardMedia component="img" image={Img2} className="zone-img" style={{ objectPosition: "0 60%" }} />
              <CardHeader title="近期活動" />
              <p>溯登/高山溪/溪降/訓練</p>
            </Card>
          </Grid>
        </Grid>
        <Grid container spacing={2} className=" my-2" alignContent="center" justifyContent="center">
          <Grid xs={12} md={8} as={Link} to="Announcement/List/">
            <Card>
              <CardHeader title="協會公告" />
              <Announcement />
            </Card>
          </Grid>
        </Grid>
      </div>
    </>
  );
}

export default Home;
