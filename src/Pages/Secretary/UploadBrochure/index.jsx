import { useState, useEffect } from "react";
import { Button, Grid, FormControl, MenuItem, Select, TextField } from "@mui/material";
import { uploadBrochure, getCampTypes } from "@/Services/Camps";

export default function UploadBrochure() {
  const [campTypes, setCampTypes] = useState([]);
  const [selectedCampType, setSelectedCampType] = useState("");
  const [fileUrl, setFileUrl] = useState("");

  useEffect(() => {
    setupCampOptions();
  }, []);

  async function setupCampOptions() {
    let res = await getCampTypes();
    setCampTypes(res.data);
    setSelectedCampType(res.data[0].value);
  }

  const doSubmit = async e => {
    e.preventDefault();

    let formData = new FormData();
    formData.append("camp_type", selectedCampType);
    formData.append("pdf_uuid", fileUrl);
    await uploadBrochure(formData);
    alert("上傳成功");
  };

  return (
    <>
      <div className="content-area">
        <h1>營隊簡章上傳</h1>
        <Grid container spacing={1}>
          <Grid item xs={12} md={12}>
            營隊類型
          </Grid>
          <Grid item xs={12} md={12}>
            <FormControl fullWidth>
              <Select name="campType" value={selectedCampType} onChange={e => setSelectedCampType(e.target.value)}>
                {campTypes.map(e => (
                  <MenuItem value={e.value} key={e.value}>
                    {e.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12} md={12}>
            上傳檔案
          </Grid>
          <Grid item xs={12} md={12}>
            <TextField
              fullWidth
              id="formFile"
              type="file"
              name="pdf_file"
              accept="pdf"
              onChange={e => {
                setFileUrl(e.target.files[0]);
              }}
            />
          </Grid>
          <Grid item xs={12} md={12}>
            <Button variant="contained" color="success" onClick={doSubmit}>
              上傳
            </Button>
          </Grid>
        </Grid>
      </div>
    </>
  );
}
