import { useState, useRef } from "react";
import { changeGroup } from "@/Services/User";
import CloseIcon from "@mui/icons-material/Close";
import { Snackbar, IconButton, Button, Grid, Card, CardContent, FormControlLabel, Checkbox, Typography } from "@mui/material";
import PropTypes from "prop-types";

export default function UserGroup({ user, groupNames }) {
  UserGroup.propTypes = {
    user: PropTypes.object,
    groupNames: PropTypes.array
  };

  const [groups, setGroups] = useState(user.groups.map(e => e.name));
  const [alertState, setAlertState] = useState({ text: "修改成功", show: false, variant: "primary" });

  async function sendChangeGroup(event) {
    event.preventDefault();

    let data = {
      membership_number: user.membership_number,
      groups: groups
    };

    try {
      await changeGroup(data);
      setAlertState({ text: "修改成功", show: true, variant: "primary" });
    } catch {
      setAlertState({ text: "修改失敗!", show: true, variant: "warning" });
    }
  }

  function changeClick(name) {
    if (groups.includes(name)) {
      setGroups(groups.filter(e => e != name));
    } else {
      setGroups([...groups, name]);
    }
  }

  function groupCheckbox(e) {
    return (
      <Grid item xs={12} md={3} className="mb-3" id={e.name} key={e.id}>
        <FormControlLabel
          key={`${e.name}-formlabel`}
          control={<Checkbox checked={groups.includes(e.name)} key={`${e.id}-checkbox`} name={e.name} onChange={() => changeClick(e.name)} />}
          label={e.name}
        />
      </Grid>
    );
  }

  const snackBarAction = (
    <>
      <IconButton size="small" color="error" onClick={() => setAlertState({ show: false })}>
        <CloseIcon fontSize="small" />
      </IconButton>
    </>
  );

  return (
    <>
      <Grid item xs={12} sm={4}>
        <Snackbar
          open={alertState.show}
          autoHideDuration={1000}
          message={alertState.text}
          onClose={() => setAlertState({ show: false })}
          action={snackBarAction}
          anchorOrigin={{ vertical: "top", horizontal: "center" }}
        />
        <Card sx={{ marginTop: 2 }}>
          <CardContent>
            <Typography variant="h5" component="div">
              {user.username}
            </Typography>
            <p>會員編號:{user.membership_number}</p>
            <Grid container>{groupNames.map(e => groupCheckbox(e))}</Grid>
            <Button variant="contained" type="submit" onClick={sendChangeGroup}>
              設定
            </Button>
          </CardContent>
        </Card>
      </Grid>
    </>
  );
}
