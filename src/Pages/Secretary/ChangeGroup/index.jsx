import { useState, useEffect } from "react";
import { getGroups, getUsersGroups } from "@/Services/User";
import { Grid, TextField, Button, Typography } from "@mui/material";
import UserGroup from "./Components/UserGroup";

export default function ChangeGroup() {
  const [keyword, setKeyword] = useState();
  const [users, setUsers] = useState([]);
  const [groupNames, setGroupNames] = useState([]);

  useEffect(() => {
    fetchGroupOptions();
  }, []);

  async function fetchGroupOptions() {
    let res = await getGroups();
    setGroupNames(res.data.results);
  }

  async function search() {
    let request = "all";
    if (keyword) {
      request = keyword;
    }
    const res = await getUsersGroups(request);
    setUsers(res.data.results);
  }

  return (
    <>
      <div className="content-area">
        <Typography variant="h4" sx={{ marginTop: 3 }} gutterBottom>
          設定幹部屬性
        </Typography>
        <Grid container spacing={2}>
          <Grid item>
            <TextField label="輸入姓名或會員編號" value={keyword} onChange={e => setKeyword(e.target.value)} />
          </Grid>
          <Grid item>
            <Button variant="contained" onClick={search}>
              搜尋
            </Button>
          </Grid>
        </Grid>
        <Grid container spacing={2}>
          {users.map(e => (
            <UserGroup key={e.membership_number} user={e} groupNames={groupNames} />
          ))}
        </Grid>
      </div>
    </>
  );
}
