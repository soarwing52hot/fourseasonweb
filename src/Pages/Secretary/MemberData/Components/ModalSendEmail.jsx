import PropTypes from "prop-types";
import { useRef, useState } from "react";
import { Button, IconButton, Grid, FormControlLabel, Checkbox, TextField, Dialog, DialogTitle, DialogContent, DialogActions } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";

export default function ModalSendEmail(props) {
  ModalSendEmail.propTypes = {
    action: PropTypes.func,
    modalShow: PropTypes.bool,
    title: PropTypes.string,
    defaultValue: PropTypes.object
  };
  const [includeUnpaid, setIncludeUnpaid] = useState(false);

  const form = useRef({});

  function closeModal(state) {
    let ele = form.current.elements;
    let data = {
      subject: ele["subject"].value,
      message: ele["message"].value,
      includeUnpaid: includeUnpaid
    };
    props.action(state, data);
  }

  return (
    <Dialog open={props.modalShow} onClose={() => closeModal(false)}>
      <DialogTitle sx={{ m: 0, p: 2 }}>{props.title}</DialogTitle>
      <IconButton
        aria-label="close"
        onClick={() => closeModal(false)}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: theme => theme.palette.grey[500]
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent dividers sx={{ px: 2 }}>
        <form noValidate ref={form}>
          <TextField fullWidth id="subject" label="主旨" variant="standard" defaultValue={props.defaultValue.title} sx={{ mb: 2 }} />
          <TextField fullWidth id="message" label="內文" multiline rows={6} defaultValue={props.defaultValue.content} />
        </form>
      </DialogContent>

      <DialogActions>
        <Grid container justifyContent="space-between">
          <Grid item>
            <FormControlLabel control={<Checkbox onChange={() => setIncludeUnpaid(!includeUnpaid)} />} label="包含沒繳會費會員" />
          </Grid>
          <Grid item>
            <Button sx={{ mx: 1 }} variant="outlined" color="error" onClick={() => closeModal(false)}>
              取消
            </Button>
            <Button sx={{ mx: 1 }} variant="contained" onClick={() => closeModal(true)}>
              確認
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
}
