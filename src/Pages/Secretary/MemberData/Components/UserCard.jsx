import { Grid, Card, CardContent, Typography } from "@mui/material";
import PropTypes from "prop-types";

export default function UserCard({ member }) {
  UserCard.propTypes = {
    member: PropTypes.object
  };

  return (
    <Grid item xs={1} md={1}>
      <Card style={{ height: "100%" }}>
        <CardContent>
          <Typography variant="h6">{member.username}</Typography>

          <Grid container>
            <Grid item sm={6} md="auto">
              <b>會員編號</b>
            </Grid>
            <Grid item sm={6} md="auto">
              {member.membership_number}
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item sm={6} md="auto">
              <b>email</b>
            </Grid>
            <Grid item sm={6} md="auto">
              {member.email}
            </Grid>
          </Grid>
          <Grid container spacing={2}>
            <Grid item sm={6} md="auto">
              <b>電話</b>
            </Grid>
            <Grid item sm={6} md="auto">
              <p>{member.phone}</p>
              <p>{member.land_line}</p>
            </Grid>
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );
}
