import { useState } from "react";

import { Button } from "@mui/material";
import { postSendEmail } from "@/Services/Announcement";
import ModalSendEmail from "./ModalSendEmail";

export default function ModalSendEmailButton() {
  const [showSendEmailModal, setShowSendEmailModal] = useState(false);
  const defaultEmailValue = {
    title: `全會員通知`,
    content: ""
  };

  async function sendEmail(state, data) {
    setShowSendEmailModal(false);
    if (state == false) {
      return;
    }

    const result = await postSendEmail(data);

    if (result.data.success) {
      alert("寄送成功");
    } else {
      alert("寄送失敗");
    }
  }
  return (
    <>
      <Button variant="contained" color="secondary" sx={{ my: 1 }} onClick={() => setShowSendEmailModal(true)}>
        寄送Email
      </Button>
      <ModalSendEmail title={"寄送會員信件"} modalShow={showSendEmailModal} action={sendEmail} defaultValue={defaultEmailValue} />
    </>
  );
}
