import { useState, useEffect } from "react";
import { getUserList } from "@/Services/User";

import UserCard from "./Components/UserCard";
import { Grid, Button, TextField } from "@mui/material";

import usePagination from "@/hooks/usePagination";
import ModalSendEmailButton from "./Components/ModalSendEmailButton";
import PageButton from "@/Components/PageButton";

export default function MemberData() {
  const [keyword, setKeyword] = useState("");
  const [userData, setUserData] = useState([]);
  const { pages, setNewPages } = usePagination({});

  useEffect(() => {
    getUserData();
  }, []);

  const getUserData = async (page = 1) => {
    let requestData = { page };
    if (keyword) {
      let memberNumber = parseInt(keyword);

      if (memberNumber) {
        requestData.membership_number = memberNumber;
      } else {
        requestData.username = keyword;
      }
    }

    let result = await getUserList(requestData);

    let data = result.data;
    setUserData(data.results);
    setNewPages(data);
  };

  return (
    <>
      <div className="content-area">
        <h3 className="mt-3">會員資料總覽</h3>
        <Grid container spacing={2} mb={2}>
          <Grid item>
            <TextField placeholder="輸入姓名或會員編號" value={keyword} onChange={e => setKeyword(e.target.value)} />
          </Grid>
          <Grid item>
            <Button variant="contained" onClick={() => getUserData()} sx={{ marginY: 1 }}>
              搜尋
            </Button>
          </Grid>
          <Grid item>
            <ModalSendEmailButton />
          </Grid>
        </Grid>
        <Grid container columns={{ xs: 1, md: 5 }} spacing={2}>
          {userData.map(e => (
            <UserCard member={e} key={e.membership_number} />
          ))}
        </Grid>
        <PageButton pages={pages} func={getUserData} />
      </div>
    </>
  );
}
