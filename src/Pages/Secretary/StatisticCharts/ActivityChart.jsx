import { useState } from "react";
import { getActivityCount } from "@/Services/Activities";
import { Grid } from "@mui/material";
import MonthlyBarChartBlock from "./Components/MonthlyBarChartBlock";
import SearchBar from "./Components/SearchBar";

export default function ActivityChart() {
  const [years, setYears] = useState([]);

  return (
    <div className="content-area">
      <h1 className="title-bar">隊伍數量統計</h1>
      <SearchBar setYears={setYears} />
      <Grid container spacing={2}>
        {years.map(e => (
          <MonthlyBarChartBlock key={e} year={e} apiAction={getActivityCount} chartName="隊伍數量統計" />
        ))}
      </Grid>
    </div>
  );
}
