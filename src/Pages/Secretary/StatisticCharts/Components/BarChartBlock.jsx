import { useState, useEffect } from "react";

import { Grid, Card, CardContent, Typography, TextField } from "@mui/material";
import { BarChart, Bar, Rectangle, XAxis, YAxis, Tooltip, ResponsiveContainer } from "recharts";
import PropTypes from "prop-types";

export default function BarChartBlock(props) {
  BarChartBlock.propTypes = {
    year: PropTypes.number,
    chartName: PropTypes.string,
    apiAction: PropTypes.func
  };
  const year = parseInt(props.year);
  const chartName = props.chartName;
  const apiAction = props.apiAction;
  const [apiData, setData] = useState([]);
  const [numberOfRank, setNumberOfRank] = useState(10);
  let dataToShow = apiData.slice(0, numberOfRank);

  const data = dataToShow.map(e => {
    return {
      name: e[0],
      count: e[1]
    };
  });

  useEffect(() => {
    _getChartData();
  }, []);

  async function _getChartData() {
    let res = await apiAction(year);
    setData(res.data);
  }
  return (
    <Grid item xs={12} sm={3} sx={{ my: 2 }}>
      <Card>
        <CardContent>
          <Typography>
            <h2>{`${props.year} ${props.chartName}`}</h2>
          </Typography>
          <Grid container>
            <TextField label="顯示名次數量" type="number" value={numberOfRank} onChange={e => setNumberOfRank(e.target.value)} />
          </Grid>
          <Grid container sx={{ height: 500 }}>
            {data.length > 0 ? (
              <ResponsiveContainer width="100%">
                <BarChart
                  height={200}
                  data={data}
                  margin={{
                    top: 5,
                    right: 0,
                    left: 0,
                    bottom: 5
                  }}
                >
                  <XAxis dataKey="name" />
                  <YAxis dataKey="count" type="number" allowDecimals={false} ticks={false} label={{ value: "隊數", angle: -90 }} />
                  <Tooltip />
                  <Bar dataKey="count" fill="rgba(219, 229, 81, 0.7)" activeBar={<Rectangle fill="rgba(219, 229, 81, 0.7)" stroke="blue" />} />
                </BarChart>
              </ResponsiveContainer>
            ) : (
              <p>無資料</p>
            )}
          </Grid>
        </CardContent>
      </Card>
    </Grid>
  );
}
