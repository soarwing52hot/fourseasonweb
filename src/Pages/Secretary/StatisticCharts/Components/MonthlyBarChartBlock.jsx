import { useState, useEffect } from "react";
import { Grid, Card, CardContent, Typography } from "@mui/material";
import { BarChart, Bar, Rectangle, XAxis, YAxis, Tooltip, ResponsiveContainer } from "recharts";
import PropTypes from "prop-types";

MonthlyBarChartBlock.propTypes = {
  apiAction: PropTypes.func.isRequired,
  year: PropTypes.number.isRequired,
  chartName: PropTypes.string.isRequired
};

export default function MonthlyBarChartBlock(props) {
  const year = parseInt(props.year);
  const apiAction = props.apiAction;
  const [chartData, setChartData] = useState([]);

  useEffect(() => {
    _getChartData();
  }, []);

  const transformData = apiData => {
    return Object.entries(apiData).map(([month, amount]) => ({
      month,
      amount
    }));
  };

  async function _getChartData() {
    let res = await apiAction(year);
    let transformedData = transformData(res.data);
    setChartData(transformedData);
  }

  return (
    <Grid item xs={12} sm={6}>
      <Card>
        <CardContent>
          <Typography variant="h6">{`${props.year} ${props.chartName}`}</Typography>
          <div style={{ height: 500 }}>
            {chartData.length > 0 ? (
              <ResponsiveContainer width="100%">
                <BarChart
                  height={200}
                  data={chartData}
                  margin={{
                    top: 5,
                    right: 0,
                    left: 0,
                    bottom: 5
                  }}
                >
                  <XAxis dataKey="month" />
                  <YAxis dataKey="amount" type="number" allowDecimals={false} ticks={false} label={{ value: "隊數", angle: -90 }} />
                  <Tooltip />
                  <Bar dataKey="amount" fill="rgba(219, 229, 81, 0.7)" activeBar={<Rectangle fill="rgba(219, 229, 81, 0.7)" stroke="blue" />} />
                </BarChart>
              </ResponsiveContainer>
            ) : (
              <p>無資料</p>
            )}
          </div>
        </CardContent>
      </Card>
    </Grid>
  );
}
