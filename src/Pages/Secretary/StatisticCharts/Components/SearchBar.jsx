import { useState } from "react";
import { Button, Grid, FormControl, MenuItem, InputLabel, Select } from "@mui/material";
import moment from "moment-timezone";
import PropTypes from "prop-types";

export default function SearchBar(props) {
  SearchBar.propTypes = {
    setYears: PropTypes.func
  };
  const setYears = props.setYears;
  const today = moment();
  const [form, setForm] = useState({
    yearStart: today.year(),
    yearEnd: today.year()
  });

  function yearOptions() {
    let yearsAvailable = 10;
    let result = [];
    for (let i = 0; i < yearsAvailable; i++) {
      let yearToAdd = today.year() - i;
      result.push(yearToAdd);
    }

    return result.map(e => (
      <MenuItem key={e} value={e}>
        {e}
      </MenuItem>
    ));
  }

  const handleChange = event => {
    const name = event.target.name;
    const value = event.target.value;
    setForm(values => ({ ...values, [name]: value }));
  };

  async function handleSubmit() {
    let yearArray = [];

    for (let i = parseInt(form.yearEnd); i >= parseInt(form.yearStart); i--) {
      yearArray.push(i);
    }

    setYears(yearArray);
  }

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={4}>
        <FormControl fullWidth>
          <InputLabel>起始年</InputLabel>
          <Select name="yearStart" value={form.yearStart} onChange={handleChange}>
            {yearOptions()}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={4}>
        <FormControl fullWidth>
          <InputLabel>截止年</InputLabel>
          <Select name="yearEnd" value={form.yearEnd} onChange={handleChange}>
            {yearOptions()}
          </Select>
        </FormControl>
      </Grid>
      <Grid item xs={12} md={4}>
        <Button variant="contained" onClick={handleSubmit}>
          搜尋
        </Button>
      </Grid>
    </Grid>
  );
}
