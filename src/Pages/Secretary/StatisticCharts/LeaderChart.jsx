import { useState } from "react";
import { getLeaderChart } from "@/Services/Activities";
import { Grid } from "@mui/material";
import BarChartBlock from "./Components/BarChartBlock";
import SearchBar from "./Components/SearchBar";

export default function LeaderChart() {
  const [years, setYears] = useState([]);

  return (
    <div className="content-area">
      <h1 className="title-bar">帶隊王統計</h1>
      <SearchBar setYears={setYears} />
      <Grid container spacing={2}>
        {years.map(e => (
          <BarChartBlock key={e} year={e} apiAction={getLeaderChart} chartName="帶隊統計(次)" />
        ))}
      </Grid>
    </div>
  );
}
