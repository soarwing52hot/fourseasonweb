import { useState } from "react";
import { getParticipantChart } from "@/Services/Activities";
import { Grid } from "@mui/material";
import BarChartBlock from "./Components/BarChartBlock";
import SearchBar from "./Components/SearchBar";

export default function ParticipantChart() {
  const [years, setYears] = useState([]);

  return (
    <div className="content-area">
      <h1 className="title-bar">出隊王統計</h1>
      <SearchBar setYears={setYears} />
      <Grid container spacing={2}>
        {years.map(e => (
          <BarChartBlock key={e} year={e} apiAction={getParticipantChart} chartName="出隊統計(次)" />
        ))}
      </Grid>
    </div>
  );
}
