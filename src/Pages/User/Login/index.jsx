import { useRef } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";

import { Grid, Button, TextField } from "@mui/material";
import { login as reduxLogin, setUserByToken } from "@/Store/userSlice";
import { login } from "@/Services/User";

function LoginPage() {
  const loginRef = useRef({ username: "", password: "" });

  const dispatch = useDispatch();
  const history = useNavigate();

  async function LoginBtn(ele) {
    ele.preventDefault();
    let data = {
      membership_number: loginRef.current.elements["membership_number"].value,
      password: loginRef.current.elements["password"].value
    };

    let rsp = await login(data);

    dispatch(reduxLogin(rsp.data));
    dispatch(setUserByToken());
    history("/Home");
  }

  const RegisterBtn = e => {
    e.preventDefault();
    history("/User/Disclaimers");
  };

  return (
    <div style={{ maxWidth: 600 }} className="content-area">
      <form ref={loginRef}>
        <Grid container rowSpacing={1} spacing={2} justifyContent="center" alignItems="center" sx={{ my: 2 }}>
          <Grid item xs={4} md={2}>
            會員編號
          </Grid>
          <Grid item xs={8} md={10}>
            <TextField fullWidth type="number" name="membership_number" />
          </Grid>
        </Grid>
        <Grid container rowSpacing={1} spacing={2} justifyContent="center" alignItems="center" sx={{ my: 2 }}>
          <Grid item xs={4} md={2}>
            密碼
          </Grid>
          <Grid item xs={8} md={10}>
            <TextField fullWidth type="password" name="password" placeholder="Password" />
          </Grid>
        </Grid>
        <Grid container rowSpacing={1} spacing={2} justifyContent="center" alignItems="center" sx={{ my: 2 }}>
          <Grid item xs={6} md={3}>
            <Button type="submit" color="success" variant="contained" id="LoginBtn" className="btn button-act" onClick={LoginBtn}>
              登入
            </Button>
          </Grid>
          <Grid item xs={6} md={3}>
            <Button id="RegisterBtn" color="secondary" className="btn button-light" onClick={RegisterBtn}>
              註冊
            </Button>
          </Grid>
        </Grid>
      </form>
    </div>
  );
}

export default LoginPage;
