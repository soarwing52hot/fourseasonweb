import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { tokenGetActivityRecord } from "@/Services/Activities";
import ActivityCard from "@/Components/ActivityView/ActivityCard";
import { Grid } from "@mui/material";
import moment from "moment-timezone";
import SearchBar from "@/Components/SearchBar";
import { useParams } from "react-router-dom";

export default function ActivityHistory() {
  const { id } = useParams();
  const user = useSelector(state => state.user);
  const [DateSearch, setDateSearch] = useState({});
  const [activityHistory, setActivityHistory] = useState([]);
  const [leaderHistory, setLeaderHistory] = useState([]);

  useEffect(() => {
    searchAction();
  }, []);

  function searchAction() {
    if (!user) {
      return;
    }
    if (!DateSearch.dateStart || !DateSearch.dateEnd) {
      return;
    }

    Promise.all([getRecords(), getLeaderRecords()]);
  }

  async function getRecords() {
    const data = {
      trip_date_start: moment(DateSearch.dateStart).format("YYYY-MM-DD"),
      trip_date_end: moment(DateSearch.dateEnd).format("YYYY-MM-DD"),
      membership_number: id
    };
    let response = await tokenGetActivityRecord(data);
    setActivityHistory(response.data.results);
  }

  async function getLeaderRecords() {
    let data = {
      leader: id
    };
    let res = await tokenGetActivityRecord(data);
    setLeaderHistory(res.data.results);
  }

  return (
    <>
      <div className="content-area">
        <h1 className="title-bar">活動紀錄</h1>
        <SearchBar DateSearch={DateSearch} setDateSearch={setDateSearch} searchAction={searchAction} />
        <br />
        {leaderHistory.length > 0 ? (
          <>
            <Grid>
              <h3 className="title-bar">帶隊紀錄</h3>
            </Grid>
            <Grid container columns={{ xs: 1, md: 4 }} spacing={{ sm: 0, md: 2 }}>
              {leaderHistory?.map(e => (
                <ActivityCard activity={e} key={e.id} />
              ))}
            </Grid>
          </>
        ) : null}

        <Grid>
          <h3 className="title-bar">跟隊紀錄</h3>
        </Grid>
        <Grid container columns={{ xs: 1, md: 4 }} spacing={{ sm: 0, md: 2 }}>
          {activityHistory?.map(e => (
            <ActivityCard activity={e} key={e.id} />
          ))}
        </Grid>
      </div>
    </>
  );
}
