import { useState, useEffect } from "react";
import moment from "moment-timezone";
import { useSelector } from "react-redux";
import { getUser, patchUser } from "@/Services/User";
import { useParams, useNavigate } from "react-router-dom";
import ProfilePhoto from "./Components/ProfilePhoto";
import UserEditForm from "./Components/UserEditForm";
import { ImgProfile } from "@/Style/images";
import { Card, CardContent, Grid } from "@mui/material";

export default function Edit() {
  let { id } = useParams();
  const user = useSelector(state => state.user);
  const history = useNavigate();
  const [userData, setUserData] = useState({});

  useEffect(() => {
    if (user.membership_number != id) {
      alert("非本人！");
      history("/Home");
    }
    _getUser();
  }, []);

  async function _getUser() {
    let res = await getUser(id);
    setUserData(res.data);
  }

  async function handleSubmit(data) {
    await patchUser(id, data);
    alert("修改成功");
    _getUser();
  }

  const initialValue = {
    membership_number: id,
    username: userData.username || "",
    email: userData.email || "",
    gender: userData.gender || "",
    birth_date: moment(userData.birth_date),
    birth_location: userData.birth_location || "",
    citizen_id: userData.citizen_id || "",
    phone: userData.phone || "",
    land_line: userData.land_line || "",
    postal_code: userData.postal_code || "",
    address: userData.address || "",
    emergency_contact_name: userData.emergency_contact_name || "",
    emergency_contact_phone: userData.emergency_contact_phone || "",
    emergency_contact_relation: userData.emergency_contact_relation || "",
    facebook_name: userData.facebook_name || ""
  };

  return (
    <>
      <div className="content-area">
        <Card sx={{ mt: 2 }}>
          <CardContent>
            <Grid container spacing={2} rowSpacing={1}>
              <Grid item xs={12}>
                <img className="m-3" src={userData?.photo ? userData?.photo?.image_url : ImgProfile} alt="profile" height={150} />
              </Grid>
              <Grid item xs={12}>
                <ProfilePhoto getUser={_getUser} />
              </Grid>
              <Grid item xs={12}>
                <UserEditForm initialValue={initialValue} submitForm={handleSubmit} />
              </Grid>
            </Grid>
          </CardContent>
        </Card>
      </div>
    </>
  );
}
