import { useState } from "react";
import { Grid, Button, TextField, Box } from "@mui/material";

import { uploadProfilePhoto } from "@/Services/User";
import PropTypes from "prop-types";

export default function ProfilePhoto(props) {
  ProfilePhoto.propTypes = { getUser: PropTypes.func };
  const getUser = props.getUser;
  const [data, setData] = useState({
    title: "",
    description: "",
    image_url: ""
  });

  const handleImageChange = e => {
    let newData = { ...data };
    newData["image_url"] = e.target.files[0];
    setData(newData);
  };

  const doSubmit = async e => {
    e.preventDefault();

    let formData = new FormData();

    formData.append("image_url", data.image_url);

    await uploadProfilePhoto(formData);
    alert("上傳成功");
    getUser();
  };

  return (
    <Box sx={{ mr: 3 }}>
      <form>
        <Grid container spacing={2} sx={{ my: 1 }}>
          <Grid item xs={3} md={1}>
            大頭貼
          </Grid>
          <Grid item xs={7} md={4}>
            <TextField
              type="file"
              name="image_url"
              accept="image/jpeg,image/png,image"
              onChange={e => {
                handleImageChange(e);
              }}
              fullWidth
            />
          </Grid>
          <Grid item xs={2} md={4}>
            <Button variant="contained" color="success" type="submit" onClick={e => doSubmit(e)}>
              上傳照片
            </Button>
          </Grid>
        </Grid>
      </form>
    </Box>
  );
}
