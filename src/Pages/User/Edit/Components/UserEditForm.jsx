import { useFormik } from "formik";
import moment from "moment-timezone";
import * as yup from "yup";
import PropTypes from "prop-types";
import { Grid, TextField, Button, FormControlLabel, Radio, RadioGroup } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

export default function UserEditForm(props) {
  UserEditForm.propTypes = { initialValue: PropTypes.object, submitForm: PropTypes.func };

  const validationSchema = yup.object().shape({
    username: yup.string().required("必填欄位"),
    email: yup
      .string()
      .email()
      .required("必填欄位"),
    gender: yup.string().required("必填欄位"),
    birth_date: yup.date().required("必填欄位"),
    birth_location: yup.string().required("必填欄位"),
    citizen_id: yup
      .string()
      .required("必填欄位")
      .min(10)
      .max(10),
    phone: yup.string().required("必填欄位"),
    land_line: yup.string(),
    postal_code: yup.string().required("必填欄位"),
    address: yup.string().required("必填欄位"),
    emergency_contact_name: yup.string().required("必填欄位"),
    emergency_contact_phone: yup.string().required("必填欄位"),
    emergency_contact_relation: yup.string().required("必填欄位"),
    facebook_name: yup.string().required("必填欄位")
  });

  const formik = useFormik({
    initialValues: props.initialValue,
    validationSchema: validationSchema,
    enableReinitialize: true,
    onSubmit: values => {
      let data = { ...values };

      const dateCols = ["birth_date"];
      dateCols.forEach(e => {
        data[e] = data[e].format("YYYY-MM-DD");
      });

      props.submitForm(data);
    }
  });
  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={2} rowSpacing={1}>
        <Grid item xs={3} md={1}>
          會員編號
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField fullWidth id="membership_number" name="membership_number" value={props.initialValue.membership_number} disabled />
        </Grid>
        <Grid item xs={3} md={1}>
          姓名
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="username"
            name="username"
            placeholder="王小明"
            value={formik.values.username}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.username && Boolean(formik.errors.username)}
          />
          {formik.errors.username}
        </Grid>
        <Grid item xs={3} md={1}>
          身分證字號
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="citizen_id"
            name="citizen_id"
            placeholder="英文要大寫"
            value={formik.values.citizen_id}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.citizen_id && Boolean(formik.errors.citizen_id)}
          />
          {formik.errors.citizen_id}
        </Grid>
        <Grid item xs={3} md={1}>
          Facebook 名稱
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="facebook_name"
            name="facebook_name"
            value={formik.values.facebook_name}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.facebook_name && Boolean(formik.errors.facebook_name)}
          />
          {formik.errors.facebook_name}
        </Grid>
        <Grid item xs={3} md={1}>
          性別
        </Grid>
        <Grid item xs={9} md={11}>
          <RadioGroup name="gender" value={formik.values.gender} onChange={formik.handleChange}>
            <FormControlLabel value="M" control={<Radio />} label="男性" />
            <FormControlLabel value="F" control={<Radio />} label="女性" />
          </RadioGroup>
        </Grid>
        <Grid item xs={3} md={1}>
          出生日期
        </Grid>
        <Grid item xs={9} md={11}>
          <DatePicker
            name="birth_date"
            value={moment(formik.values.birth_date)}
            onChange={e => formik.setFieldValue("birth_date", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          EMAIL
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="email"
            name="email"
            type="email"
            value={formik.values.email}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.email && Boolean(formik.errors.email)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          行動電話
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="phone"
            name="phone"
            type="tel"
            value={formik.values.phone}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.phone && Boolean(formik.errors.phone)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          住家電話(選填)
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="land_line"
            name="land_line"
            type="tel"
            value={formik.values.land_line}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.land_line && Boolean(formik.errors.land_line)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          國籍
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="birth_location"
            name="birth_location"
            type="text"
            value={formik.values.birth_location}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.birth_location && Boolean(formik.errors.birth_location)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          住址
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="postal_code"
            name="postal_code"
            type="number"
            label="郵遞區號"
            placeholder="郵遞區號"
            value={formik.values.postal_code}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.postal_code && Boolean(formik.errors.postal_code)}
          />
        </Grid>
        <Grid item xs={3} md={1}></Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="address"
            name="address"
            label="地址"
            placeholder="地址"
            type="text"
            value={formik.values.address}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.address && Boolean(formik.errors.address)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          緊急聯絡人
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="emergency_contact_name"
            name="emergency_contact_name"
            type="text"
            value={formik.values.emergency_contact_name}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.emergency_contact_name && Boolean(formik.errors.emergency_contact_name)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          緊急聯絡人電話
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="emergency_contact_phone"
            name="emergency_contact_phone"
            type="text"
            value={formik.values.emergency_contact_phone}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.emergency_contact_phone && Boolean(formik.errors.emergency_contact_phone)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          關係
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="emergency_contact_relation"
            name="emergency_contact_relation"
            type="tel"
            value={formik.values.emergency_contact_relation}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.emergency_contact_relation && Boolean(formik.errors.emergency_contact_relation)}
          />
        </Grid>
        <Grid item xs={12} md={12}>
          <Button color="success" variant="contained" fullWidth type="submit">
            儲存資料
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
