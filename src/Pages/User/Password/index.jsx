import { useEffect } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { Grid, TextField, Button, Card, CardContent } from "@mui/material";
import { useSelector } from "react-redux";
import { changePassword } from "@/Services/User";
import { useParams, useNavigate } from "react-router-dom";

export default function Password() {
  let { id } = useParams();
  const user = useSelector(state => state.user);
  const history = useNavigate();

  useEffect(() => {
    if (user.membership_number != id) {
      alert("非本人！");
      history("/Home");
    }
  }, []);
  const validationSchema = yup.object().shape({
    old_password: yup.string().required(),
    password: yup.string().required(),
    password2: yup.string().required()
  });
  const initialValue = { old_password: "", password: "", password2: "" };

  const formik = useFormik({
    initialValues: initialValue,
    validationSchema: validationSchema,
    onSubmit: values => {
      let data = { ...values };

      handleSubmit(data);
    }
  });

  async function handleSubmit(data) {
    console.log(data);
    if (data.old_password == data.password) {
      alert("新舊密碼相同！");
      return;
    }

    if (data.password != data.password2) {
      alert("密碼確認不相同！");
      return;
    }

    await changePassword(id, data);
    alert("修改成功");
  }

  return (
    <div className="content-area">
      <Card sx={{ my: 2 }}>
        <CardContent>
          <form onSubmit={formik.handleSubmit}>
            <Grid container spacing={2} rowSpacing={1}>
              <Grid item xs={3} md={6}>
                舊密碼
              </Grid>
              <Grid item xs={9} md={6}>
                <TextField
                  fullWidth
                  type="password"
                  name="old_password"
                  value={formik.values.old_password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.old_password && Boolean(formik.errors.old_password)}
                />
              </Grid>
              <Grid item xs={3} md={6}>
                新密碼
              </Grid>
              <Grid item xs={9} md={6}>
                <TextField
                  fullWidth
                  type="password"
                  name="password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.password && Boolean(formik.errors.password)}
                />
              </Grid>
              <Grid item xs={3} md={6}>
                新密碼確認
              </Grid>
              <Grid item xs={9} md={6}>
                <TextField
                  fullWidth
                  type="password"
                  name="password2"
                  value={formik.values.password2}
                  onChange={formik.handleChange}
                  onBlur={formik.handleBlur}
                  error={formik.touched.password2 && Boolean(formik.errors.password2)}
                />
              </Grid>
              <Grid item xs={6} md={3}>
                <Button type="submit" variant="contained" color="success">
                  更改密碼
                </Button>
              </Grid>
              <Grid item></Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </div>
  );
}
