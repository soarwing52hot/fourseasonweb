import LoginPage from "./Login";
import Register from "./Register";
import Disclaimers from "./Disclaimers";
import Payment from "./Payment";
import Edit from "./Edit";
import Password from "./Password";

export { LoginPage, Register, Disclaimers, Payment, Edit, Password };
