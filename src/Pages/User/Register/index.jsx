import { postPayment } from "@/Services/Finance";
import { useNavigate } from "react-router-dom";
import BankInfo from "@/Components/BankInfo";
import { PaymentMapping } from "@/Utilities/Maps";
import UserRegisterForm from "./Components/UserRegisterForm";

function Register() {
  const history = useNavigate();

  function handleSubmit(data) {
    let requestData = {
      account_number: data.account_number,
      transfer_date: data.transfer_date,
      memo: JSON.stringify(data),
      amount: data.amount,
      name: data.username,
      email: data.email,
      phone: data.phone,
      type: PaymentMapping.newMember
    };
    postPayment(requestData)
      .then(res => {
        alert(`感謝您 已收到您的資料 編號為 ${res.data.id}`);
        history("/Home");
      })
      .catch(err => {
        alert("資料傳輸失敗!", JSON.stringify(err));
      });
  }

  return (
    <div className="content-area">
      <BankInfo />
      <UserRegisterForm submitForm={handleSubmit} />
    </div>
  );
}

export default Register;
