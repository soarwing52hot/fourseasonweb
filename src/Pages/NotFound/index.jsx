function NotFound() {
  return (
    <div className="content-area title-bar">
      <h1>無此頁面</h1>
      <h3>404 Not Found</h3>
    </div>
  );
}

export default NotFound;
