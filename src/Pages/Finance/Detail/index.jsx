import { useState, useEffect } from "react";
import { ImgLogo, ImgChairman } from "@/Style/images";
import { useParams } from "react-router-dom";
import { getInvoice } from "@/Services/Finance";

const PaymentDetail = () => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState();
  const type_mapping = {
    "1": "營隊報名費",
    "2": "會員會費",
    "3": "其他"
  };

  let { id } = useParams();

  useEffect(() => {
    _getInvoice();
  }, []);

  async function _getInvoice() {
    let res = await getInvoice(id);
    setData(res.data);
    setLoading(false);
  }

  console.debug("Finally!! A wild Pokémon appears :)");

  return (
    <>
      {isLoading ? (
        <div className="App">Loading...</div>
      ) : (
        <div className="invoice-box">
          <table cellPadding="0" cellSpacing="0">
            <tr className="top">
              <td colSpan="2">
                <table>
                  <tr>
                    <td className="title">
                      <img src={ImgLogo} alt="logo" height="90" />
                    </td>

                    <td>
                      收據編號: {data.id}
                      <br />
                      開立日期: {data.transfer_date}
                      <br />
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr className="information">
              <td colSpan="2">
                <table>
                  <tr>
                    <td>
                      臺灣四季溯溪協會
                      <br />
                      新北市板橋區縣民大道三段187巷
                      <br />
                      統一編號：18438492
                      <br />
                    </td>

                    <td>
                      付款人姓名 : {data.name}
                      <br />
                      {data.email}
                    </td>
                  </tr>
                </table>
              </td>
            </tr>

            <tr className="heading">
              <td>項目</td>
              <td>金額</td>
            </tr>

            <tr className="item">
              <td> {type_mapping[data.type]} </td>

              <td>{data.amount}</td>
            </tr>
          </table>
          <br />
          <br />
          <div align="right" className="sign">
            理事長&nbsp;&nbsp;&nbsp;
            <img src={ImgChairman} alt="logo" height="90" /> <br />
            <br />
          </div>
        </div>
      )}
    </>
  );
};

export default PaymentDetail;
