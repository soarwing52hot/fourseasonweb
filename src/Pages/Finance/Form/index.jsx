import { useState, useEffect } from "react";
import moment from "moment-timezone";
import { Button, Grid, FormControl, MenuItem, Select, TextField, Card, CardContent, CardHeader, Divider } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { useNavigate } from "react-router-dom";
import { getPaymentTypes, postPayment, tokenPostPayment } from "@/Services/Finance";
import { useSelector } from "react-redux";
import BankInfo from "@/Components/BankInfo";

const paymentFormData = {
  name: "",
  email: "",
  phone: "",
  account_number: "",
  memo: "",
  transfer_date: moment().format("YYYY-MM-DD"),
  amount: 600,
  type: "2"
};

const PaymentForm = () => {
  const [paymentData, setPaymentData] = useState(paymentFormData);
  const [paymentOptions, setPaymentOptions] = useState([]);
  const user = useSelector(state => state.user);
  const history = useNavigate();
  useEffect(() => {
    setupPaymentOptions();
  }, []);

  function onHandleChange(e) {
    const { name, value } = e.target;

    setPaymentData(prev => ({ ...prev, [name]: value }));
  }

  async function setupPaymentOptions() {
    const res = await getPaymentTypes();
    setPaymentOptions(res.data);
  }
  const submitForm = event => {
    const form = event.currentTarget;
    event.preventDefault();
    if (form.checkValidity() === false) {
      event.stopPropagation();
      return;
    }
    let data = Object.assign({}, paymentData);
    if (user) {
      let memo = `會員編號:${user.membership_number} ${paymentData.memo}`;
      data.member = user.membership_number;
      data.memo = memo;
      tokenPostPayment(data)
        .then(result => {
          alert("success");
          history("/Home");
        })
        .catch(err => {
          console.error(err);
          alert(err.message);
        });
      return;
    }
    postPayment(data)
      .then(result => {
        alert("success");
        history("/Home");
      })
      .catch(err => {
        console.error(err);
        alert(err.message);
      });
  };

  return (
    <div className="content-area">
      <h1 className="title-bar">繳款填寫</h1>
      <Card sx={{ my: 1 }}>
        <CardHeader title={<BankInfo />} />
        <Divider />
        <CardContent>
          <form onSubmit={submitForm}>
            <Grid container rowSpacing={1}>
              <Grid item xs={12} md={1}>
                繳費種類
              </Grid>
              <Grid item xs={12} md={11}>
                <FormControl fullWidth>
                  {paymentOptions.length > 0 && (
                    <Select value={paymentData.type} onChange={onHandleChange} name="type">
                      {paymentOptions.map(e => (
                        <MenuItem value={e.value} key={e.value} name={e.label}>
                          {e.label}
                        </MenuItem>
                      ))}
                    </Select>
                  )}
                </FormControl>
              </Grid>
              <Grid item xs={12} md={1}>
                姓名
              </Grid>
              <Grid item xs={12} md={11}>
                <TextField fullWidth type="text" name="name" size="6" required={true} placeholder="王小明" value={paymentData.name} onChange={onHandleChange} />
              </Grid>
              <Grid item xs={12} md={1}>
                EMAIL
              </Grid>
              <Grid item xs={12} md={11}>
                <TextField fullWidth type="email" name="email" size="30" required={true} value={paymentData.email} onChange={onHandleChange} />
              </Grid>
              <Grid item xs={12} md={1}>
                電話
              </Grid>
              <Grid item xs={12} md={11}>
                <TextField fullWidth type="text" name="phone" size="4" required={true} value={paymentData.phone} onChange={onHandleChange} />
              </Grid>
              <Grid item xs={12} md={1}>
                金額
              </Grid>
              <Grid item xs={12} md={11}>
                <TextField fullWidth type="number" name="amount" size="4" required={true} value={paymentData.amount} onChange={onHandleChange} />
              </Grid>
              <Grid item xs={12} md={1}>
                帳號末五碼
              </Grid>
              <Grid item xs={12} md={11}>
                <TextField fullWidth type="text" pattern="\d*" required={true} maxLength={5} name="account_number" value={paymentData.account_number} onChange={onHandleChange} />
              </Grid>
              <Grid item xs={12} md={1}>
                匯款日期
              </Grid>
              <Grid item xs={12} md={11}>
                <DatePicker
                  required={true}
                  value={moment(paymentData.transfer_date)}
                  onChange={newValue =>
                    setPaymentData(prevState => ({
                      ...prevState,
                      transfer_date: newValue
                    }))
                  }
                />
              </Grid>
              <Grid item xs={12} md={1}>
                備註
              </Grid>
              <Grid item xs={12} md={11}>
                <TextField fullWidth multiline name="memo" rows={3} value={paymentData.memo} onChange={onHandleChange} />
              </Grid>
              <Grid item xs={12}>
                <Button variant="contained" color="success" type="submit">
                  送出
                </Button>
              </Grid>
            </Grid>
          </form>
        </CardContent>
      </Card>
    </div>
  );
};

export default PaymentForm;
