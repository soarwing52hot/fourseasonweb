import { useState, useEffect } from "react";
import { Button, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper, Chip } from "@mui/material";
import { useSelector } from "react-redux";
import { getPaymentList, putPaymentConfirm, sendInvoiceMail } from "@/Services/Finance";
import { postUser, patchUser } from "@/Services/User";
import { PaymentMapping } from "@/Utilities/Maps";

import usePagination from "@/hooks/usePagination";
import PageButton from "@/Components/PageButton";

const PaymentList = () => {
  const user = useSelector(state => state.user);
  const [Payments, setPayments] = useState([]);
  const { pages, setNewPages } = usePagination({});

  const GetPaymentList = async (page = 1) => {
    let result = await getPaymentList(page);

    let data = result.data;
    setPayments(data.results);
    setNewPages(data);
  };

  useEffect(() => {
    GetPaymentList();
  }, []);

  function createRows(e) {
    let btnText = "確認匯款";
    let confirmStatus = true;
    let btnColor = "primary";

    if (e.confirmed) {
      btnText = "取消確認";
      confirmStatus = false;
      btnColor = "error";
    }
    return (
      <TableRow key={e.id}>
        <TableCell>{e.id}</TableCell>
        <TableCell>{e.name}</TableCell>
        <TableCell>{e.email}</TableCell>
        <TableCell>{e.phone}</TableCell>
        <TableCell>{e.transfer_date}</TableCell>
        <TableCell>{e.account_number}</TableCell>
        <TableCell>{e.type != PaymentMapping.newMember && e.memo}</TableCell>
        <TableCell>{e.confirmed && <Chip color="secondary" label="匯款已確認" />}</TableCell>
        <TableCell>
          <Button className="btn-paymentlist" variant="contained" color={btnColor} onClick={() => confirmPayment(e.id, confirmStatus)}>
            {btnText}
          </Button>
        </TableCell>
        <TableCell>
          <Button className="btn-paymentlist" variant="contained" color="primary" onClick={sendMail}>
            寄送
          </Button>
        </TableCell>

        <TableCell>{actionBtn(e)}</TableCell>
      </TableRow>
    );
  }

  function actionBtn(rowData) {
    if (rowData.type === PaymentMapping.newMember) {
      return (
        <Button className="btn-paymentlist" variant="contained" color="secondary" disabled={!rowData.confirmed} onClick={() => createUser(rowData)}>
          新建會員
        </Button>
      );
    } else if (rowData.type === PaymentMapping.oldMember) {
      let data = { paid_this_year: true };
      return (
        <Button
          className="btn-paymentlist"
          variant="contained"
          color="success"
          onClick={() => {
            patchUser(rowData.member, data);
          }}
        >
          會員新年度會籍確認
        </Button>
      );
    }
  }

  function confirmPayment(id, confirmStatus) {
    let temp = {
      confirmed: confirmStatus,
      confirm_person: user.membership_number
    };
    putPaymentConfirm(id, temp)
      .then(rsp => {
        GetPaymentList();
      })
      .catch(err => {
        alert(err);
        console.log(err);
      });
  }

  function createUser(e) {
    let data = JSON.parse(e.memo);
    data.password = data.citizen_id;
    postUser(data)
      .then(res => alert("建立成功"))
      .catch(err => {
        alert("建立失敗 應該已經建立過了唷");
      });
  }

  function sendMail(id) {
    sendInvoiceMail(id)
      .then(rsp => {
        alert("發票寄送成功");
      })
      .catch(err => {
        alert("發票寄送失敗");
      });
  }

  return (
    <>
      <div className="content-area">
        <h1 className="title-bar">匯款確認</h1>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 650 }} className="namelist-table">
            <TableHead>
              <TableRow>
                <TableCell>#</TableCell>
                <TableCell>姓名</TableCell>
                <TableCell>信箱</TableCell>
                <TableCell>電話</TableCell>
                <TableCell>匯款日期</TableCell>
                <TableCell>帳號後五碼</TableCell>
                <TableCell>備註</TableCell>
                <TableCell>狀態</TableCell>
                <TableCell>財務確認</TableCell>
                <TableCell>寄送發票</TableCell>
                <TableCell>動作</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{Payments.map(createRows)}</TableBody>
          </Table>
        </TableContainer>
        <PageButton pages={pages} func={GetPaymentList} />
      </div>
    </>
  );
};

export default PaymentList;
