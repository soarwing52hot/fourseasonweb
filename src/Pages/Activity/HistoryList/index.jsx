import { useState } from "react";
import { Grid, TextField, Button } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

import ActivityCard from "@/Components/ActivityView/ActivityCard";
import moment from "moment-timezone";

import { useSelector } from "react-redux";
import { getActivityList, getActivityListWithToken } from "@/Services/Activities";

export default function HistoricalActivityList() {
  const user = useSelector(state => state.user);
  const [Activities, setActivities] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [DateSearch, setDateSearch] = useState({});

  async function GetActivityList() {
    let start = moment(DateSearch.dateStart).format("YYYY-MM-DD");
    if (start == "Invalid date") {
      start = undefined;
    }
    let end = moment(DateSearch.dateEnd).format("YYYY-MM-DD");
    if (end == "Invalid date") {
      end = undefined;
    }

    let data = { title: keyword, trip_date_start: start, trip_date_end: end };
    let res;

    if (user) {
      res = await getActivityListWithToken(data);
    } else {
      res = await getActivityList(data);
    }
    let results = res.data.results;

    setActivities(results);
  }

  return (
    <>
      <div className="content-area">
        <h1 className="title-bar">歷史活動查詢</h1>
        <Grid container columns={{ xs: 12, md: "auto" }} spacing={2}>
          <Grid item xs={12} md="auto">
            <DatePicker
              label="開始日期"
              value={moment(DateSearch?.dateStart)}
              onChange={newValue =>
                setDateSearch(prevState => ({
                  ...prevState,
                  dateStart: newValue
                }))
              }
            />
          </Grid>
          <Grid item xs={12} md="auto">
            <DatePicker
              label="結束日期"
              value={moment(DateSearch?.dateEnd)}
              onChange={newValue =>
                setDateSearch(prevState => ({
                  ...prevState,
                  dateEnd: newValue
                }))
              }
            />
          </Grid>
          <Grid item xs={12} md="auto">
            <TextField label="搜尋隊伍名稱" variant="outlined" value={keyword} onChange={e => setKeyword(e.target.value)} />
          </Grid>
          <Grid item xs={12} md="auto">
            <Button variant="contained" color="success" onClick={GetActivityList}>
              搜尋
            </Button>
          </Grid>
        </Grid>
        <br />
        <Grid container columns={{ xs: 1, md: 4 }} spacing={{ sm: 0, md: 2 }}>
          {Activities.map(e => (
            <ActivityCard activity={e} key={e.id} />
          ))}
        </Grid>
      </div>
    </>
  );
}
