import { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import Info from "./Components/Info";
import LeaderFunc from "./Components/LeaderFunc";
import LeaderTable from "./Components/LeaderTable";
import ParticipantTable from "./Components/ParticipantTable";
import EmergencyConductorFunc from "./Components/EmergencyConductorFunc";
import EmergencyConductorTable from "./Components/EmergencyConductorTable";
import { ActivityContext, UserStateContext } from "./Components/ActivityContext";
import { Box, Typography, CircularProgress } from "@mui/material";

import { getActivity, getActivityWithUser, registerActivitiesParticipant, getMemberLog } from "@/Services/Activities";

export default function ActivityDetail() {
  let { id } = useParams();
  const user = useSelector(state => state.user);

  const [Activity, setActivity] = useState({});
  const [loading, setLoading] = useState(false);

  const [userState, setUserState] = useState({
    isLeader: false,
    isEmergencyConductor: false,
    isParticipant: false
  });

  useEffect(() => {
    _getActivityDetail();
  }, []);

  async function _getActivityDetail() {
    if (!user.isLoggedin) {
      let res = await getActivity(id);
      setActivity(res.data);
      return;
    }
    let res = await getActivityWithUser(id);
    let logs = await getAllMemberlog(id);

    res.data.leaders.forEach(e => {
      let log = logs.find(x => x.member_number.membership_number == e.membership_number);
      e.log = log;
    });

    res.data.participants.forEach(e => {
      let log = logs.find(x => x.member_number.membership_number == e.membership_number);
      e.log = log;
    });
    setActivity(res.data);
    checkUserState(res.data);
    setLoading(false);
  }

  function checkUserState(activity) {
    let isLeader = activity.leaders?.some(e => e.membership_number === user.membership_number);
    let isParticipant = activity.participants?.some(e => e.membership_number === user.membership_number);
    let isEmergencyConductor = activity.emergency_conductor?.some(e => e.membership_number === user.membership_number);
    let temp = { isLeader, isParticipant, isEmergencyConductor };
    setUserState(temp);
  }

  async function getAllMemberlog(activityId) {
    if (!activityId) return;
    let result = [];

    let data = { id: activityId, page: 1 };

    let response = await getMemberLog(data);
    result = result.concat(response.data.results);

    while (response.data.next) {
      data.page += 1;
      response = await getMemberLog(data);
      result = result.concat(response.data.results);
    }
    return result;
  }

  async function multipleRegister(requestList) {
    if (requestList.length == 0) return;
    setLoading(true);
    try {
      let promiseList = [];
      requestList.forEach(req => {
        let promise = registerActivitiesParticipant(id, req);
        promiseList.push(promise);
      });

      await Promise.all(promiseList);

      _getActivityDetail();
    } catch (e) {
      setLoading(false);
    }
  }

  return (
    <ActivityContext.Provider value={Activity}>
      <UserStateContext.Provider value={userState}>
        <div className="content-area">
          {loading && (
            <div className="loading-container">
              <Box sx={{ position: "relative", display: "inline-flex" }}>
                <CircularProgress className="loading" size={100} />
                <Box
                  sx={{
                    top: 0,
                    left: 0,
                    bottom: 0,
                    right: 0,
                    position: "absolute",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Typography variant="caption" component="div" color="text.secondary">
                    Loading
                  </Typography>
                </Box>
              </Box>
            </div>
          )}
          <Info multipleRegister={multipleRegister} />
          {userState.isLeader ? <LeaderFunc multipleRegister={multipleRegister} /> : null}
          {userState.isEmergencyConductor && <EmergencyConductorFunc />}
          <h3>領隊名單</h3>
          <LeaderTable multipleRegister={multipleRegister} />
          <h3>留守人名單</h3>
          <p>非會員留守人可填寫在活動敘述，但是就看不到隊員緊急聯絡資訊</p>
          <EmergencyConductorTable multipleRegister={multipleRegister} />
          <h3>隊員名單</h3>
          <ParticipantTable multipleRegister={multipleRegister} />
        </div>
      </UserStateContext.Provider>
    </ActivityContext.Provider>
  );
}
