import PropTypes from "prop-types";
import { useState } from "react";
import { IdentityType } from "@/Utilities/Maps";
import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Grid,
  IconButton,
  Table,
  TableCell,
  TableContainer,
  TableHead,
  TableBody,
  TableRow,
  Paper,
  TextField
} from "@mui/material";

import CloseIcon from "@mui/icons-material/Close";

export default function ModalCar(props) {
  ModalCar.propTypes = {
    modalShow: PropTypes.bool,
    editCar: PropTypes.func,
    nameList: PropTypes.array
  };

  const [arrangeCarList, setArrangeCarList] = useState([]);

  async function closeModal(sendChange) {
    let requestList = [];
    // 有確認安排車次才送去紀錄
    if (sendChange) {
      arrangeCarList.forEach(e => {
        let request = {
          membership_numbers: [e.membership_number],
          register: undefined,
          identity_type: e.identity_type,
          car: parseInt(e.car)
        };
        requestList.push(request);
      });
    }
    props.editCar(requestList);
  }

  function row(userData) {
    return (
      <TableRow key={userData.membership_number}>
        <TableCell align="center">{userData.membership_number}</TableCell>
        <TableCell align="center">{userData.username}</TableCell>
        <TableCell align="center">
          <TextField type="number" min={0} defaultValue={userData.log?.car} onChange={e => addArrangeCarList(e.target.value, userData)} l variant="outlined" />
        </TableCell>
      </TableRow>
    );
  }

  function addArrangeCarList(carNumber, userData) {
    let newCarList = Object.assign([], arrangeCarList);
    let log = newCarList.find(e => e.membership_number == userData.membership_number);

    if (log === undefined) {
      log = {
        membership_number: userData.membership_number,
        identity_type: IdentityType.Participant,
        car: 0
      };

      newCarList.push(log);
    }

    log.car = carNumber;

    setArrangeCarList(newCarList);
  }

  return (
    <Dialog open={props.modalShow} onClose={() => closeModal(false)}>
      <DialogTitle sx={{ m: 0, p: 2 }}>安排車次</DialogTitle>
      <IconButton
        aria-label="close"
        onClick={() => closeModal(false)}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: theme => theme.palette.grey[500]
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent>
        <TableContainer component={Paper} className="namelist-table">
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>會員編號</TableCell>
                <TableCell align="center">姓名</TableCell>
                <TableCell>車次</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{props.nameList.map(row)}</TableBody>
          </Table>
        </TableContainer>
      </DialogContent>
      <DialogActions>
        <Grid container justifyContent="flex-end">
          <Grid item>
            <Button className="mx-1" variant="outlined" color="error" onClick={() => closeModal(false)}>
              離開
            </Button>
            <Button variant="contained" onClick={() => closeModal(true)}>
              確認
            </Button>
          </Grid>
        </Grid>
      </DialogActions>
    </Dialog>
  );
}
