import { useState, useContext } from "react";
import PropTypes from "prop-types";
import { Button, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper } from "@mui/material";
import { BasicConfirm, confirmationModalTemplate } from "@/Components/Modals";
import moment from "moment-timezone";
import { IdentityType } from "@/Utilities/Maps";
import { ActivityContext, UserStateContext } from "./ActivityContext";

export default function LeaderTable(props) {
  LeaderTable.propTypes = {
    userState: PropTypes.object,
    sendRegisterRequest: PropTypes.func,
    multipleRegister: PropTypes.func
  };
  const Activity = useContext(ActivityContext);
  const userState = useContext(UserStateContext);

  const [modalRemoveLeader, setModalRemoveLeader] = useState(confirmationModalTemplate);
  const [modalRemoveLeaderShow, setModalRemoveLeaderShow] = useState(false);

  const styleSticky = { position: "sticky", left: 0, background: "white", minWidth: "80px", zIndex: 900 };
  function leaderRows(row) {
    let logData = row.log;

    let formatedDate = "無資料";
    if (logData?.join_date) {
      formatedDate = moment(logData?.join_date).format("YYYY/MM/DD HH:mm:ss");
    }
    return (
      <TableRow key={row.membership_number}>
        <TableCell align="center" style={styleSticky}>
          {row.username}
        </TableCell>
        <TableCell align="center">{row.membership_number}</TableCell>
        <TableCell align="center">{formatedDate}</TableCell>
        {userState.isLeader && (
          <TableCell align="center" style={{ minWidth: 100 }}>
            <Button key={row.membership_number} onClick={() => removeLeader(row)} variant="outlined" color="error">
              退出領隊群
            </Button>
          </TableCell>
        )}
        <TableCell align="center">{logData?.car}</TableCell>
        <TableCell align="center">{row.email ? row.email : "無"}</TableCell>
        <TableCell align="center">{row.phone ? row.phone : "無"}</TableCell>
        <TableCell align="center">{row.land_line ? row.land_line : "無"}</TableCell>
      </TableRow>
    );
  }

  function removeLeader(leaderData) {
    const closeRemoveModal = async state => {
      if (state) {
        let requestList = [{ membership_numbers: [leaderData.membership_number], register: false, identity_type: IdentityType.Leader }];
        props.multipleRegister(requestList);
      }

      setModalRemoveLeaderShow(false);
    };
    let template = {
      modalTitle: "確認移除領隊",
      modalContent: `確定要移除該領隊 (${leaderData.username})?`,
      action: closeRemoveModal
    };

    setModalRemoveLeader(template);
    setModalRemoveLeaderShow(true);
  }

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 650 }} aria-label="leader table">
          <TableHead>
            <TableRow>
              <TableCell style={styleSticky}>姓名</TableCell>
              <TableCell align="center">會員編號</TableCell>
              <TableCell align="center">加入日期</TableCell>
              {userState.isLeader && <TableCell align="center">動作</TableCell>}
              <TableCell align="center">車次</TableCell>
              <TableCell align="center">信箱</TableCell>
              <TableCell align="center">手機</TableCell>
              <TableCell align="center">市話</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>{Activity?.leaders?.map(leaderRows)}</TableBody>
        </Table>
      </TableContainer>
      <BasicConfirm title={modalRemoveLeader.modalTitle} content={modalRemoveLeader.modalContent} modalShow={modalRemoveLeaderShow} action={modalRemoveLeader.action} />
    </>
  );
}
