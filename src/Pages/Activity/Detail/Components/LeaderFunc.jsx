import { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import { Grid, Card, CardContent, Button, Box } from "@mui/material";
import { getInsurance, getNameList, postSendEmail } from "@/Services/Activities";
import { getUsersGroups } from "@/Services/User";
import { ModalSendEmail } from "@/Components/Modals";
import { IdentityType } from "@/Utilities/Maps";
import ModalAdd from "./ModalAdd";
import ModalCar from "./ModalCar";
import PersonalDetailList from "./PersonalDetailList";
import { ActivityContext } from "./ActivityContext";
import { useNavigate } from "react-router";

import moment from "moment-timezone";

export default function LeaderFunc(props) {
  LeaderFunc.propTypes = { multipleRegister: PropTypes.func };
  const history = useNavigate();
  const multipleRegister = props.multipleRegister;

  const Activity = useContext(ActivityContext);

  const everyone = Activity.leaders?.concat(Activity.participants);
  const participantList = Activity.participants.map(e => e.membership_number);
  const leaderList = Activity.leaders.map(e => e.membership_number);

  const [users, setUsers] = useState([]);
  const guides = users.filter(e => e.groups.find(g => g.name == "Leader") && !isExistingMember(e.membership_number));
  const memberOnly = users.filter(e => !isExistingMember(e.membership_number));

  const [includeUnpaid, setIncludUnpaid] = useState(false);

  const [showSendEmailModal, setShowSendEmailModal] = useState(false);
  const defaultEmailValue = {
    title: `${Activity.title} 活動公告`,
    content: ""
  };

  const [showAddLeaderModal, setShowLeaderModal] = useState(false);
  const [showAddEmergencyConductorModal, setShowEmergencyConductorModal] = useState(false);
  const [showAddMemberModal, setShowMemberModal] = useState(false);
  const [showCarModal, setShowCarModal] = useState(false);

  useEffect(() => {
    getUserList();
  }, [includeUnpaid]);

  function isExistingMember(membership_number) {
    if (participantList.includes(membership_number)) {
      return true;
    }
    if (leaderList.includes(membership_number)) {
      return true;
    }
    return false;
  }

  async function getUserList() {
    let key = "paid";
    if (includeUnpaid == true) {
      key = "all";
    }
    let userList = [];
    let page = 1;
    let res = await getUsersGroups(key);
    // 通常不會去打勾 所以每次重抓
    userList = userList.concat(res.data.results);
    while (res.data.next) {
      page += 1;
      res = await getUsersGroups(key, page);
      userList = userList.concat(res.data.results);
    }

    setUsers(userList);
  }
  async function _downloadInsurance() {
    await getInsurance(Activity.id, `保險名冊-${Activity.title}-${moment().format("yyyy-MM-DD")}.xlsx`);
  }
  async function _downloadNameList() {
    await getNameList(Activity.id, `人員名冊-${Activity.title}-${moment().format("yyyy-MM-DD")}.xlsx`);
  }

  async function addEmergencyConductor(nameList) {
    setIncludUnpaid(false);
    if (nameList.length == 0) {
      setShowEmergencyConductorModal(false);
      return;
    }

    let requestList = [
      {
        register: true,
        membership_numbers: nameList,
        identity_type: IdentityType.EmergencyConductor
      }
    ];

    props.multipleRegister(requestList);
    setShowEmergencyConductorModal(false);
  }

  async function addMemeber(nameList) {
    setIncludUnpaid(false);
    if (nameList.length == 0) {
      setShowMemberModal(false);
      return;
    }
    let requestList = [
      {
        register: true,
        membership_numbers: nameList,
        identity_type: IdentityType.Participant
      }
    ];

    props.multipleRegister(requestList);
    setShowMemberModal(false);
  }

  async function addLeader(nameList) {
    setIncludUnpaid(false);
    if (nameList.length == 0) {
      setShowLeaderModal(false);
      return;
    }
    let requestList = [
      {
        register: true,
        membership_numbers: nameList,
        identity_type: IdentityType.Leader
      }
    ];

    props.multipleRegister(requestList);
    setShowLeaderModal(false);
  }

  function sendEmail(state, data) {
    setShowSendEmailModal(false);
    if (state == false) {
      return;
    }
    data["activity"] = Activity.id;
    postSendEmail(data);
  }

  async function editCar(requestList) {
    setShowCarModal(false);

    multipleRegister(requestList);
  }

  function goToEdit() {
    history(`/Activity/Form/${Activity.id}/`);
  }

  return (
    <Box sx={{ my: 2 }}>
      <Card>
        <CardContent>
          <h3>領隊功能</h3>
          <Grid container spacing={1}>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" onClick={() => setShowLeaderModal(true)}>
                新增領隊
              </Button>
            </Grid>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" color="success" onClick={() => setShowEmergencyConductorModal(true)}>
                新增留守人
              </Button>
            </Grid>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" color="secondary" onClick={() => setShowMemberModal(true)}>
                新增隊員
              </Button>
            </Grid>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" color="error" onClick={goToEdit}>
                修改行程
              </Button>
            </Grid>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" sx={{ color: "white", backgroundColor: "rgb(89, 79, 59)" }} onClick={_downloadInsurance}>
                列印保險名冊
              </Button>
            </Grid>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" sx={{ color: "white", backgroundColor: "rgb(119, 98, 88)" }} onClick={_downloadNameList}>
                列印人員名冊
              </Button>
            </Grid>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" sx={{ color: "black", backgroundColor: "rgb(156, 124, 165)" }} onClick={() => setShowSendEmailModal(true)}>
                寄送Email
              </Button>
            </Grid>
            <Grid className="m-1" sm="auto" item>
              <Button variant="contained" sx={{ color: "black", backgroundColor: "rgb(48, 188, 237)" }} onClick={() => setShowCarModal(true)}>
                安排車次
              </Button>
            </Grid>
          </Grid>
          <PersonalDetailList nameList={everyone} />
        </CardContent>
      </Card>
      <ModalSendEmail title={"寄送活動通知"} modalShow={showSendEmailModal} action={sendEmail} defaultValue={defaultEmailValue} />
      <ModalAdd
        key="addLeader"
        title="新增領隊"
        modalShow={showAddLeaderModal}
        action={addLeader}
        nameList={guides}
        includeUnpaid={includeUnpaid}
        setIncludUnpaid={setIncludUnpaid}
      />
      <ModalAdd
        key="addMember"
        title="新增隊員"
        modalShow={showAddMemberModal}
        action={addMemeber}
        nameList={memberOnly}
        includeUnpaid={includeUnpaid}
        setIncludUnpaid={setIncludUnpaid}
      />
      <ModalAdd
        key="addEmergencyConductor"
        title="新增留守人"
        modalShow={showAddEmergencyConductorModal}
        action={addEmergencyConductor}
        nameList={memberOnly}
        includeUnpaid={includeUnpaid}
        setIncludUnpaid={setIncludUnpaid}
      />
      <ModalCar modalShow={showCarModal} editCar={editCar} nameList={everyone} />
    </Box>
  );
}
