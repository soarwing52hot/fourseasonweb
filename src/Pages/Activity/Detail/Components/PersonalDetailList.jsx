import PropTypes from "prop-types";

import { Accordion, AccordionSummary, AccordionDetails, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

export default function PersonalDetailList(props) {
  PersonalDetailList.propTypes = {
    nameList: PropTypes.array
  };

  function dataRows(row) {
    return (
      <TableRow key={row.membership_number}>
        <TableCell align="center">{row.username}</TableCell>
        <TableCell align="center">{row.membership_number}</TableCell>
        <TableCell align="center">{row.gender_display}</TableCell>
        <TableCell align="center">{row.birth_date}</TableCell>
        <TableCell align="center">{row.citizen_id}</TableCell>
        <TableCell align="center">{row.phone ? row.phone : "無手機"}</TableCell>
        <TableCell align="center">{row.email ? row.email : "無信箱"}</TableCell>
        <TableCell align="center">{row.land_line ? row.land_line : "無市話"}</TableCell>
        <TableCell align="center">{row.address}</TableCell>
        <TableCell align="center">{row.emergency_contact_name}</TableCell>
        <TableCell align="center">{row.emergency_contact_phone}</TableCell>
        <TableCell align="center">{row.emergency_contact_relation}</TableCell>
      </TableRow>
    );
  }
  return (
    <Accordion defaultExpanded>
      <AccordionSummary expandIcon={<ExpandMoreIcon />} aria-controls="panel1-content" id="panel1-header">
        人員詳細資料
      </AccordionSummary>
      <AccordionDetails>
        <TableContainer component={Paper} elevation={3}>
          <Table sx={{ minWidth: 650 }}>
            <TableHead>
              <TableRow>
                <TableCell align="center">姓名</TableCell>
                <TableCell>會員編號</TableCell>
                <TableCell>性別</TableCell>
                <TableCell>出生日期</TableCell>
                <TableCell>身分證字號</TableCell>
                <TableCell>手機</TableCell>
                <TableCell>Email</TableCell>
                <TableCell>市話</TableCell>
                <TableCell style={{ minWidth: "100px" }}>地址</TableCell>
                <TableCell>緊急聯絡人</TableCell>
                <TableCell>緊急聯絡人電話</TableCell>
                <TableCell>關係</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{props.nameList?.map(dataRows)}</TableBody>
          </Table>
        </TableContainer>
      </AccordionDetails>
    </Accordion>
  );
}
