import { useContext } from "react";
import { ActivityContext } from "./ActivityContext";
import { Card, CardContent } from "@mui/material";
import PersonalDetailList from "./PersonalDetailList";

export default function EmergencyConductorFunc() {
  const Activity = useContext(ActivityContext);
  const everyone = Activity.leaders?.concat(Activity.participants);
  return (
    <Card>
      <CardContent>
        <h3>留守人功能</h3>
        <PersonalDetailList nameList={everyone} />
      </CardContent>
    </Card>
  );
}
