import PropTypes from "prop-types";
import { useState } from "react";
import { Button, Dialog, DialogTitle, DialogContent, DialogActions, Grid, IconButton, Checkbox, FormControlLabel } from "@mui/material";

export default function ModalAdd(props) {
  ModalAdd.propTypes = {
    action: PropTypes.func,
    modalShow: PropTypes.bool,
    title: PropTypes.string,
    nameList: PropTypes.array,
    includeUnpaid: PropTypes.bool,
    setIncludUnpaid: PropTypes.func
  };

  const [userList, setUserList] = useState([]);

  function changeClick(member_number) {
    const num = parseInt(member_number);
    let newList = [];
    if (userList.includes(num)) {
      newList = userList.filter(e => e != num);
    } else {
      newList = [...userList, num];
    }
    setUserList(newList);
  }

  function checkList(user) {
    return (
      <Grid item xs={6} md={4} key={`${user.member_number}-grid`}>
        <FormControlLabel
          key={`${user.member_number}-formlabel`}
          control={<Checkbox key={`${user.member_number}-checkbox`} name={user.membership_number} onChange={() => changeClick(user.membership_number)} />}
          label={`${user.membership_number} ${user.username}`}
        />
      </Grid>
    );
  }

  function closeModal(state) {
    if (state === false) {
      props.action([]);
      return;
    }

    props.action(userList);
  }
  return (
    <>
      <Dialog open={props.modalShow} onClose={() => closeModal(false)} fullWidth>
        <DialogTitle>添加{props.title}</DialogTitle>
        <IconButton
          aria-label="close"
          onClick={() => closeModal(false)}
          sx={{
            position: "absolute",
            right: 8,
            top: 8,
            color: theme => theme.palette.grey[500]
          }}
        />
        <DialogContent className="modal-add-list" dividers>
          <Grid container>{props.nameList.map(e => checkList(e))}</Grid>
        </DialogContent>

        <DialogActions>
          <Grid container justifyContent="space-between" spacing={2}>
            <Grid item>
              <FormControlLabel control={<Checkbox onChange={() => props.setIncludUnpaid(!props.includeUnpaid)} />} label="包含沒繳會費會員" />
            </Grid>
            <Grid item>
              <Button sx={{ mx: 1 }} variant="outlined" color="error" onClick={() => closeModal(false)}>
                離開
              </Button>
              <Button sx={{ mx: 1 }} variant="contained" onClick={() => closeModal(true)}>
                確認
              </Button>
            </Grid>
          </Grid>
        </DialogActions>
      </Dialog>
    </>
  );
}
