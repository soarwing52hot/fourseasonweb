import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";
import { tokenGetActivityRecord } from "@/Services/Activities";
import { getCampRecord } from "@/Services/Camps";
import CloseIcon from "@mui/icons-material/Close";
import { Button, Dialog, DialogTitle, DialogContent, IconButton, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper } from "@mui/material";

function ModalCampRecord(props) {
  ModalCampRecord.propTypes = {
    action: PropTypes.func,
    modalShow: PropTypes.bool,
    modalRecordType: PropTypes.string,
    membershipNumber: PropTypes.number
  };
  const [recordContent, setRecordContent] = useState([]);

  async function showCampHistory() {
    let response = await getCampRecord(props.membershipNumber);
    let data = response.data;
    setRecordContent(data);
  }

  async function showActivityHistory() {
    let data = {
      membership_number: props.membershipNumber
    };
    let response = await tokenGetActivityRecord(data);
    let res = response.data;

    setRecordContent(res.results);
  }

  useEffect(() => {
    if (props.modalRecordType === "營隊") {
      showCampHistory();
    } else if (props.modalRecordType === "活動") {
      showActivityHistory();
    }
  }, [props.modalRecordType, props.membershipNumber]);

  function closeModal(state) {
    props.action(state);
  }

  function tableBody(e) {
    return (
      <TableRow key={e.id}>
        <TableCell>{e.title}</TableCell>
        <TableCell>{e.trip_date_start}</TableCell>
        <TableCell>{e.leaders.map(l => l.username).join(",")}</TableCell>
      </TableRow>
    );
  }

  return (
    <Dialog open={props.modalShow} onClose={() => closeModal(false)} fullWidth>
      <DialogTitle>{props.modalRecordType}參加紀錄</DialogTitle>
      <IconButton
        aria-label="close"
        onClick={() => closeModal(false)}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: theme => theme.palette.grey[500]
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent>
        <TableContainer component={Paper} className="namelist-table">
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>{props.modalRecordType}名稱</TableCell>
                <TableCell>日期</TableCell>
                <TableCell>領隊</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{recordContent.map(e => tableBody(e))}</TableBody>
          </Table>
        </TableContainer>
        {props.modalRecordType == "活動" ? (
          <Button as={Link} to={`/User/ActivityHistory/${props.membershipNumber}/`} className="m-1">
            活動紀錄詳細頁面
          </Button>
        ) : null}
      </DialogContent>
    </Dialog>
  );
}

export { ModalCampRecord };
