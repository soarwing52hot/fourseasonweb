import { useState, useEffect, useContext } from "react";
import PropTypes from "prop-types";
import { Button, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper, Grid, IconButton } from "@mui/material";
import { ModalCampRecord } from "./ModalCampRecord";
import { BasicConfirm, confirmationModalTemplate } from "@/Components/Modals";
import { IdentityType } from "@/Utilities/Maps";
import ArrowDownwardIcon from "@mui/icons-material/ArrowDownward";
import moment from "moment-timezone";
import { ActivityContext, UserStateContext } from "./ActivityContext";

export default function ParticipantTable(props) {
  ParticipantTable.propTypes = {
    multipleRegister: PropTypes.func
  };

  const Activity = useContext(ActivityContext);
  const userState = useContext(UserStateContext);

  const participants = Activity.participants;

  const [modalMembershipNumber, setModalMembershipNumber] = useState(0);
  const [modalRemoveParticipant, setModalRemoveParticipant] = useState(confirmationModalTemplate);
  const [modalRemoveParticipantShow, setModalRemoveParticipantShow] = useState(false);
  const [modalRecordShow, setModalRecordShow] = useState(false);
  const [modalRecordType, setModalRecordType] = useState("");
  const [sortedMembers, setSortedMembers] = useState(participants);

  useEffect(() => {
    if (participants?.length > 0) {
      setSortedMembers(Activity.participants);
    }
  }, [Activity.participants]);

  const sortByColumn = columnName => {
    const sorted = [...sortedMembers].sort((a, b) => {
      let columnA;
      let columnB;
      if (columnName === "car") {
        columnA = a.log?.car;
        columnB = b.log?.car;
      } else if (columnName == "membership_number") {
        columnA = a[columnName];
        columnB = b[columnName];
      } else if (columnName == "join_date") {
        columnA = new Date(a.log?.join_date);
        columnB = new Date(b.log?.join_date);
      } else {
        columnA = (a[columnName] || "").toString().toLowerCase();
        columnB = (b[columnName] || "").toString().toLowerCase();
      }

      if (columnA < columnB) return -1;
      if (columnA > columnB) return 1;
      return 0;
    });

    setSortedMembers(sorted);
  };

  async function showCampHistory(e) {
    setModalRecordType("營隊");
    setModalRecordShow(true);
    setModalMembershipNumber(e.membership_number);
  }

  async function showActivityHistory(e) {
    setModalRecordType("活動");
    setModalMembershipNumber(e.membership_number);
    setModalRecordShow(true);
  }

  function closeModalCampRecord() {
    setModalRecordShow(false);
  }
  const styleSticky = { position: "sticky", left: 0, background: "white", minWidth: "80px", zIndex: 900 };

  function participantList(row) {
    let logData = row.log;

    let formatedDate = "無資料";
    if (logData) {
      let temp = moment(logData?.join_date);
      if (temp.isValid()) {
        formatedDate = temp.format("YYYY/MM/DD HH:mm:ss");
      }
    }

    return (
      <TableRow key={row.membership_number}>
        <TableCell align="center" style={styleSticky}>
          {row.username}
        </TableCell>
        <TableCell align="center">{row.membership_number}</TableCell>
        <TableCell align="center">{formatedDate}</TableCell>
        <TableCell align="center">{logData?.car ?? 0}</TableCell>
        {userState.isLeader && (
          <TableCell align="center">
            <Grid container spacing={2}>
              <Grid item xs={12} md="auto">
                <Button variant="outlined" color="error" key={row.membership_number} onClick={() => removeParticipant(row.membership_number)}>
                  退出隊伍
                </Button>
              </Grid>
              <Grid item xs={12} md="auto">
                <Button variant="contained" key={row.membership_number} onClick={() => showCampHistory(row)}>
                  營隊參加紀錄
                </Button>
              </Grid>
              <Grid item xs={12} md="auto">
                <Button variant="contained" color="info" key={row.membership_number} onClick={() => showActivityHistory(row)}>
                  活動參加紀錄
                </Button>
              </Grid>
            </Grid>
          </TableCell>
        )}
      </TableRow>
    );
  }

  function removeParticipant(membership_number) {
    const closeRemoveModal = state => {
      if (state) {
        let requestList = [
          {
            register: false,
            membership_numbers: [membership_number],
            identity_type: IdentityType.Participant
          }
        ];
        props.multipleRegister(requestList);
      }

      setModalRemoveParticipantShow(false);
    };
    let template = {
      buttonText: "退出隊伍",
      modalTitle: "確認移除隊員",
      modalContent: "確定要移除該隊員？",
      action: closeRemoveModal
    };

    setModalRemoveParticipant(template);
    setModalRemoveParticipantShow(true);
  }

  return (
    <>
      <TableContainer component={Paper} className="namelist-table">
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow>
              <TableCell align="center" style={styleSticky}>
                姓名
              </TableCell>
              <TableCell>
                <Grid container>
                  <Grid item xs="auto">
                    會員編號
                  </Grid>
                  <Grid item xs={2}>
                    <IconButton size="small" onClick={() => sortByColumn("membership_number")}>
                      <ArrowDownwardIcon fontSize="inherit" />
                    </IconButton>
                  </Grid>
                </Grid>
              </TableCell>

              <TableCell align="center">
                <Grid container>
                  <Grid item xs="auto">
                    加入日期
                  </Grid>
                  <Grid item xs={2}>
                    <IconButton size="small" onClick={() => sortByColumn("join_date")}>
                      <ArrowDownwardIcon fontSize="inherit" />
                    </IconButton>
                  </Grid>
                </Grid>
              </TableCell>

              <TableCell align="center">車次</TableCell>
              {userState.isLeader && <TableCell align="center">動作</TableCell>}
            </TableRow>
          </TableHead>
          <TableBody>{sortedMembers?.map(participantList)}</TableBody>
        </Table>
      </TableContainer>
      <BasicConfirm
        title={modalRemoveParticipant.modalTitle}
        content={modalRemoveParticipant.modalContent}
        modalShow={modalRemoveParticipantShow}
        action={modalRemoveParticipant.action}
      />
      <ModalCampRecord modalRecordType={modalRecordType} modalShow={modalRecordShow} action={closeModalCampRecord} membershipNumber={modalMembershipNumber} />
    </>
  );
}
