import { createContext } from "react";

export const ActivityContext = createContext({});

export const UserStateContext = createContext({ isLeader: false, isEmergencyConductor: false, isParticipant: false });
