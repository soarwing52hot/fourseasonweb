import { useState, useContext } from "react";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";
import { Box, Grid, Card, CardHeader, CardContent, Button, Divider } from "@mui/material";
import { ActivityContext, UserStateContext } from "./ActivityContext";
import { useBasicInfo } from "@/Components/BasicInfoContext/BasicInfoContext";
import { stampMap, IdentityType } from "@/Utilities/Maps";
import { BasicConfirm, confirmationModalTemplate } from "@/Components/Modals";
import AnnounceById from "@/Pages/Announcement/Components/AnnounceById";

export default function Info(props) {
  Info.propTypes = {
    multipleRegister: PropTypes.func
  };

  const user = useSelector(state => state.user);
  const { stampNameMap } = useBasicInfo();
  const Activity = useContext(ActivityContext);
  const userState = useContext(UserStateContext);

  const stampNamesResult = calStampNamesResult();

  function calStampNamesResult() {
    if (!stampNameMap.size) {
      return [];
    }
    let stampNames = [];
    Activity?.stamp?.forEach(stamp => {
      let name = stampNameMap.get(stamp);
      stampNames.push(name);
    });

    return stampNames;
  }

  function stampImage(e) {
    return <Box fluid="true" key={e} component="img" sx={{ height: "40px", mx: 1 }} alt="stamp" src={stampMap.get(e)} />;
  }
  const [registerModalShow, setRegisterModalShow] = useState(false);
  let registerModal = confirmationModalTemplate;

  setRegisterBtn();

  function showModal() {
    setRegisterModalShow(true);
  }

  function registerActivityModalAction(state, membership_number, register) {
    if (state) {
      let requestList = [
        {
          register: register,
          membership_numbers: [membership_number],
          identity_type: IdentityType.Participant
        }
      ];
      props.multipleRegister(requestList);
    }
    setRegisterModalShow(false);
  }

  function setRegisterBtn() {
    if (userState.isParticipant) {
      registerModal = {
        buttonVariant: "outlined",
        buttonColor: "error",
        buttonText: "退出活動",
        modalTitle: "確認退出活動",
        modalContent: "請問是否確認退出活動？",
        action: state => registerActivityModalAction(state, user.membership_number, false)
      };
    } else {
      registerModal = {
        buttonVariant: "contained",
        buttonColor: "success",
        buttonText: "報名活動",
        modalTitle: "確認報名活動",
        modalContent: "請問是否確認報名活動？",
        action: state => registerActivityModalAction(state, user.membership_number, true)
      };
    }
  }

  const canRegisterActivity = Activity.register_available && !userState.isLeader && user.isLoggedin;
  return (
    <Card sx={{ my: 3 }}>
      <CardHeader title={Activity?.title} />
      <Divider />
      <CardContent>
        <Grid container>
          <Grid sm={12} item>
            {stampNamesResult.map(stampImage)}
          </Grid>
        </Grid>
        <Grid container>
          <Grid sm="auto" item>
            活動日期：
          </Grid>
          <Grid sm="auto" item>
            {Activity?.trip_date_start} ~ {Activity?.trip_date_end}
          </Grid>
        </Grid>
        <Grid container>
          <Grid sm="auto" item>
            活動類型：
          </Grid>
          <Grid sm="auto" item>
            {Activity.activity_type_name}
          </Grid>
        </Grid>

        <Grid container>
          <Grid sm="auto" item>
            報名條件：
          </Grid>
          <Grid sm="auto" item>
            {Activity.activity_requirements_name} : {Activity.activity_requirements_count}次
          </Grid>
        </Grid>
        <Grid container>
          <Grid sm="auto" item>
            活動內容：
          </Grid>
          <Grid sm="auto" item>
            {Activity?.content}
          </Grid>
        </Grid>
        <Grid container>
          <Grid sm={12} md="auto" item>
            活動公告：
          </Grid>
          <Grid sm={12} md={11} item>
            <AnnounceById id={Activity.announcement} />
          </Grid>
        </Grid>
        <Grid container>
          {canRegisterActivity && (
            <Grid sm={12} md="auto" item>
              <Button className="mx-3 mt-1" variant={registerModal.buttonVariant ?? "contained"} color={registerModal.buttonColor ?? "success"} onClick={showModal}>
                {registerModal.buttonText}
              </Button>
              <BasicConfirm title={registerModal.modalTitle} content={registerModal.modalContent} modalShow={registerModalShow} action={registerModal.action} />
            </Grid>
          )}
        </Grid>
      </CardContent>
    </Card>
  );
}
