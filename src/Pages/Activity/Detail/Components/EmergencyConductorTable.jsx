import { useContext } from "react";
import { ActivityContext, UserStateContext } from "./ActivityContext";
import PropTypes from "prop-types";
import { Button, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper } from "@mui/material";
import { IdentityType } from "@/Utilities/Maps";

export default function EmergencyConductorTable(props) {
  EmergencyConductorTable.propTypes = { multipleRegister: PropTypes.func };
  const Activity = useContext(ActivityContext);
  const userState = useContext(UserStateContext);
  const styleSticky = { position: "sticky", left: 0, background: "white", minWidth: "80px", zIndex: 900 };
  async function removeEmergencyConductor(leaderData) {
    let requestList = [
      {
        register: false,
        membership_numbers: [leaderData.membership_number],
        identity_type: IdentityType.EmergencyConductor
      }
    ];
    props.multipleRegister(requestList);
  }

  function dataRows(row) {
    return (
      <TableRow key={row.membership_number}>
        <TableCell align="center" style={styleSticky}>
          {row.username}
        </TableCell>
        <TableCell align="center">{row.membership_number}</TableCell>
        <TableCell align="center">{row.email ? row.email : "無信箱"}</TableCell>
        <TableCell align="center">{row.phone ? row.phone : "無手機"}</TableCell>
        <TableCell align="center">{row.land_line ? row.land_line : "無市話"}</TableCell>
        {userState?.isLeader && (
          <TableCell align="center" style={{ minWidth: 100 }}>
            <Button variant="outlined" color="error" onClick={() => removeEmergencyConductor(row)}>
              移除留守人
            </Button>
          </TableCell>
        )}
      </TableRow>
    );
  }
  return (
    <>
      <TableContainer component={Paper} className="namelist-table">
        <Table sx={{ minWidth: 650 }}>
          <TableHead>
            <TableRow>
              <TableCell align="center" style={styleSticky}>
                姓名
              </TableCell>
              <TableCell>會員編號</TableCell>
              <TableCell>Email</TableCell>
              <TableCell>手機</TableCell>
              <TableCell>市話</TableCell>
              {userState.isLeader && <TableCell align="center">動作</TableCell>}
            </TableRow>
          </TableHead>
          <TableBody>{Activity?.emergency_conductor?.map(dataRows)}</TableBody>
        </Table>
      </TableContainer>
    </>
  );
}
