import { Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper } from "@mui/material";

export default function EmergencyContact() {
  const contactList = [
    { name: "王廷元", machine: "Thuraya SO-2510", number: "+8821668131237" },
    { name: "古佳惠", machine: "Thuraya SO-2510", number: "" },
    { name: "溯溪協會", machine: "Thuraya SO-2510", number: "" }
  ];
  return (
    <div className="content-area">
      <TableContainer component={Paper} sx={{ my: 5 }}>
        <Table sx={{ minWidth: 650 }} aria-label="leader table" className="namelist-table">
          <TableHead>
            <TableRow>
              <TableCell>#</TableCell>
              <TableCell>聯絡人</TableCell>
              <TableCell>機器編號</TableCell>
              <TableCell>衛星電話號碼</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {contactList.map((e, index) => (
              <TableRow key={e.name}>
                <TableCell>{index + 1}</TableCell>
                <TableCell>{e.name}</TableCell>
                <TableCell>{e.machine}</TableCell>
                <TableCell>{e.number}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </div>
  );
}
