import { useState } from "react";
import { Grid, TextField } from "@mui/material";
import ActivityCard from "@/Components/ActivityView/ActivityCard";
import moment from "moment-timezone";
import SearchBar from "@/Components/SearchBar";
import { useSelector } from "react-redux";
import { getActivityList, getActivityListWithToken } from "@/Services/Activities";

export default function ActivityList() {
  const [Activities, setActivities] = useState([]);
  const [keyword, setKeyword] = useState("");
  const [DateSearch, setDateSearch] = useState({});
  const user = useSelector(state => state.user);

  async function GetActivityList() {
    let data = { trip_date_start: moment(DateSearch.dateStart).format("YYYY-MM-DD"), trip_date_end: moment(DateSearch.dateEnd).format("YYYY-MM-DD") };

    let res;

    if (user.isLoggedin) {
      res = await getActivityListWithToken(data);
    } else {
      res = await getActivityList(data);
    }
    let results = res.data.results;
    setActivities(results);
  }

  function searchActivity() {
    if (!keyword) {
      return Activities;
    }
    return Activities.filter(e => e.title.includes(keyword));
  }

  const activityToShow = searchActivity();

  return (
    <div className="content-area">
      <h1 className="title-bar">活動報名</h1>
      <SearchBar DateSearch={DateSearch} setDateSearch={setDateSearch} searchAction={GetActivityList} />
      <br />

      <TextField fullWidth className="m-2" label="搜尋隊伍名稱" variant="outlined" value={keyword} onChange={e => setKeyword(e.target.value)} />

      <Grid container columns={{ xs: 1, md: 4 }} spacing={{ sm: 0, md: 2 }}>
        {activityToShow.map(e => (
          <ActivityCard activity={e} key={e.id} />
        ))}
      </Grid>
    </div>
  );
}
