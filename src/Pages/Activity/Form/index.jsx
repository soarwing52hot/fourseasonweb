import { useState, useEffect, useRef } from "react";
import { useSelector } from "react-redux";
import { useParams, useNavigate } from "react-router-dom";

import { getAnnouncement, postAnnouncement, patchAnnouncement } from "@/Services/Announcement";

import { getActivityWithUser, postActivity, patchActivity } from "@/Services/Activities";
import moment from "moment-timezone";
import ActivityFormik from "./Components/ActivityFormik";

export default function ActivityForm() {
  let { id } = useParams();
  const isNew = id == "new";
  const user = useSelector(state => state.user);
  const ActivityForm = useRef({});
  const history = useNavigate();
  const [Activity, setActivity] = useState({});

  useEffect(() => {
    if (isNew) {
      return;
    }

    _getActivityDetail(id);
  }, []);

  async function _getActivityDetail(id) {
    let res = await getActivityWithUser(id);
    let data = res.data;

    if (data.announcement > 0) {
      let announceResponse = await getAnnouncement(data.announcement);
      data.announce = announceResponse.data;
      data.announceContent = data.announce?.content;
    }
    setActivity(data);
  }

  async function postOrUpdateAnnouncement(data) {
    let request = {
      title: `${data.title}活動公告`,
      content: data.announceContent,
      announce_type: "L"
    };
    let res;
    if (Activity.announcement) {
      res = await patchAnnouncement(Activity.announcement, request);
    } else {
      res = await postAnnouncement(request);
    }

    return res.data.id;
  }
  const initialValue = {
    id: Activity?.id || 0,
    title: Activity?.title || "",
    content: Activity?.content || "",
    active: Activity?.active || false,
    is_test: Activity?.is_test || false,
    is_emergency: Activity?.is_emergency || false,
    can_register: Activity?.can_register || false,
    notice_leader: Activity?.notice_leader || false,
    announcement: Activity?.announcement || undefined,
    announceContent: Activity?.announceContent || "",
    trip_date_start: moment(Activity.trip_date_start),
    trip_date_end: moment(Activity.trip_date_end),
    activity_type: Activity?.activity_type || "",
    activity_requirements: Activity?.activity_requirements || "",
    activity_requirements_count: Activity?.activity_requirements_count || 0,
    participant_limit: Activity?.participant_limit || 0,
    register_end_date: moment(Activity.register_end_date),
    register_end_time: moment(Activity.register_end_date),
    stamp: Activity?.stamp || []
  };

  async function submitForm(data) {
    // 活動取消就把可以報名關掉
    if (!data.active) {
      data.can_register = false;
    }

    data.announcement = await postOrUpdateAnnouncement(data);

    let apiId = id;
    let stamp = data.stamp;
    if (isNew) {
      data.leader = [user.membership_number];
      // 因為many to many 欄位不能直接post的時候加 於是就建立之後再patch data進去
      data.stamp = undefined;
      let res = await postActivity(data);
      apiId = res.data.id;
      data.stamp = stamp;
    }

    await patchActivity(apiId, data);

    if (isNew) {
      alert("建立活動成功");
    } else {
      alert("修改活動成功");
    }
    history(`/Activity/Detail/${apiId}/`);
  }

  return (
    <div className="content-area">
      <h3 className="title-bar">活動基本資料</h3>
      <ActivityFormik initialValue={initialValue} submitForm={submitForm} />
    </div>
  );
}
