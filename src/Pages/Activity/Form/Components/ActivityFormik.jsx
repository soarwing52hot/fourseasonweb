import { useFormik } from "formik";
import * as yup from "yup";
import { Grid, TextField, Button, FormControl, MenuItem, Select, Checkbox, FormControlLabel, Box, Chip, OutlinedInput, Divider, InputLabel } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import moment from "moment-timezone";
import { handleDatetime } from "@/Utilities/FormUtils";
import { useBasicInfo } from "@/Components/BasicInfoContext/BasicInfoContext";
import PropTypes from "prop-types";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

export default function ActivityFormik(props) {
  ActivityFormik.propTypes = { initialValue: PropTypes.object, submitForm: PropTypes.func };
  const isNew = props.initialValue?.id == 0;
  const { requirementOptions, activityTypes, stamps } = useBasicInfo();

  const validationSchema = yup.object({
    title: yup.string().required("必填欄位"),
    content: yup
      .string()
      .required("必填欄位")
      .max(150),
    active: yup.boolean().required("必填欄位"),
    is_test: yup.boolean().required("必填欄位"),
    is_emergency: yup.boolean().required("必填欄位"),
    can_register: yup.boolean().required("必填欄位"),
    notice_leader: yup.boolean().required("必填欄位"),
    announceContent: yup.string().required("必填欄位"),
    trip_date_start: yup.date().required("必填欄位"),
    trip_date_end: yup.date().required("必填欄位"),
    activity_type: yup.string().required("必填欄位"),
    activity_requirements: yup.string().required("必填欄位"),
    activity_requirements_count: yup.number().required("必填欄位"),
    participant_limit: yup.number().required("必填欄位"),
    register_end_date: yup.date().required("必填欄位"),
    register_end_time: yup.date().required("必填欄位")
  });

  const formik = useFormik({
    initialValues: props.initialValue,
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit: values => {
      let data = { ...values };
      const datetimeCol = ["register_end"];
      datetimeCol.forEach(e => handleDatetime(data, e));

      const dateCols = ["trip_date_start", "trip_date_end"];
      dateCols.forEach(e => {
        data[e] = data[e].format("YYYY-MM-DD");
      });

      props.submitForm(data);
    }
  });

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={2} rowSpacing={1}>
        <Grid item xs={3} md={1}>
          活動名稱
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="title"
            name="title"
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.title && Boolean(formik.errors.title)}
          />
        </Grid>
        <Grid item xs={3} md={1}>
          活動類型
        </Grid>
        <Grid item xs={9} md={2}>
          <FormControl fullWidth>
            <Select
              name="activity_type"
              value={formik.values.activity_type}
              onChange={formik.handleChange}
              error={formik.touched.activity_type && Boolean(formik.errors.activity_type)}
            >
              {activityTypes.map(e => (
                <MenuItem value={e.value} key={e.value}>
                  {e.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={4} md={1}>
          活動開始日期
        </Grid>
        <Grid item xs={8} md={2}>
          <DatePicker
            name="trip_date_start"
            value={moment(formik.values.trip_date_start)}
            onChange={e => formik.setFieldValue("trip_date_start", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          活動結束日期
        </Grid>
        <Grid item xs={8} md={2}>
          <DatePicker
            name="trip_date_end"
            value={moment(formik.values.trip_date_end)}
            onChange={e => formik.setFieldValue("trip_date_end", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Grid item xs={0} md={3}></Grid>
        <Grid item xs={12} md={2}>
          <FormControlLabel control={<Checkbox name="active" checked={formik.values.active} onChange={formik.handleChange} />} label="正常開隊" />
        </Grid>
        <Grid item xs={12} md={2}>
          <FormControlLabel control={<Checkbox name="is_emergency" checked={formik.values.is_emergency} onChange={formik.handleChange} />} label="緊急召集" />
        </Grid>
        <Grid item xs={12} md={2}>
          <FormControlLabel control={<Checkbox name="notice_leader" checked={formik.values.notice_leader} onChange={formik.handleChange} />} label="人數異動通知領隊" />
        </Grid>
        <Grid item xs={12} md={2}>
          <FormControlLabel control={<Checkbox name="is_test" checked={formik.values.is_test} onChange={formik.handleChange} />} label="測試活動(不納入統計)" />
        </Grid>
        <Grid item xs={0} md={4}></Grid>
        <Grid item xs={12} md={1}>
          <p>活動簡介</p>
          <span className="text-muted">上限150字</span>
        </Grid>
        <Grid item xs={12} md={11}>
          <TextField
            fullWidth
            multiline
            rows={4}
            id="content"
            name="content"
            value={formik.values.content}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.content && Boolean(formik.errors.content)}
            inputProps={{ maxLength: 150 }}
          />
        </Grid>
        <Grid item xs={12} md={1}>
          <p>活動公告</p>
          <span className="text-muted">上限150字</span>
        </Grid>
        <Grid item xs={12} md={11}>
          <TextField
            fullWidth
            multiline
            rows={4}
            id="announceContent"
            name="announceContent"
            value={formik.values.announceContent}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.announceContent && Boolean(formik.errors.announceContent)}
            inputProps={{ maxLength: 150 }}
          />
        </Grid>
        <Grid item xs={12} md={1}>
          蓋章認證
        </Grid>
        <Grid item xs={12} md={4}>
          <FormControl fullWidth>
            <InputLabel></InputLabel>
            <Select
              fullWidth
              name="stamp"
              multiple
              onChange={formik.handleChange}
              value={formik.values.stamp}
              MenuProps={MenuProps}
              input={<OutlinedInput label="Chip" />}
              renderValue={selected => (
                <Box sx={{ display: "flex", flexWrap: "wrap", gap: 0.5 }}>
                  {selected.map(value => (
                    <Chip key={value} label={stamps.find(e => e.id == value).name} />
                  ))}
                </Box>
              )}
            >
              {stamps.map(e => (
                <MenuItem key={e.id} value={e.id}>
                  {e.name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={12}>
          <Divider />
        </Grid>
        <Grid item xs={12} md={12}>
          <h4 className="title-bar">報名相關</h4>
        </Grid>
        <Grid item xs={12} md={12}>
          <FormControlLabel control={<Checkbox name="can_register" checked={formik.values.can_register} onChange={formik.handleChange} />} label="開放報名" />
        </Grid>
        <Grid item xs={4} md={1}>
          報名截止日期
        </Grid>
        <Grid item xs={8} md={2}>
          <DatePicker
            name="register_end_date"
            value={formik.values.register_end_date}
            onChange={e => formik.setFieldValue("register_end_date", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          報名截止時間
        </Grid>
        <Grid item xs={8} md={2}>
          <TimePicker
            name="register_end_time"
            value={formik.values.register_end_time}
            onChange={e => formik.setFieldValue("register_end_time", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          <p>人數上限</p>
          <span className="text-muted">0為不設限</span>
        </Grid>
        <Grid item xs={8} md={1}>
          <TextField
            fullWidth
            id="participant_limit"
            name="participant_limit"
            type="number"
            value={formik.values.participant_limit}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.participant_limit && Boolean(formik.errors.participant_limit)}
          />
        </Grid>
        <Grid item xs={0} md={4} sx={{ display: { xs: "none", sm: "block" } }}></Grid>
        <Grid item xs={4} md={1}>
          報名資格
        </Grid>
        <Grid item xs={8} md={3}>
          <FormControl fullWidth>
            <Select
              name="activity_requirements"
              value={formik.values.activity_requirements}
              onChange={formik.handleChange}
              error={formik.touched.activity_requirements && Boolean(formik.errors.activity_requirements)}
            >
              {requirementOptions.map(e => (
                <MenuItem value={e.value} key={e.value}>
                  {e.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={4} md={1}>
          次數
        </Grid>
        <Grid item xs={8} md={1}>
          <TextField
            fullWidth
            name="activity_requirements_count"
            type="number"
            value={formik.values.activity_requirements_count}
            onChange={formik.handleChange}
            error={formik.touched.activity_requirements_count && Boolean(formik.errors.activity_requirements_count)}
          />
        </Grid>
        <Grid item xs={12} md={12}>
          <Divider />
        </Grid>
        <Grid item xs={12} md={12}>
          <Button color="primary" variant="contained" fullWidth type="submit">
            {isNew ? "新增活動" : "完成修改"}
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
