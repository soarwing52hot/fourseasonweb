import { Button, Chip, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper } from "@mui/material";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import { updateCampPaymentStatus } from "@/Services/Camps";
import { useSelector } from "react-redux";

export default function FinanceTable({ members, getCampParticipant }) {
  FinanceTable.propTypes = {
    members: PropTypes.array,
    getCampParticipant: PropTypes.func
  };
  const user = useSelector(state => state.user);

  function createRows(member, index) {
    const accountNumber = member?.payment?.account_number;
    return (
      <TableRow key={index}>
        <TableCell align="center">{index + 1}</TableCell>
        <TableCell>{member?.name}</TableCell>
        <TableCell>{accountNumber?.substr(accountNumber?.length - 4)}</TableCell>
        <TableCell>{moment(member.transfer_date).format("YYYY-MM-DD")}</TableCell>
        <TableCell>{paymentStatus(member)}</TableCell>
        <TableCell>{action(member)}</TableCell>
      </TableRow>
    );
  }

  function paymentStatus(member) {
    let value = member.payment;

    if (!value) {
      return <Chip color="primary" label="尚未填寫匯款資料" variant="outlined" />;
    }

    if (value.confirmed === false) {
      return <Chip color="error" label="等待財務組確認" />;
    } else {
      return <Chip color="success" label="匯款已確認" />;
    }
  }

  function action(member) {
    if (!member.payment) {
      return <p>尚未填寫匯款資料</p>;
    }
    if (member.payment.confirmed) {
      return (
        <Button
          variant="danger"
          onClick={() => {
            confirmPayment(member, false);
          }}
        >
          取消確認匯款
        </Button>
      );
    } else {
      return (
        <Button
          variant="success"
          onClick={() => {
            confirmPayment(member, true);
          }}
        >
          確認匯款
        </Button>
      );
    }
  }

  async function confirmPayment(member, state) {
    let temp = {
      account_number: member.payment.account_number,
      confirmed: state,
      confirm_person: user.membership_number
    };

    await updateCampPaymentStatus(member.id, temp);
    getCampParticipant();
  }
  return (
    <TableContainer className="namelist-table" component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="staff table">
        <TableHead>
          <TableRow>
            <TableCell align="center">#</TableCell>
            <TableCell>姓名</TableCell>
            <TableCell>匯款帳戶末五碼</TableCell>
            <TableCell>匯款日期</TableCell>
            <TableCell>匯款狀態</TableCell>
            <TableCell>動作</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>{members?.map(createRows)}</TableBody>
      </Table>
    </TableContainer>
  );
}
