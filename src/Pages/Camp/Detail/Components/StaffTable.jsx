import { Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper } from "@mui/material";
import PropTypes from "prop-types";

export default function StaffTable({ members }) {
  StaffTable.propTypes = {
    members: PropTypes.array
  };

  const styleSticky = { position: "sticky", left: 0, background: "white", minWidth: "80px", zIndex: 900 };

  function createRows(member, index) {
    return (
      <TableRow key={index}>
        <TableCell align="center" style={styleSticky}>
          {member?.username}
        </TableCell>
        <TableCell align="center">{member?.membership_number}</TableCell>
      </TableRow>
    );
  }
  return (
    <TableContainer className="namelist-table" component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="staff table">
        <TableHead>
          <TableRow>
            <TableCell align="center" style={styleSticky}>
              姓名
            </TableCell>
            <TableCell align="center">會員編號</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>{members?.map(createRows)}</TableBody>
      </Table>
    </TableContainer>
  );
}
