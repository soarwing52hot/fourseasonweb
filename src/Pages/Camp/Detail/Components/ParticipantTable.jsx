import { Button, Table, TableCell, TableContainer, TableHead, TableBody, TableRow, Paper, Chip } from "@mui/material";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";

export default function ParticipantTable({ members }) {
  ParticipantTable.propTypes = {
    members: PropTypes.array
  };

  const history = useNavigate();
  function createRows(member, index) {
    return (
      <TableRow key={index}>
        <TableCell align="center">{index + 1}</TableCell>
        <TableCell>{member?.name}</TableCell>
        <TableCell>{moment(member.birth_date).format("YYYY-MM-DD")}</TableCell>
        <TableCell>{member.citizen_id}</TableCell>
        <TableCell>{paymentStatus(member)}</TableCell>
      </TableRow>
    );
  }

  function paymentStatus(member) {
    let value = member.payment;
    if (!value) {
      return (
        <Button
          variant="contained"
          onClick={() => {
            history(`/Camp/PaymentForm/${member.id}`);
          }}
        >
          前往匯款
        </Button>
      );
    }

    if (value.confirmed === false) {
      return <Chip label="等待財務組確認" color="secondary"></Chip>;
    } else {
      return <Chip label="匯款已確認" color="success"></Chip>;
    }
  }
  return (
    <TableContainer className="namelist-table" component={Paper}>
      <Table sx={{ minWidth: 650 }} aria-label="staff table">
        <TableHead>
          <TableRow>
            <TableCell align="center">#</TableCell>
            <TableCell>姓名</TableCell>
            <TableCell>生日</TableCell>
            <TableCell>身分證末五碼</TableCell>
            <TableCell>匯款狀態</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>{members?.map(createRows)}</TableBody>
      </Table>
    </TableContainer>
  );
}
