import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { Grid, Box, Button, Accordion, AccordionSummary, AccordionDetails, Typography, Tabs, Tab } from "@mui/material";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";

import { getCamp, getCampParticipantByCampId, getInsurance, patchCampStaff, downloadBrochure, getStaffExcel, getParticipantExcel } from "@/Services/Camps";

import Notice from "./Components/Notice";
import StaffTable from "./Components/StaffTable";
import ParticipantTable from "./Components/ParticipantTable";
import FinanceTable from "./Components/FinanceTable";
import AnnounceById from "@/Pages/Announcement/Components/AnnounceById";

const CampDetail = () => {
  const history = useNavigate();
  const { id } = useParams();
  const user = useSelector(state => state.user);

  const [Camp, setCamp] = useState({});
  const [Participant, setParticipant] = useState([]);
  const [BrochureUUID, setBrochureUUID] = useState("");
  const [tabId, setTabId] = useState(1);

  const Staff = Camp.staff;
  const isOwner = Camp.owner?.membership_number == user.membership_number;
  const isLeader = Camp.leaders?.some(e => e.membership_number === user.membership_number);
  const isStaff = Camp.staff?.some(e => e.membership_number === user.membership_number);
  const isFinance = user?.group?.includes("Finance");
  const staffRegisterDisabled = !(user.isLoggedin && Camp.can_change_staff);

  useEffect(() => {
    Promise.all([_GetCamp(), _GetParticipant()]);
  }, []);

  async function _GetCamp() {
    let result = await getCamp(id);
    setCamp(result.data);
  }

  async function _GetParticipant() {
    let result = await getCampParticipantByCampId(id);
    setParticipant(result.data.results);
  }

  const _StaffJoin = async join => {
    let data = {
      membership_number: user.membership_number,
      join: join
    };

    patchCampStaff(id, data).then(() => {
      _GetCamp();
    });
  };

  function _download() {
    getInsurance(Camp.id, `${Camp.camp_type_name}.xlsx`);
  }

  function downloadStaffExcel() {
    getStaffExcel(Camp.id, `${Camp.title}_工作人員.xlsx`);
  }

  function downloadParticipantExcel() {
    getParticipantExcel(Camp.id, `${Camp.title}_學員.xlsx`);
  }

  const toRegister = () => {
    history(`/Camp/Register/${id}`);
  };

  function toEdit() {
    history(`/Camp/Form/${Camp.id}/`);
  }

  const DownloadContract = async () => {
    const res = await downloadBrochure(Camp.camp_type);
    let uuid = res.data.pdf_uuid;
    let url = `https://storage.googleapis.com/fourseasonweb/brochure/${uuid}`;
    setBrochureUUID(url);
  };

  function changeTab(event, newValue) {
    setTabId(newValue);
  }

  return (
    <div className="content-area">
      <h1 className="title-bar">{Camp?.title}</h1>
      <Grid container spacing={2} sx={{ mb: 1 }}>
        <Grid item xs={12} md={2}>
          <a href={BrochureUUID} rel="noopener noreferrer" target="_blank">
            <Button fullWidth variant="contained" color="primary" onClick={DownloadContract}>
              {Camp.camp_type_name}簡章下載
            </Button>
          </a>
        </Grid>

        <Grid item xs={6} md={2}>
          {isStaff ? (
            <Button fullWidth variant="outlined" color="secondary" onClick={() => _StaffJoin(false)} disabled={staffRegisterDisabled}>
              取消報名工作人員
            </Button>
          ) : (
            <Button fullWidth variant="contained" color="secondary" onClick={() => _StaffJoin(true)} disabled={staffRegisterDisabled}>
              報名工作人員
            </Button>
          )}
        </Grid>
        <Grid item xs={6} md={2}>
          <Button fullWidth onClick={toRegister} disabled={!Camp.register_available} variant="contained" color={Camp.register_available ? "success" : "error"}>
            {Camp.register_available ? "報名參加營隊" : "此活動報名已經截止囉"}
          </Button>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        {(isOwner || isLeader) && (
          <>
            <Grid item xs={4} md={2}>
              <Button fullWidth variant="contained" color="primary" onClick={_download}>
                列印保險資料
              </Button>
            </Grid>
            <Grid item xs={4} md={2}>
              <Button fullWidth variant="contained" color="secondary" onClick={downloadStaffExcel}>
                列印工作人員資料
              </Button>
            </Grid>
            <Grid item xs={4} md={2}>
              <Button fullWidth variant="contained" color="forth" onClick={downloadParticipantExcel}>
                列印學員資料
              </Button>
            </Grid>
          </>
        )}

        {isOwner && (
          <Grid item xs={12} md={2}>
            <Button fullWidth onClick={toEdit} variant="contained" color="error">
              修改營隊
            </Button>
          </Grid>
        )}
      </Grid>
      <Accordion defaultExpanded>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography>注意事項</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Notice />
        </AccordionDetails>
      </Accordion>
      {Camp.announcement ? <AnnounceById id={Camp.announcement} /> : <p>尚未發布公告</p>}

      <br />
      <Tabs value={tabId} onChange={changeTab}>
        <Tab label="協會人員"></Tab>
        <Tab label="學員"></Tab>
        <Tab label="財務專區" disabled={!isFinance}></Tab>
      </Tabs>
      {tabId == 0 && (
        <div name="staff">
          <p>總召：</p>
          <StaffTable members={[Camp.owner]} />
          <p>領隊：</p>
          <StaffTable members={Camp.leaders} />
          <p>工作人員：</p>
          <StaffTable members={Staff} />
        </div>
      )}
      {tabId == 1 && (
        <div name="participant">
          <ParticipantTable members={Participant} />
        </div>
      )}
      {tabId == 2 && isFinance && (
        <div name="finance">
          <FinanceTable members={Participant} getCampParticipant={_GetParticipant} />
        </div>
      )}
    </div>
  );
};

export default CampDetail;
