import { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { getCampTypes, getCamp } from "@/Services/Camps.js";
import InputForm from "./Components/InputForm";
import { getAnnouncement, postAnnouncement, patchAnnouncement } from "@/Services/Announcement";
import { postCamp, patchCamp } from "@/Services/Camps";
import moment from "moment-timezone";

export default function CampForm() {
  let { id } = useParams();
  const history = useNavigate();
  const isNew = id == "new";
  const [campTypes, setCampTypes] = useState([]);
  const [camp, setCamp] = useState({});

  useEffect(() => {
    _getCampOptions();

    if (isNew) {
      return;
    }

    _getDetail(id);
  }, []);

  async function _getCampOptions() {
    const res = await getCampTypes();
    setCampTypes(res.data);
  }

  async function _getDetail(id) {
    let res = await getCamp(id);
    let data = res.data;

    if (data.announcement > 0) {
      let announceResponse = await getAnnouncement(data.announcement);
      data.announce = announceResponse.data;
      data.announceContent = data.announce?.content;
    }

    setCamp(data);
  }

  const initialValue = {
    id: camp?.id || 0,
    owner: camp?.owner?.membership_number || 0,
    leaders: camp?.leaders?.map(e => e.membership_number) || [],
    title: camp?.title || "",
    content: camp?.content || "",
    can_change_staff: camp?.can_change_staff || false,
    announcement: camp?.announcement || undefined,
    announceContent: camp?.announceContent || "",
    register_date_start_date: moment(camp?.register_date_start),
    register_date_start_time: moment(camp?.register_date_start),
    register_date_due_date: moment(camp?.register_date_due),
    register_date_due_time: moment(camp?.register_date_due),
    trip_date_start: moment(camp?.trip_date_start),
    trip_date_end: moment(camp?.trip_date_end),
    participant_limit: camp?.participant_limit || 0,
    camp_type: camp?.camp_type || campTypes.find(e => e != undefined)?.value || ""
  };

  async function submitForm(data) {
    data.announcement = await postOrUpdateAnnouncement(data);
    let apiId = id;
    let action = isNew ? "建立營隊" : "修改營隊";
    try {
      if (isNew) {
        let res = await postCamp(data);
        apiId = res.data.id;
      } else {
        await patchCamp(apiId, data);
      }
      history(`/Camp/Detail/${apiId}`);
    } catch (err) {
      alert(`${action}失敗`, err);
    }
  }

  async function postOrUpdateAnnouncement(data) {
    let request = {
      title: `${data.title}營隊公告`,
      content: data.announceContent,
      announce_type: "L"
    };
    let res;
    if (camp.announcement) {
      res = await patchAnnouncement(camp.announcement, request);
    } else {
      res = await postAnnouncement(request);
    }

    return res.data.id;
  }

  return (
    <div className="content-area">
      <h3 className="title-bar">營隊基本資料</h3>
      <InputForm initialValue={initialValue} campTypes={campTypes} submitForm={submitForm} />
    </div>
  );
}
