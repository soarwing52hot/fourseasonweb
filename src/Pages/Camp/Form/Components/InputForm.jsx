import { useState, useEffect } from "react";

import { useFormik } from "formik";
import * as yup from "yup";
import { Grid, TextField, Button, FormControl, MenuItem, Select, Checkbox, FormControlLabel, Box, Chip, OutlinedInput, Divider, ListItemText, InputLabel } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import { TimePicker } from "@mui/x-date-pickers/TimePicker";
import { handleDatetime } from "@/Utilities/FormUtils";
import { getUsersGroups } from "@/Services/User";

import PropTypes from "prop-types";
import moment from "moment";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250
    }
  }
};

export default function InputForm(props) {
  InputForm.propTypes = { initialValue: PropTypes.object, campTypes: PropTypes.array, submitForm: PropTypes.func };

  const isNew = props.initialValue?.id == 0;
  const [users, setUsers] = useState([]);

  useEffect(() => {
    getUserList();
  }, []);

  const validationSchema = yup.object().shape({
    owner: yup
      .number()
      .required("必填欄位")
      .min(1, "請選擇總召！"),
    leaders: yup
      .array()
      .of(yup.number())
      .required("必填欄位"),
    title: yup.string().required("必填欄位"),
    content: yup.string().required("必填欄位"),
    announceContent: yup.string().required("必填欄位"),
    can_change_staff: yup.boolean().required("必填欄位"),
    register_date_start_date: yup.date().required("必填欄位"),
    register_date_start_time: yup.date().required("必填欄位"),
    register_date_due_date: yup.date().required("必填欄位"),
    register_date_due_time: yup.date().required("必填欄位"),
    trip_date_start: yup.date().required("必填欄位"),
    trip_date_end: yup.date().required("必填欄位"),
    participant_limit: yup.number().required("必填欄位"),
    camp_type: yup.string().required("必填欄位")
  });

  const formik = useFormik({
    initialValues: props.initialValue,
    enableReinitialize: true,
    validationSchema: validationSchema,
    onSubmit: values => {
      let data = { ...values };

      const datetimeCol = ["register_date_start", "register_date_due"];
      datetimeCol.forEach(e => handleDatetime(data, e));

      const dateCols = ["trip_date_start", "trip_date_end"];
      dateCols.forEach(e => {
        data[e] = data[e].format("YYYY-MM-DD");
      });
      props.submitForm(data);
    }
  });

  async function getUserList() {
    let key = "leader";
    let userList = [];
    let page = 1;
    let res = await getUsersGroups(key);

    userList = userList.concat(res.data.results);
    while (res.data.next) {
      page += 1;
      res = await getUsersGroups(key, page);
      userList = userList.concat(res.data.results);
    }

    setUsers(userList);
  }

  return (
    <form onSubmit={formik.handleSubmit}>
      <Grid container spacing={2} rowSpacing={1}>
        <Grid item xs={3} md={1}>
          營隊名稱
        </Grid>
        <Grid item xs={9} md={11}>
          <TextField
            fullWidth
            id="title"
            name="title"
            value={formik.values.title}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.title && Boolean(formik.errors.title)}
          />
          {formik.errors.title}
        </Grid>
        <Grid item xs={3} md={1}>
          營隊類型
        </Grid>
        <Grid item xs={9} md={2}>
          <FormControl fullWidth>
            <Select name="camp_type" value={formik.values.camp_type} onChange={formik.handleChange} error={formik.touched.camp_type && Boolean(formik.errors.camp_type)}>
              <MenuItem value="" key="empty">
                請選擇營隊類型
              </MenuItem>
              {props.campTypes.map(e => (
                <MenuItem value={e.value} key={e.value}>
                  {e.label}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={4} md={1}>
          營隊開始日期
        </Grid>
        <Grid item xs={8} md={2}>
          <DatePicker
            name="trip_date_start"
            value={moment(formik.values.trip_date_start)}
            onChange={e => formik.setFieldValue("trip_date_start", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          營隊結束日期
        </Grid>
        <Grid item xs={8} md={2}>
          <DatePicker
            name="trip_date_end"
            value={moment(formik.values.trip_date_end)}
            onChange={e => formik.setFieldValue("trip_date_end", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Box component={Grid} item md={3} display={{ xs: "none", lg: "block" }}></Box>

        <Grid item xs={3} md={1}>
          總召
        </Grid>
        <Grid item xs={9} md={2}>
          <FormControl fullWidth>
            <Select name="owner" value={formik.values.owner} onChange={formik.handleChange} error={formik.touched.owner && Boolean(formik.errors.owner)}>
              <MenuItem value={0} key={0}>
                請選擇總召
              </MenuItem>
              {users.map(e => (
                <MenuItem value={e.membership_number} key={e.membership_number}>
                  {e.username}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          {formik.errors.owner}
        </Grid>
        <Grid item xs={3} md={1}>
          領隊群
        </Grid>
        <Grid item xs={9} md={6}>
          <FormControl fullWidth>
            <InputLabel></InputLabel>
            <Select
              label=""
              id="leaders"
              name="leaders"
              multiple
              value={formik.values.leaders}
              onChange={formik.handleChange}
              input={<OutlinedInput label="Tag" />}
              renderValue={selected => selected.map(value => <Chip key={value} label={users.find(e => e.membership_number == value).username} />)}
              MenuProps={MenuProps}
            >
              {users.map(e => (
                <MenuItem key={e.membership_number} value={e.membership_number}>
                  <Checkbox checked={formik.values.leaders.includes(e.membership_number)} />
                  <ListItemText primary={e.username} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>
        <Grid item xs={12} md={2}>
          <FormControlLabel control={<Checkbox name="can_change_staff" checked={formik.values.can_change_staff} onChange={formik.handleChange} />} label="開放工作人員報名" />
        </Grid>
        <Grid item xs={12} md={1}>
          <p>活動簡介</p>
          <span className="text-muted">上限150字</span>
        </Grid>
        <Grid item xs={12} md={11}>
          <TextField
            fullWidth
            multiline
            rows={4}
            id="content"
            name="content"
            value={formik.values.content}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.content && Boolean(formik.errors.content)}
            inputProps={{ maxLength: 150 }}
          />
          {formik.errors.content}
        </Grid>
        <Grid item xs={12} md={1}>
          <p>活動公告</p>
          <span className="text-muted">上限150字</span>
        </Grid>
        <Grid item xs={12} md={11}>
          <TextField
            fullWidth
            multiline
            rows={4}
            id="announceContent"
            name="announceContent"
            value={formik.values.announceContent}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.announceContent && Boolean(formik.errors.announceContent)}
            inputProps={{ maxLength: 150 }}
          />
          {formik.errors.announceContent}
        </Grid>
        <Grid item xs={12} md={12}>
          <Divider />
        </Grid>
        <Grid item xs={12} md={12}>
          <h4 className="title-bar">報名相關</h4>
        </Grid>
        <Grid item xs={4} md={1}>
          報名開始日期
        </Grid>
        <Grid item xs={8} md={2}>
          <DatePicker
            name="register_date_start_date"
            value={formik.values.register_date_start_date}
            onChange={e => formik.setFieldValue("register_date_start_date", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Grid item xs={4} md={1}>
          報名開始時間
        </Grid>
        <Grid item xs={8} md={2}>
          <TimePicker
            name="register_date_start_time"
            value={formik.values.register_date_start_time}
            onChange={e => formik.setFieldValue("register_date_start_time", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>
        <Box component={Grid} item md={6} display={{ xs: "none", lg: "block" }} />
        <Grid item xs={4} md={1}>
          報名截止日期
        </Grid>
        <Grid item xs={8} md={2}>
          <DatePicker
            name="register_date_due_date"
            value={formik.values.register_date_due_date}
            onChange={e => formik.setFieldValue("register_date_due_date", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>

        <Grid item xs={4} md={1}>
          報名截止時間
        </Grid>
        <Grid item xs={8} md={2}>
          <TimePicker
            name="register_date_due_time"
            value={formik.values.register_date_due_time}
            onChange={e => formik.setFieldValue("register_date_due_time", e)}
            slotProps={{ textField: { fullWidth: true } }}
          />
        </Grid>

        <Box component={Grid} item md={6} display={{ xs: "none", lg: "block" }} />
        <Grid item xs={4} md={1}>
          <p>人數上限</p>
          <span className="text-muted">0為不設限</span>
        </Grid>
        <Grid item xs={8} md={1}>
          <TextField
            fullWidth
            id="participant_limit"
            name="participant_limit"
            type="number"
            value={formik.values.participant_limit}
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            error={formik.touched.participant_limit && Boolean(formik.errors.participant_limit)}
          />
        </Grid>
        <Grid item xs={12} md={12}>
          <Button color="primary" variant="contained" fullWidth type="submit">
            {isNew ? "新增營隊" : "完成修改"}
          </Button>
        </Grid>
      </Grid>
    </form>
  );
}
