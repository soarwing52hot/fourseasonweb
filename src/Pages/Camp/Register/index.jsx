import { useNavigate, useParams } from "react-router-dom";
import { postCampParticipant } from "@/Services/Camps";
import CampRegisterForm from "./Components/CampRegisterForm";

const CampRegister = () => {
  let { id } = useParams();
  const history = useNavigate();

  const handleSubmit = data => {
    data.camp = id;
    postCampParticipant(data)
      .then(result => {
        let data = result.data;
        history(`../Camp/PaymentForm/${data.id}`);
      })
      .catch(err => {
        console.error(err);
        alert(`報名失敗 ${JSON.stringify(err.response.data)}`);
      });
  };

  return (
    <div className="content-area">
      <h1 className="title-bar">營隊報名</h1>
      <p className="title-bar">以下資料為保險用資料，請詳實填寫</p>
      <CampRegisterForm submitForm={handleSubmit} />
    </div>
  );
};

export default CampRegister;
