import { Box, Grid, Card, CardContent, CardActionArea, Button, Chip, Typography, Divider } from "@mui/material";
import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import moment from "moment-timezone";

export default function CampCard({ camp }) {
  CampCard.propTypes = {
    camp: PropTypes.object
  };
  const history = useNavigate();
  const today = moment();
  const trip_date_start = moment(camp.trip_date_start);
  const trip_date_end = moment(camp.trip_date_end);
  const register_date_start = moment(camp.register_date_start);

  function defineChip() {
    if (camp.register_available) {
      return <Chip label="開放報名開放中" color="success"></Chip>;
    }
    if (today < register_date_start) {
      return <Chip label="尚未開放報名" color="grey"></Chip>;
    } else if (today < trip_date_start) {
      return <Chip label="活動即將開始"></Chip>;
    } else if (today < trip_date_end) {
      return <Chip label="活動進行中" color="success"></Chip>;
    } else if (today > trip_date_end) {
      return <Chip label="活動已結束" color="error"></Chip>;
    }
  }

  return (
    <Grid item xs={12} md={4}>
      <Card sx={{ height: "100%" }}>
        <CardActionArea onClick={() => history(`../Camp/Detail/${camp.id}`)}>
          <CardContent>
            <Typography gutterBottom variant="h5" sx={{ mx: 1 }}>
              <b>{camp.title}</b>
            </Typography>
            <Divider />
            <Grid container spacing={2} rowSpacing={1}>
              <Grid item xs={3}>
                活動日期
              </Grid>
              <Grid item xs={9}>
                {moment(camp.trip_date_start).format("YYYY-MM-DD")} ~ {moment(camp.trip_date_end).format("YYYY-MM-DD")}
              </Grid>
              <Grid item xs={3}>
                報名日期
              </Grid>
              <Grid item xs={9}>
                {moment(camp.register_date_start).format("YYYY-MM-DD")} ~ {moment(camp.register_date_start).format("YYYY-MM-DD")}
              </Grid>
              <Grid item xs={3}>
                活動狀態
              </Grid>
              <Grid item xs={9}>
                {defineChip()}
              </Grid>
              <Grid item xs={3}>
                活動簡介
              </Grid>
              <Grid item xs={9}>
                {camp.content}
              </Grid>
            </Grid>
          </CardContent>
        </CardActionArea>
        <Box sx={{ textAlign: "center", m: 1 }}>
          {camp.register_available ? (
            <Button variant="contained" onClick={() => history(`/Camp/Detail/${camp.id}`)}>
              前往報名
            </Button>
          ) : (
            <Button disabled color="grey">
              已截止
            </Button>
          )}
        </Box>
      </Card>
    </Grid>
  );
}
