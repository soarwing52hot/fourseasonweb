import { useState } from "react";
import { getCampByDate } from "@/Services/Camps";
import moment from "moment-timezone";
import { Grid } from "@mui/material";
import CampCard from "./Components/CampCard";
import SearchBar from "@/Components/SearchBar";

export default function CampList() {
  const [Camps, setCamps] = useState([]);
  const [DateSearch, setDateSearch] = useState({});

  async function GetCampList() {
    if (!DateSearch.dateStart || !DateSearch.dateEnd) {
      return;
    }
    let result = await getCampByDate(moment(DateSearch.dateStart).format("YYYY-MM-DD"), moment(DateSearch.dateEnd).format("YYYY-MM-DD"));
    let data = result.data;
    setCamps(data.results);
  }

  return (
    <div className="content-area">
      <h1 className="title-bar">營隊報名</h1>
      <SearchBar DateSearch={DateSearch} setDateSearch={setDateSearch} searchAction={GetCampList} />
      <br />
      <Grid container spacing={2}>
        {Camps.map(e => (
          <CampCard camp={e} key={e.id} />
        ))}
      </Grid>
    </div>
  );
}
