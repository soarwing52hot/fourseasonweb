import { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { getCampParticipant, postCampPayment } from "@/Services/Camps";
import { CampParticipant } from "@/Models/Camp";
import { Button, Card, CardContent, CardHeader, TextField, Grid } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers";

import { useNavigate, useParams } from "react-router-dom";
import moment from "moment";

const CampPaymentForm = () => {
  let { id } = useParams();
  const [participantData, setParticipantData] = useState(new CampParticipant());
  const history = useNavigate();
  useEffect(() => {
    _getCampParticipant();
  }, []);

  const validationSchema = yup.object().shape({
    account_number: yup
      .string()
      .required("必填欄位")
      .min(5, "必須5位數")
      .max(5, "必須5位數"),
    transfer_date: yup.date().required("必填欄位"),
    memo: yup.string(),
    amount: yup.number().required("必填欄位")
  });

  const initialValue = { account_number: "", transfer_date: moment(), amount: 0, memo: "" };

  const formik = useFormik({
    initialValues: initialValue,
    enableReinitialize: false,
    validationSchema: validationSchema,
    onSubmit: values => {
      let data = { ...values };
      if (participantData.citizen_id !== data.citizen_id) {
        alert("身份證字號不相符！");
        return;
      }
      if (data.account_number.toString().length != 5) {
        alert("匯款帳號只能5碼");
        return;
      }
      data.transfer_date = values.transfer_date.format("YYYY-MM-DD");
      Send(data);
    }
  });

  const Send = async data => {
    data["camp_participant"] = parseInt(id);

    try {
      await postCampPayment(data);
      alert("付款資料填寫成功！財務組會再確認您的匯款");
      history("/Home");
    } catch (err) {
      alert(JSON.stringify(err.response.data));
    }
  };

  async function _getCampParticipant() {
    let response = await getCampParticipant(id);
    setParticipantData(response.data);
  }

  return (
    <div className="content-area">
      <h3 className="title-bar">匯款確認</h3>
      <Card sx={{ margin: 2 }}>
        <CardHeader title="轉帳資訊" />
        <CardContent>
          <p>匯款銀行：永豐銀行 (萬華分行) (ATM 代號：807) </p>
          <p>匯款戶名： 台灣四季溯溪協會</p>
          <p>轉帳帳號：105-018-0003005-3 (共 14 碼數字)</p>
          <p>
            <i>備註1：請將轉帳後之收據保留，以便查詢</i>
          </p>
          <p>
            <i>備註2：匯款人請填您本人名字，如此，銀行對帳明細中就可查到您的名字囉</i>
          </p>
        </CardContent>
      </Card>
      <br />
      <div className="content-area">
        <form onSubmit={formik.handleSubmit}>
          <Grid container rowSpacing={1}>
            <Grid item xs={2} md={2}>
              姓名
            </Grid>
            <Grid item xs={10} md={10}>
              <TextField type="text" name="name" disabled value={participantData.name} fullWidth />
            </Grid>
            <Grid item xs={4} md={2}>
              身分證字號末四碼(驗證用)
            </Grid>
            <Grid item xs={8} md={10}>
              <TextField
                type="text"
                name="citizen_id"
                fullWidth
                value={formik.values.title}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.title && Boolean(formik.errors.title)}
              />
            </Grid>
            <Grid item xs={4} md={2}>
              帳號末五碼
            </Grid>
            <Grid item xs={8} md={10}>
              <TextField
                required
                type="number"
                pattern="[0-9]{,5}"
                name="account_number"
                max="99999"
                fullWidth
                value={formik.values.account_number}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.account_number && Boolean(formik.errors.account_number)}
              />
              {formik.errors.account_number}
            </Grid>
            <Grid item xs={4} md={2}>
              匯款日期
            </Grid>
            <Grid item xs={8} md={10}>
              <DatePicker
                name="transfer_date"
                value={moment(formik.values.transfer_date)}
                onChange={e => formik.setFieldValue("transfer_date", e)}
                slotProps={{ textField: { fullWidth: true } }}
              />
            </Grid>
            <Grid item xs={4} md={2}>
              匯款金額
            </Grid>
            <Grid item xs={8} md={10}>
              <TextField
                name="amount"
                type="number"
                fullWidth
                value={formik.values.amount}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.amount && Boolean(formik.errors.amount)}
              />
            </Grid>
            <Grid item xs={4} md={2}>
              備註
            </Grid>
            <Grid item xs={8} md={10}>
              <TextField
                name="memo"
                type="text"
                fullWidth
                multiline
                rows={5}
                value={formik.values.memo}
                onChange={formik.handleChange}
                onBlur={formik.handleBlur}
                error={formik.touched.memo && Boolean(formik.errors.memo)}
              />
            </Grid>
            <Grid item xs={12} md={12}>
              <Button type="submit" variant="contained" color="success" fullWidth>
                送出
              </Button>
            </Grid>
          </Grid>
        </form>
      </div>
    </div>
  );
};

export default CampPaymentForm;
