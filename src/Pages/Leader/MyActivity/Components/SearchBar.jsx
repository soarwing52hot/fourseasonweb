import { useState, useEffect, useCallback } from "react";
import { Grid, Button, TextField } from "@mui/material";
import PropTypes from "prop-types";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";
import moment from "moment-timezone";

export default function SearchBar({ GetActivities }) {
  SearchBar.propTypes = {
    GetActivities: PropTypes.func
  };

  const [searchParam, setSearchParam] = useState({ title: "", trip_date_start: moment(moment().format("YYYY-01-01"), "YYYY-MM-DD"), trip_date_end: moment() });

  const setDate = useCallback(() => {
    let today = moment();
    let temp = { title: "", dateStart: moment(today.format("YYYY-01-01")), dateEnd: moment(today.format(`YYYY-12-31`)) };
    setSearchParam(temp);
  }, []);

  useEffect(() => {
    setDate();
  }, []);

  const handleSubmit = event => {
    const form = event.currentTarget;
    event.preventDefault();
    if (form.checkValidity() === false) {
      event.stopPropagation();
      return;
    }

    let data = {
      title: searchParam.title,
      trip_date_start: searchParam.dateStart.format("YYYY-MM-DD"),
      trip_date_end: searchParam.dateEnd.format("YYYY-MM-DD")
    };

    GetActivities(data);
  };

  return (
    <Grid container>
      <Grid item xs={12} md="auto" className="m-2">
        <TextField
          label="關鍵字"
          sx={{ marginRight: 2 }}
          onChange={e => {
            setSearchParam(prevState => ({
              ...prevState,
              title: e.target.value
            }));
          }}
        ></TextField>
        <DatePicker
          label="開始日期"
          value={moment(searchParam?.dateStart)}
          onChange={newValue =>
            setSearchParam(prevState => ({
              ...prevState,
              dateStart: newValue
            }))
          }
        />
      </Grid>
      <Grid item xs={12} md="auto" className="m-2">
        <DatePicker
          label="結束日期"
          value={moment(searchParam?.dateEnd)}
          onChange={newValue =>
            setSearchParam(prevState => ({
              ...prevState,
              dateEnd: newValue
            }))
          }
        />
      </Grid>
      <Grid item md={2} xs={12}>
        <Button variant="contained" sx={{ margin: 2, alignContent: "center" }} color="success" type="submit" onClick={handleSubmit}>
          查詢
        </Button>
      </Grid>
    </Grid>
  );
}
