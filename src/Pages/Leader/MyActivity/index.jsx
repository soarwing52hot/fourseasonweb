import { useState, useEffect } from "react";

import { Grid, Button } from "@mui/material";
import { useNavigate } from "react-router-dom";
import { useSelector } from "react-redux";
import { tokenGetActivityRecord } from "@/Services/Activities";
import SearchBar from "./Components/SearchBar";
import ActivityCard from "@/Components/ActivityView/ActivityCard";
import usePagination from "@/hooks/usePagination";
import PageButton from "@/Components/PageButton";

export default function MyActivity() {
  const history = useNavigate();
  const user = useSelector(state => state.user);
  const { pages, setNewPages } = usePagination({});

  const [ActivityList, setActivityList] = useState([]);

  useEffect(() => {
    GetActivities();
  }, []);

  async function GetActivities(requestData, page = 1) {
    let rq = { ...requestData, leader: user.membership_number, page };
    let res = await tokenGetActivityRecord(rq);
    const data = res.data;
    setActivityList(res.data.results);
    setNewPages(data);
  }

  function toNew() {
    history("/Activity/Form/new/");
  }

  return (
    <div className="content-area">
      <h1 className="title-bar">我的行程</h1>
      <Grid container sx={{ marginTop: 2 }}>
        <Grid item xs={12} md={10}>
          <SearchBar GetActivities={GetActivities} />
        </Grid>
        <Grid item xs={12} md={2}>
          <Button className="m-3" variant="contained" color="success" onClick={toNew}>
            開新活動
          </Button>
        </Grid>
      </Grid>

      <Grid container spacing={2} columns={{ xs: 1, md: 4 }}>
        {ActivityList.map(e => (
          <ActivityCard activity={e} key={e.id} />
        ))}
      </Grid>
      <PageButton pages={pages} func={GetActivities} />
    </div>
  );
}
