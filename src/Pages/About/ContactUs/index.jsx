import { Grid, Paper, List, ListItem, Divider } from "@mui/material";

export default function ContactUs() {
  const style = {
    py: 1,
    width: "100%",
    borderRadius: 2,
    borderColor: "divider",
    backgroundColor: "background.paper"
  };

  const rows = [
    { title: "會址", info: "新北市板橋區縣民大道三段187巷2號3樓" },
    { title: "內政部立案證號", info: "台內社字第０九０００三九五七四０號" },
    { title: "統一編號", info: "18434892" },
    { title: "理事長E-mail", info: "director@4season.org.tw" },
    { title: "秘書長E-mail", info: "secretary@4season.org.tw" },
    { title: "版權所有", info: "台灣四季溯溪協會" }
  ];

  function listItem(data, index) {
    return (
      <div key={`data${index}`}>
        <ListItem>
          <Grid container>
            <Grid item xs={6} md={2}>
              {data.title}
            </Grid>
            <Grid item xs={6} md={10}>
              <p style={{ lineBreak: "anywhere" }}>{data.info}</p>
            </Grid>
          </Grid>
        </ListItem>
        {index + 1 != rows.length && <Divider component="li" />}
      </div>
    );
  }
  return (
    <div className="content-area">
      <h3 className="chapter-title">聯絡我們</h3>
      <Paper elevation={3}>
        <List style={style}>{rows.map(listItem)}</List>
      </Paper>
    </div>
  );
}
