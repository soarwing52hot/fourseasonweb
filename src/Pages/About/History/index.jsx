import { useState, useEffect } from "react";
import { ImgCover } from "@/Style/images";
import { getCommittee } from "@/Services/About";

export default function History() {
  const [cadre, setCadre] = useState([]);
  useEffect(() => {
    getCadreData();
  }, []);

  async function getCadreData(page = 1) {
    let res = await getCommittee(page);
    let data = res.data.results;
    setCadre(data);
  }

  function cadreUnit(data) {
    return (
      <div key={data.year} style={{ margin: 10 }}>
        <p className="chapter-bold">
          西元{data.year}年-民國{data.year - 1911}年
        </p>
        <p className="chapter-text">
          理事長 : {data.chairman} 秘書長 : {data.secratary} 嚮導組長: {data.guide_lead}
        </p>
        <p className="chapter-text">理事 : {data.directors}</p>
        <p className="chapter-text">監事 : {data.superivsors}</p>
      </div>
    );
  }

  return (
    <div className="content-area">
      <div className="cover-about">
        <img className="cover-img" src={ImgCover} alt="Background Image" height="30px" />
        <div className="color-overlay-about" />
        <h1 className="cover-text">ABOUT</h1>
      </div>
      <h1 className="chapter-title">台灣四季溯溪協會成立沿革</h1>
      <p className="chapter-title">理事長:謝永潭</p>
      <h4 className="chapter-bold">緣起：</h4>
      <p className="chapter-content">1990年12月1日，林宗聖先生首創四季溯溪俱樂部，名為四季，意在說明台灣一年四季均可溯溪。</p>
      <p className="chapter-content">
        有鑑於外國之溯溪已蔚為風氣，遂計劃在國內推廣此運動於是選擇大屯山火山群疆界各水系溪谷進行全面且多元性之溯行調查與探索，並.將多年探勘所得集結成大屯火山博物誌系列叢書.除了著書分享讀者，同時也積極推廣溯溪活動暨培育溯溪人才.由於林先生對溯溪界的卓越貢獻，遂被俱樂部成員推舉為首任部長。
      </p>
      <p className="chapter-content">
        俱樂部成立伊始，會員寥寥可數，而今協會成員已有上百人之多，十年來，會長亦曾改選過三次，分別由洪泰郎(大師兄),羅元妙(阿妙),以及邱琦升等三位擔任，他們均懷抱熱情與衝勁領導四季溯溪俱樂部，全力推動溯溪活動，因為會員人數劇增，前任三位會長和幹部們均認為需籌組協會以制度讓俱樂部更為茁壯成長，於是在1999年5月31日會議時,開始逐步擬訂各相關規則。
      </p>
      <p className="chapter-content">
        然而當時因為尚有諸多困難未排除，是故，無法立即順利籌組協會，經過一段時間的努力，各工作幹部終於排除萬難於2001年5月30日擬就各項申請成立之章程，由邱前會長琦升等42人發起籌組,並檢具申請書發起人名冊，向內政部申請籌組全國性之社團.內政部於2001年7月23日以台(90)內社第902287號函准予設立台灣四季溯溪協會。
      </p>
      <p className="chapter-content">
        本會並於2001年12月2日投票選舉理監事，旋即召開第一屆理監事會議，選舉謝永潭先生為台灣四季溯溪協會首任會長，並聘張達宏先生為秘書長，台灣四季溯溪協會於焉誕生。
      </p>
      <h3 className="chapter-title">宗旨：</h3>
      <p className="chapter-content">
        本會將溯溪活動多元化推動，本著追本溯源之理念，進行山與溪谷地探索及調查，藉以瞭解人與自然的互動，進而引導生態保育重視及正當休閒活動的風行為宗旨。
      </p>
      <h3 className="chapter-title">任務：</h3>
      <p className="chapter-content">
        一：推廣溯溪活動。 二：溯溪攀岩登山知識之傳達暨技術之推廣與訓練。 三：建立台灣溪谷山林資料。 四：宣導生態環保觀念，維護自然生態環境以達育教於樂之目的。
      </p>
      <div className="content-area cadre-history">
        <h1 className="chapter-title">歷屆幹部</h1>
        <hr />
        {cadre.map(cadreUnit)}
      </div>
    </div>
  );
}
