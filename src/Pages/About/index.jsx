import ArticlesOfAssociation from "./ArticlesOfAssociation";
import Cadre from "./Cadre";
import History from "./History";
import DevHistory from "./DevHistory";
import ContactUs from "./ContactUs";

export { ArticlesOfAssociation, Cadre, History, DevHistory, ContactUs };
