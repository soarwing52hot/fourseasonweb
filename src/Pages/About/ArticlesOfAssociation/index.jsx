import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemText from "@mui/material/ListItemText";
import Divider from "@mui/material/Divider";

export default function ArticlesOfAssociation() {
  const style = {
    py: 1,
    width: "100%",
    borderRadius: 2,
    border: "1px solid",
    borderColor: "divider",
    backgroundColor: "background.paper"
  };

  function listItem(text, index, array) {
    return (
      <div key={text}>
        <ListItem>
          <ListItemText primary={text} />
        </ListItem>
        {index + 1 != array.length && <Divider component="li" />}
      </div>
    );
  }

  const mission = ["一、推廣溯溪活動。", "二、溯溪攀岩登山知識傳達技術推廣與訓練。", "三、建立台灣溪谷山林資料。", "四、宣導生態環保觀念，維護自然生態環境以達育教於樂之目的。"];

  const requirements = [
    "一、普通會員：贊同本會宗旨，年滿二十歲，具有行為能力不分男女品德端正，填妥個人入會申請表，繳納入會費即為個人會員。",
    "二、永久會員：贊同本會宗旨，擁有本會個人會員資格繳納一定數額，即不必繳納常年會費，但如其出會、入會仍須依章程繳納會費。112年度起暫停新永久會員之加入申請，既有之永久會員名單及權益不受影響。",
    "三、榮譽會員：經理監事會認定對本會有重大貢獻者。",
    "四、預備會員：贊同本會宗旨未滿二十歲經家長或法定代理人同意完成入會手續，經理事會通過，年資併入正式會員，申請時應填具入會申請書，經理事會通過並繳納會費。",
    "五、團體會員：凡贊同本會宗旨之公私立機構或團體，填具入會申請書，經理事會通過並繳納入會費後為團體會員，團體會員得推派代表一人，行使會員權。"
  ];

  const authority = [
    "一、訂定與變更章程。",
    "二、選舉及罷免理事、監事。",
    "三、議決入會費、常年會費、事業費及會員捐款之數額及方式。",
    "四、議決年度工作計畫、報告及預算、決算。",
    "五、議決會員(會員代表)之除名處分。",
    "六、議決財產之處分。",
    "七、議決本會之解散。",
    "八、議決與會員權利義務有關之其他重大事項。 前項第八款重大事項之範圍由理事會定之。"
  ];
  return (
    <div className="content-area">
      <p className="chapter-title">
        本會宗旨{"  "}
        <span>本會將溯溪活動多元化推廣，本著追本溯源之理念，進行山與溪谷的探索及調查，藉以瞭解人與自然的互動，進而引導生態保育重視及正當休閒活動的風行為宗旨。</span>
      </p>
      <p className="chapter-text"></p>
      <h2 className="chapter-title">
        <span className="chapter-text">台灣四季溯溪協會組織章程</span>
      </h2>
      <h3 className="chapter-title">第一章 總 則</h3>
      <p className="chapter-text">第 一 條 本會名稱為 台灣四季溯溪協會(以下簡稱本會)。</p>
      <p className="chapter-text">第 二 條 本會為依法設立，非以營利為目的之社會團體。</p>
      <p className="chapter-text">
        宗旨如下：本會將溯溪活動多元化推廣，本著追本溯源之理念，進行山與溪谷的探索及調查，藉以瞭解人與自然的互動，進而引導生態保育重視及正當休閒活動的風行為宗旨。{" "}
      </p>
      <p className="chapter-text">第 三 條 本會以全國行政區域為組織區域。</p>
      <p className="chapter-text">第 四 條 本會會址設於主管機關所在地，並得報經主管機關核准設分支機構。</p>
      <p className="chapter-text">前項分支機構組織簡則由理事會擬訂，報請主管機關核准後行之。</p>
      <p className="chapter-text">會址及分支機構之地址於設置及變更時應報請主管機關核備。</p>
      <div>
        <p className="chapter-text">第 五 條 本會之任務如下：</p>
        <List sx={style}>{mission.map((m, index) => listItem(m, index, mission))}</List>
      </div>
      <p className="chapter-text">第 六 條 本會之主管機關為內政部。 本會之目的事業應受各該事業主管機關之指導、監督。</p>
      <h3 className="chapter-title">第二章 會員</h3>
      <div>
        <p className="chapter-text">第 七 條 本會會員申請資格如下：</p>
        <List sx={style}>{requirements.map((m, index) => listItem(m, index, requirements))}</List>
      </div>
      <p className="chapter-text">第 八 條 會員(會員代表)有表決權、選舉權、被選舉權與罷免權，每一會員(會員代表)為一權。預備會員、榮譽會員無前項權利。</p>
      <p className="chapter-text">第 九 條 會員有遵守本會章程、決議及繳納會費之義務。 會員未依規定繳納會費，即喪失會員資格。</p>
      <p className="chapter-text">
        第 十 條 會員(會員代表)有違反法令、章程或不遵守會員(會員代表)大會決議時，得經理事會決議，予以警告或停權處分，其危害團體情節重大者，得經會員(會員代表)大會決議予以除名。
      </p>
      <p className="chapter-text">第 十一 條 會員喪失會員資格或經會員(會員代表)大會決議除名者，即為出會。</p>
      <p className="chapter-text">第 十二 條 會員得以書面敘明理由向本會聲明退會。</p>
      <h3 className="chapter-title">第三章 組織及職權</h3>
      <p className="chapter-text">
        第 十三 條 本會以會員大會為最高權力機構。 會員人數超過三百人以上時得分區比例選出會員代表，再召開會員代表大會，行使會員大會職權。
        會員代表任期二年，其名額及選舉辦法由理事會擬訂，報請主管機關核備後行之。
      </p>
      <p className="chapter-text">第 十四 條 會員大會之職權如下：</p>
      <List sx={style}>{authority.map((a, index) => listItem(a, index, authority))}</List>
      <p className="chapter-text">
        第 十五 條
        本會置理事九人，監事三人，由會員在會員大會(會員代表)選舉之，分別成立理事會、監事會。選舉前項理事、監事，依計票情形得同時選出候補理事三人，候補監事一人，理事、監事，候補理事、監事之當選名次依票數多寡為序，票數相同時，以抽籤定之，遇理事、監事出缺時，分別依序遞補之，以補足原任者餘留之任期為限。本屆理事會得提出下屆理事、監事候選人參考名單。
        理事、監事如得採用通訊選舉，理事、監事得採用通訊選舉，但不連續辦理。通訊選舉辦法由理事會通過報請主管機關核備後行之。
      </p>
      <p className="chapter-text">
        第 十六 條 理事會之職權如下： 一、審定會員(會員代表)之資格。 二、選舉及罷免常務理事、理事長。 三、議決理事、常務理事及理事長之辭職。 四、聘免工作人員。
        五、擬訂年度工作計畫、報告及預算、決算。 六、團體會員、榮譽會員之審核。 七、其他應執行事項。
      </p>
      <p className="chapter-text">
        第 十七 條 理事會置常務理事三人，由理事互選之，並由理事就常務理事中選舉一人為理事長。 理事長對內綜理督導會務，對外代表本會，並擔任會員大會、理事會主席及聯席會議主席。
        理事長因事不能執行職務時，應指定常務理事一人代理之，未指定或不能指定時，由常務理事互推一人代理之。 理事長、常務理事出缺時，應於一個月內補選之。
      </p>
      <p className="chapter-text">
        第 十八 條 監事會之職權如下： 一、監理事會工作之執行。 二、審年度決算。 三、選舉及罷免常務監事。 四、議決監事及常務監事之辭職。 五、其他應監察事項。
      </p>
      <p className="chapter-text">
        第 十九 條 監事會置常務監事一人，由監事互選之，監察日常會務，並擔任監事會主席。 常務監事因事不能執行職務時，應指定監事一人代理之，未指定或不能指定時，由監事互推一人代理之。
        監事會主席(常務監事)出缺時，應於一個月內補選之。
      </p>
      <p className="chapter-text">第 二十 條 理事、監事均為無給職，任期二年，連選得連任。理事長之連任以一次為限。</p>
      <p className="chapter-text">
        第二十一條 理事、監事有下列情事之一者，應即解任： 一、喪失會員(會員代表)資格者。 二、因故辭職經理事會或監事會決議通過者。 三、被罷免或撤免者。
        四、受停權處分期間逾任期二分之一者。
      </p>
      <p className="chapter-text">
        第二十二條 本會置秘書長一人，承理事長之命處理本會事務，其他工作人員若干人，由理事長提名經理事會通過後聘免之，並報主管機關備查。但秘書長之解聘應先報主管機關核備。
        前項工作人員不得由理、監事選任之職員擔任。 工作人員權責及分層負責事項由理事會另定之。
      </p>
      <p className="chapter-text">第二十三條 本會得設各種委員會、小組或其他內部作業組織，其組織簡則經理事會通過後施行，變更時亦同。</p>
      <p className="chapter-text">第二十四條 本會得由理事會聘請名譽理事長一人、名譽理事、顧問各若干人，均為義務職，其聘期與理事、監事之任期同。</p>
      <h3 className="chapter-title">第四章 會 議</h3>
      <p className="chapter-text">
        第二十五條 會員（會員代表）大會分定期會議與臨時會議2種，由理事長召集之。召集時除緊急事故之臨時會議外，應於15日前通知全體應出席人員。
        定期會議每年召開一次，臨時會議於理事會認為必要，或經會員(會員代表)五分之一以上之請求，或監事會函請召集召開之。
        本會辦理法人登記後，臨時會議經會員(會員代表)十分之一以上之請求時召開之。
      </p>
      <p className="chapter-text">第二十六條 會員(會員代表)不能親自出席會員大會時，得以書面委託其他會員(會員代表)代理，每一會員(會員代表)以代理一人為限</p>
      <p className="chapter-text">
        第二十七條
        會員大會之決議，以會員(會員代表)過半數之出席，出席人數較多數之同意行之。但章程之訂定與變更、會員(會員代表)之除名、理事及監事之罷免、財產之處分、本會之解散及其他與會員權利義務有關之重大事項應出席人數三分之二以上同意。
        本會辦理法人登記後，章程之變更以出席人數四分之三以上之同意或全體會員三分之二以上書面之同意行之。本會之解散，得隨時以全體會員三分之二以上之可決解散之。
      </p>
      <p className="chapter-text">
        第二十八條 理事會、監事會至少每六個月各舉行會議一次，會員大會每一年召開一次，必要時得召開聯席會議或臨時會議。
        前項會議召集時除臨時會議外，應於七日前以書面通知，會議之決議各以理事、監事過半數之出席，出席人數較多數之同意行之。
      </p>
      <p className="chapter-text">第二十九條 理事應出席理事會議，監事應出席監事會議，不得委託出席；理事、監事連績二次無故缺席理事會、監事會者，視同辭職。</p>
      <h3 className="chapter-title"> 第五章 經費及會計 </h3>
      <p className="chapter-text">
        第 三十 條 本會經費來源如下： 一、入會費：新台幣六佰元，於會員入會時繳納。 二、事業費。 三、會員捐款。 四、委託收益。 五、基金及其孳息。 六、其他收入。
        七、常年會費:個人會員:新台幣陸佰元，於每年第一次會員大會開會時繳交，永久會員：一次繳納新台幣陸仟元，即免再繳納常年會費。
      </p>
      <p className="chapter-text">第三十一條 本會會計年度以曆年為準，自每年一月一日起至十二月三十一日止。</p>
      <p className="chapter-text">
        第三十二條
        本會每年於會計年度開始前二個月由理事會編造年度工作計畫、收支預算表、員工待遇表，提會員大會通過(會員大會因故未能如期召開者，先提理監事聯席會議通過)，於會計年度開始前報主管機關核備。並於會計年度終了後二個月內由理事會編造年度工作報告、收支決算表、現金出納表、資產負債表、財產目錄及基金收支表，送監事會審核後，造具審核意見書送還理事會，提會員大會通過，於三月底前報主管機關核備(會員大會未能如期召開者，先報主管機關)。
      </p>
      <p className="chapter-text">第三十三條 本會解散後，剩餘財產歸屬所在地方之自治團體或主管機關指定機關團體所有。</p>
      <h3 className="chapter-title">第六章 附 則</h3>
      <p className="chapter-text">第三十四條 本章程未規定事項，悉依有關法令規定辦理。</p>
      <p className="chapter-text">第三十五條 本章程經會員(會員代表)大會通過，報經主管機關核備後施行，變更時亦同。</p>
      <p className="chapter-text">第三十六條 本章程經本會 90 年 12月 2 日第一屆第一次會員大會通過，並報經內政部准予備查。</p>
      <p className="chapter-text">95.01.08第九、三十條修正，經第二屆第一次會員大會通過(奉內政部台內社字第0950059129號函核備)</p>
      <p className="chapter-text">112.01.08第十屆第一次會員代表大會通過第7、25條文內容修改(內政部112年3月台內團字第1120290369號函准予備查)</p>
    </div>
  );
}
