export default function DevHistory() {
  return (
    <div className="content-area">
      <h3 className="chapter-title">歷任大頭目</h3>
      <p className="chapter-content">網站前代目 明光 俱樂部時期 ~ 2001/12</p>
      <p className="chapter-content">網站一代目 廷元 2002/01 ~ 2004/02</p>
      <p className="chapter-content">網站二代目 萬益 2004/03 ~ 2006/12</p>
      <p className="chapter-content">網站三代目 小麟 2006/01 ~2006/12</p>
      <p className="chapter-content">網站四代目 廷元 2006/12 ~ 2011/12</p>
      <h3 className="chapter-title">網站記事</h3>
      <p className="chapter-content">2002/01 廷元 本站首任站長(廷元)開始接手籌劃站務</p>
      <p className="chapter-content">2002/03 廷元 本站正式上網開放..目前架在廷元學校的電腦上</p>
      <p className="chapter-content">2002/04 廷元 本站完成留言板討論區及線上通話等功能</p>
      <p className="chapter-content">2002/06 廷元 本站長開始紀錄本站歷史..今後將會將本站重大改革紀錄上來</p>
      <p className="chapter-content">2002/07 廷元 本站開始接續俱樂部時期資料..建立四季大事紀要</p>
      <p className="chapter-content">2002/08 廷元 本站完成線上報名及自動名單列印系統</p>
      <p className="chapter-content">2002/10 廷元 本站修正線上報名及自動名單列印系統.加上領隊自行開放關閉系統</p>
      <p className="chapter-content">2002/10 廷元 開放會員email連線...增進會員間感情交流</p>
      <p className="chapter-content">2002/11 廷元 完成入山證名冊自動化製作</p>
      <p className="chapter-content">2003/01 廷元 完成入山證名冊改版</p>
      <p className="chapter-content">2003/02 廷元 完成入山證名冊自動化製作</p>
      <p className="chapter-content">2003/04 廷元 完成體驗營初溯營線上報名系統</p>
      <p className="chapter-content">2003/06 廷元 完成溪流資料筆劃分類</p>
      <p className="chapter-content">2003/07 廷元 著手從事裝備資料和食譜討論區</p>
      <p className="chapter-content">2004/03 MOUSE 購買&建置協會新電腦,網站搬移</p>
      <p className="chapter-content">2006/03 小麟 會內無線網路環境架設完成</p>
      <p className="chapter-content">2006/10 小麟 網站網路上傳照片功能完成</p>
      <p className="chapter-content">2006/12 小麟/廷元 會內網站功能翻新完成</p>
      <p className="chapter-content">2011/02 廷元 協會電腦更新,網站改版V3.0, 功能升級</p>
    </div>
  );
}
