import { useState, useEffect } from "react";
import { Card, CardHeader, CardContent } from "@mui/material";
import { getVideo } from "@/Services/About";
import Grid from "@mui/material/Unstable_Grid2/Grid2";

export default function Knowledge() {
  const [videos, setVideos] = useState([]);
  useEffect(() => {
    getVideoData();
  }, []);
  async function getVideoData(page = 1) {
    let res = await getVideo(page);
    let data = res.data.results;
    setVideos(data);
  }

  function videoCard(e) {
    // width = "560";
    // height = "315";
    return (
      <Grid sx={{ my: 2 }} key={e.id} xs={12} md={4}>
        <Card>
          <CardHeader title={e.title}></CardHeader>
          <CardContent>
            <iframe
              className="col-lg-12 col-md-12 col-sm-12"
              src={e.url}
              title="YouTube video player"
              allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
              allowFullScreen
            ></iframe>
          </CardContent>
        </Card>
      </Grid>
    );
  }
  return (
    <div className="content-area">
      <h1 className="title-bar">溯溪知識</h1>
      <div>
        <Grid container spacing={2}>
          {videos.map(videoCard)}
        </Grid>
      </div>
    </div>
  );
}
