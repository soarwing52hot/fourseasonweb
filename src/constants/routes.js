export const HOME = "/Home";
export const ABOUT_ARTICLES_OF_ASSOCIATION = "About/ArticlesOfAssociation";
export const ABOUT_HISTORY = "About/History";
export const ABOUT_CADRE = "About/Cadre";
export const ABOUT_KNOWLEDGE = "About/Knowledge";
export const ABOUT_DEV_HISTORY = "About/DevHistory";
export const ABOUT_CONTACT_US = "About/ContactUs";

export const USER_LOGIN = "User/Login";
export const USER_REGISTER = "User/Register";
export const USER_DISCLAIMERS = "User/Disclaimers";
export const USER_ACTIVITY_HISTORY = "User/ActivityHistory/:id";
export const USER_EDIT = "User/Edit/:id";
export const USER_PASSWORD = "User/Password/:id";

export const FINANCE_PAYMENT_LIST = "Finance/PaymentList";
export const FINANCE_PAYMENT_FORM = "Finance/PaymentForm";
export const FINANCE_PAYMENT_DETAIL = "Finance/PaymentDetail/:id";

export const CAMP_LIST = "/Camp/List";
export const CAMP_REGISTER = "/Camp/Register/:id";
export const CAMP_DETAIL = "/Camp/Detail/:id";
export const CAMP_PAYMENT_FORM = "/Camp/PaymentForm/:id";
export const CAMP_FORM = "/Camp/Form/:id";
export const NEW_CAMP = "Camp/Form/new";

export const ACTIVITY_DETAIL = "/Activity/Detail/:id";
export const ACTIVITY_LIST = "/Activity/List";
export const HISTORICAL_ACTIVITY_LIST = "/Activity/History";
export const EMERGENCY_CONTACT = "/Activity/EmergencyContact";
export const ACTIVITY_FORM = "/Activity/Form/:id";
export const NEW_ACTIVITY = "/Activity/Form/new";

export const MY_ACTIVITY = "/Leader/MyActivity";
export const ACTIVITY_CHART = "/Secretary/ActivityChart";
export const ACTIVITY_PARTICIPANT_CHART = "/Secretary/ActivityParticipantChart";
export const LEADER_CHART = "/Secretary/LeaderChart";
export const PARTICIPANT_CHART = "/Secretary/ParticipantChart";
export const CHANGE_GROUP = "/Secretary/ChangeGroup";
export const UPLOAD_BROCHURE = "/Secretary/UploadBrochure";
export const MEMBER_DATA = "/Secretary/MemberData";

export const ANNOUNCEMENT_LIST = "/Announcement/List";
export const ANNOUNCEMENT_DETAIL = "/Announcement/Detail/:id";
export const ANNOUNCEMENT_FORM = "/Announcement/Form/:id";

export const LEGACY_SCHEDULE_LIST = "/Legacy/Schedule/List";
