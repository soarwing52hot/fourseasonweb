import { lazy, Suspense } from "react";
import { BrowserRouter, Route, Routes, Navigate } from "react-router-dom";
import Footer from "@/Components/Footer";
import * as ROUTES from "@/constants/routes";

import PublicRoute from "./PublicRoute";
import PrivateRoute from "./PrivateRoute";
import { WithNavbar } from "@/Components/Bars/NavbarControl";
import Home from "@/Pages/Home";

const NotFound = lazy(() => import("@/Pages/NotFound"));
const ArticlesOfAssociation = lazy(() => import("@/Pages/About/ArticlesOfAssociation"));
const History = lazy(() => import("@/Pages/About/History"));
const Knowledge = lazy(() => import("@/Pages/About/Knowledge"));
const DevHistory = lazy(() => import("@/Pages/About/DevHistory"));
const ContactUs = lazy(() => import("@/Pages/About/ContactUs"));

const LeaderChart = lazy(() => import("@/Pages/Secretary/StatisticCharts/LeaderChart"));
const ParticipantChart = lazy(() => import("@/Pages/Secretary/StatisticCharts/ParticipantChart"));
const ActivityChart = lazy(() => import("@/Pages/Secretary/StatisticCharts/ActivityChart"));
const ActivityParticipantChart = lazy(() => import("@/Pages/Secretary/StatisticCharts/ActivityParticipantChart"));
const ChangeGroup = lazy(() => import("@/Pages/Secretary/ChangeGroup"));
const UploadBrochure = lazy(() => import("@/Pages/Secretary/UploadBrochure"));
const MemberData = lazy(() => import("@/Pages/Secretary/MemberData"));

const LoginPage = lazy(() => import("@/Pages/User/Login"));
const Register = lazy(() => import("@/Pages/User/Register"));
const Disclaimers = lazy(() => import("@/Pages/User/Disclaimers"));
const Edit = lazy(() => import("@/Pages/User/Edit"));
const Password = lazy(() => import("@/Pages/User/Password"));
const ActivityHistory = lazy(() => import("@/Pages/User/ActivityHistory"));

const CampList = lazy(() => import("@/Pages/Camp/List"));
const CampDetail = lazy(() => import("@/Pages/Camp/Detail"));
const CampRegister = lazy(() => import("@/Pages/Camp/Register"));
const CampPaymentForm = lazy(() => import("@/Pages/Camp/PaymentForm"));
const CampForm = lazy(() => import("@/Pages/Camp/Form"));

const ActivityList = lazy(() => import("@/Pages/Activity/List"));
const HistoricalActivityList = lazy(() => import("@/Pages/Activity/HistoryList"));
const ActivityDetail = lazy(() => import("@/Pages/Activity/Detail"));
const ActivityForm = lazy(() => import("@/Pages/Activity/Form"));
const EmergencyContact = lazy(() => import("@/Pages/Activity/EmergencyContact"));

const MyActivity = lazy(() => import("@/Pages/Leader/MyActivity"));

const PaymentList = lazy(() => import("@/Pages/Finance/List"));
const PaymentDetail = lazy(() => import("@/Pages/Finance/Detail"));
const PaymentForm = lazy(() => import("@/Pages/Finance/Form"));

const AnnouncementList = lazy(() => import("@/Pages/Announcement/List"));
const AnnouncementDetail = lazy(() => import("@/Pages/Announcement/Detail"));
const AnnouncementForm = lazy(() => import("@/Pages/Announcement/Form"));

const LegacyScheduleList = lazy(() => import("@/Pages/Legacy/Schedule/List.jsx"));
import NavBar from "@/Components/Bars/NavBar";

export default function AppRouter() {
  return (
    <Suspense>
      <BrowserRouter>
        <div className="App main-container">
          <div className="main-body">
            <NavBar />
            <Routes>
              <Route exact path="/" element={<PublicRoute />}>
                {/* 公用頁面 */}
                <Route path={"/"} element={<Home />} />
                <Route path={ROUTES.HOME} element={<Navigate replace to="/" />} />
                <Route path="*" element={<NotFound />} />

                {/* 會務相關 */}
                <Route path={ROUTES.ABOUT_ARTICLES_OF_ASSOCIATION} element={<ArticlesOfAssociation />} />
                <Route path={ROUTES.ABOUT_HISTORY} element={<History />} />
                <Route path={ROUTES.ABOUT_KNOWLEDGE} element={<Knowledge />} />
                <Route path={ROUTES.ABOUT_DEV_HISTORY} element={<DevHistory />} />
                <Route path={ROUTES.ABOUT_CONTACT_US} element={<ContactUs />} />
                {/* 使用者相關 */}
                <Route path={ROUTES.USER_LOGIN} element={<LoginPage />} />
                <Route path={ROUTES.USER_REGISTER} element={<Register />} />
                <Route path={ROUTES.USER_DISCLAIMERS} element={<Disclaimers />} />
                {/* 營隊相關 */}
                <Route path={ROUTES.CAMP_LIST} element={<CampList />} />
                <Route path={ROUTES.CAMP_DETAIL} element={<CampDetail />} />
                <Route path={ROUTES.CAMP_PAYMENT_FORM} element={<CampPaymentForm />} />
                <Route path={ROUTES.CAMP_REGISTER} element={<CampRegister />} />
                {/* 活動相關 */}
                <Route path={ROUTES.ACTIVITY_LIST} element={<ActivityList />} />
                <Route path={ROUTES.LEGACY_SCHEDULE_LIST} element={<LegacyScheduleList />} />
                {/* 公告 */}
                <Route path={ROUTES.ANNOUNCEMENT_LIST} element={<AnnouncementList />} />
                <Route path={ROUTES.ANNOUNCEMENT_DETAIL} element={<AnnouncementDetail />} />
              </Route>

              <Route exact path="/" element={<PrivateRoute />}>
                {/* 使用者相關 */}
                <Route path={ROUTES.USER_EDIT} element={<Edit />} />
                <Route path={ROUTES.USER_PASSWORD} element={<Password />} />

                {/* 財務相關 */}
                <Route path={ROUTES.FINANCE_PAYMENT_LIST} element={<PaymentList />} />
                <Route path={ROUTES.FINANCE_PAYMENT_FORM} element={<PaymentForm />} />
                <Route path={ROUTES.FINANCE_PAYMENT_DETAIL} element={<PaymentDetail />} />

                {/* 營隊相關 */}
                <Route path={ROUTES.CAMP_FORM} element={<CampForm />} />
                {/* 活動相關 */}
                <Route path={ROUTES.USER_ACTIVITY_HISTORY} element={<ActivityHistory />} />
                <Route exact path={ROUTES.HISTORICAL_ACTIVITY_LIST} element={<HistoricalActivityList />} />
                <Route path={ROUTES.ACTIVITY_DETAIL} element={<ActivityDetail />} />
                <Route path={ROUTES.ACTIVITY_FORM} element={<ActivityForm />} />
                <Route path={ROUTES.EMERGENCY_CONTACT} element={<EmergencyContact />} />
              </Route>
              <Route exact path="/" element={<PrivateRoute role="Leader" />}>
                {/* 領隊相關 */}
                <Route path={ROUTES.MY_ACTIVITY} element={<MyActivity />} />
              </Route>
              {/* 會務相關 */}
              <Route exact path="/" element={<PrivateRoute role="Secretary" />}>
                <Route path={ROUTES.ACTIVITY_CHART} element={<ActivityChart />} />
                <Route path={ROUTES.ACTIVITY_PARTICIPANT_CHART} element={<ActivityParticipantChart />} />
                <Route path={ROUTES.LEADER_CHART} element={<LeaderChart />} />
                <Route path={ROUTES.PARTICIPANT_CHART} element={<ParticipantChart />} />
                <Route path={ROUTES.CHANGE_GROUP} element={<ChangeGroup />} />
                <Route path={ROUTES.UPLOAD_BROCHURE} element={<UploadBrochure />} />
                <Route path={ROUTES.MEMBER_DATA} element={<MemberData />} />
                <Route path={ROUTES.ANNOUNCEMENT_FORM} element={<AnnouncementForm />} />
              </Route>
            </Routes>
          </div>
          <Footer />
        </div>
      </BrowserRouter>
    </Suspense>
  );
}
