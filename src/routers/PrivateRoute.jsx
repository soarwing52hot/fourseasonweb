import PropTypes from "prop-types";

import { Navigate, Outlet } from "react-router-dom";
import { useSelector } from "react-redux";
import { USER_LOGIN, HOME } from "@/constants/routes";

export default function PrivateRoute(props) {
  PrivateRoute.propTypes = {
    role: PropTypes.string
  };

  const user = useSelector(state => state.user);

  if (!user) {
    return <Navigate to={USER_LOGIN} />;
  }

  const requiredRole = props.role;
  if (requiredRole) {
    if (!user?.group.includes(requiredRole)) {
      alert("權限不足！");
      return <Navigate to={HOME} />;
    }
  }

  return <Outlet />;
}
