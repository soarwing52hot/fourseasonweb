import moment from "moment-timezone";

export function handleDatetime(data, colName) {
  const date = data[`${colName}_date`];
  const time = data[`${colName}_time`];
  let value = `${date.format("YYYY-MM-DD")} ${time.format("HH:mm")}`;
  let momentValue = moment(value, "YYYY-MM-DD HH:mm").tz("Asia/Taipei");
  data[colName] = momentValue.format();
}
