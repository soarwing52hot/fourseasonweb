import axios from "axios";
import { RefreshToken, TokenExpired, getUserToken } from "@/Components/Authentication/UserStatus";

const createInstance = useToken => {
  const instance = axios.create({
    baseURL: import.meta.env.VITE_API_URL, //JSON-Server端口位置
    timeout: 10000
  });

  if (!useToken) {
    return instance;
  }

  instance.interceptors.request.use(
    config => {
      const token = getUserToken();
      config.headers.Authorization = `Bearer ${token}`;
      return config;
    },
    error => {
      return Promise.reject(error);
    }
  );

  instance.interceptors.response.use(
    function(response) {
      return response;
    },
    async function(error) {
      let config = error.config;
      if (error.response?.status == 401 && config.url == "auth/refresh-token/") {
        alert("請重新登入");
        document.location.href = "/";
        return Promise.reject(error);
      }
      if (error.response?.status == 401 && !config._retry) {
        config._retry = true;

        let tokenExpired = TokenExpired();

        if (tokenExpired) {
          try {
            let res = await RefreshToken();
            config.headers.Authorization = `Bearer ${res}`;
            return instance(config);
          } catch (err) {
            alert("請重新登入");
            document.location.href = "/";
            return Promise.reject(error);
          }
        }
      }
      if (error?.response?.data?.message) {
        alert(error?.response?.data?.message);
      } else {
        alert(JSON.stringify(error.response.data));
      }
      return Promise.reject(error);
    }
  );

  return instance;
};

const basicInstance = createInstance(false);
const tokenInstance = createInstance(true);

export { basicInstance, tokenInstance };
