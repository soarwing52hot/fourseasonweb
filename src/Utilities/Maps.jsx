import { hotSpring, shiayao, soyuan, tankan, yindo, mohei, milan } from "@/Style/images/activityStamps";

const stampMap = new Map([
  ["溫泉營地", hotSpring],
  ["無比逍遙", shiayao],
  ["保證溯源", soyuan],
  ["別問探勘", tankan],
  ["鐵板硬斗", yindo],
  ["鐵定摸黑", mohei],
  ["糜爛至極", milan]
]);

const PaymentMapping = {
  newMember: "1",
  oldMember: "2",
  other: "3"
};

const IdentityType = {
  Leader: "L",
  Participant: "P",
  EmergencyConductor: "E"
};

export { stampMap, PaymentMapping, IdentityType };
