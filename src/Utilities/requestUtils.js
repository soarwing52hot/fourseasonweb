import { basicInstance, tokenInstance } from "@/Utilities/axios";

export const BasicGet = url => basicInstance.get(url);

export const AddFilters = (url, keys, data) => {
  let newUrl = url;

  Object.entries(keys).forEach(([key, value]) => {
    if (data[key]) {
      newUrl += `${value}=${data[key]}&`;
    }
  });

  if (newUrl.at(-1) === "&") {
    newUrl = newUrl.slice(0, -1);
  }
  return newUrl;
};

export const BasicPost = (url, data) => basicInstance.post(url, data);

export const TokenGet = url => tokenInstance.get(url);
export const TokenPost = (url, data) => tokenInstance.post(url, data);
export const TokenPut = (url, data) => tokenInstance.put(url, data);
export const TokenPatch = (url, data) => tokenInstance.patch(url, data);

export const TokenDownloadFile = (url, fileName) => {
  tokenInstance
    .get(url, {
      responseType: "blob"
    })
    .then(response => {
      const href = URL.createObjectURL(response.data);

      const link = document.createElement("a");
      link.href = href;
      link.setAttribute("download", fileName); //or any other extension
      document.body.appendChild(link);
      link.click();

      document.body.removeChild(link);
      URL.revokeObjectURL(href);
    })
    .catch(err => {
      if (err.response.status == 404) {
        alert("找不到該檔案!");
      }
    });
};

export const TokenUploadForm = (url, formData) => {
  let config = {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  };

  return tokenInstance.post(url, formData, config);
};
