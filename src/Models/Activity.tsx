export interface IActivityResponse {
  count: number;
  next: null;
  previous: null;
  results: Array<IActivity>;
}

export interface IActivity {
  id: number;
  title: string;
  content: string;
  active: boolean;
  can_register: boolean;
  is_test: boolean;
  announcement: string;
  created_on: string;
  trip_date_start: string;
  trip_date_end: string;
  activity_type: string;
  activity_requirements: string;
  activity_requirements_count: number;
  participant_limit: number;
  leaders: number[];
  participants: number[];
  stamp: number[];
}
