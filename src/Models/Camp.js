export class Camp {}

export class CampParticipant {
  name = "";
  camp = "";
  gender = "";
  birth_date = "";
  birth_location = "";
  citizen_id = "";
  phone = "";
  land_line = "";
  email = "";
  postal_code = "";
  address = "";
  emergency_contact_name = "";
  emergency_contact_phone = "";
  emergency_contact_relation = "";
  facebook_name = "";
  line_id = "";
  experience = "";
}

export class CampPayment {
  camp_participant = undefined;
  name = "";
  email = "";
  phone = "";
  account_number = 0;
  memo = "";
  transfer_date = "";
}
