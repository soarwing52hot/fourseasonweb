import { BasicGet, AddFilters, BasicPost, TokenGet, TokenPost, TokenPut, TokenPatch, TokenDownloadFile } from "@/Utilities/requestUtils";

export const getCommittee = (page = 1) => BasicGet(`/about/committee/?page=${page}`);
export const getVideo = (page = 1) => BasicGet(`/about/videos/?page=${page}`);
