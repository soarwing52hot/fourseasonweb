import { BasicGet, BasicPost, TokenGet, TokenPost, TokenPut } from "@/Utilities/requestUtils";

export const getPaymentList = (page = 1) => BasicGet(`/finance/Payment/?page=${page}`);
export const getPaymentTypes = () => BasicGet(`/finance/types/`);

export const postPayment = data => BasicPost(`/finance/Payment/`, data);
export const tokenPostPayment = data => TokenPost(`/finance/Payment/`, data);

export const putPaymentConfirm = (id, data) => TokenPut(`/finance/PaymentConfirm/${id}/`, data);

export const sendInvoiceMail = id => BasicGet(`finance/Invoice/${id}/send`);

export const getInvoice = hash_id => BasicGet(`finance/Invoice/${hash_id}`);
