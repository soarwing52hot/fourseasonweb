import { BasicGet, AddFilters, BasicPost, TokenGet, TokenPost, TokenPut, TokenPatch, TokenDownloadFile } from "@/Utilities/requestUtils";

export const getAnnouncementTypes = () => BasicGet(`/announcements/options/types/`);
export const getAvailableAnnouncementTypes = () => TokenGet(`/announcements/options/availableTypes/`);

const announcementListKey = {
  creation_date: "creation_date__gte",
  announce_type: "announce_type__iexact",
  page: "page"
};
export const getAnnouncementList = data => BasicGet(AddFilters(`/announcements/info/?`, announcementListKey, data));

export const getAnnouncement = id => BasicGet(`/announcements/info/${id}/`);
export const postAnnouncement = data => TokenPost(`/announcements/info/`, data);
export const patchAnnouncement = (id, data) => TokenPatch(`/announcements/info/${id}/`, data);

export const postSendEmail = data => TokenPost(`/announcements/sendEmail/`, data);
