import { BasicGet, AddFilters, BasicPost, TokenGet, TokenPatch, TokenPost, TokenPut, TokenUploadForm } from "@/Utilities/requestUtils";

export const login = data => BasicPost("auth/login/", data);
export const refreshToken = data => BasicPost("auth/refresh-token/", data);

const userListKey = {
  username: "username__icontains",
  membership_number: "membership_number",
  page: "page"
};
export const getUserList = data => TokenGet(AddFilters(`/user/CustomUser/?`, userListKey, data));
export const getUser = id => TokenGet(`/user/CustomUser/${id}/`);
export const postUser = data => TokenPost("/user/create/", data);
export const patchUser = (id, data) => TokenPatch(`/user/CustomUser/${id}/`, data);
export const changePassword = (id, data) => TokenPut(`/user/change_password/${id}/`, data);

export const uploadProfilePhoto = data => TokenUploadForm(`/user/ProfilePhoto/`, data);

export const getGroups = () => TokenGet(`/user/group/`);
export const getUsersGroups = (keyword, page = 1) => TokenGet(`/user/getGroups/${keyword}?page=${page}`);
export const changeGroup = data => TokenPut(`/user/changeGroup/`, data);
