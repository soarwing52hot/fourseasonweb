import { BasicGet, TokenGet } from "@/Utilities/requestUtils";

export const getLegacyScheduleList = (size, page) => TokenGet(`legacy/schedule?size=${size}&page=${page}`);
export const getLegacyScheduleListByLeaderId = (size, page, leader_id) => TokenGet(`legacy/schedule?size=${size}&page=${page}&leader_id=${leader_id}`);
export const getLegacyScheduleListByMemberId = (size, page, member_id) => TokenGet(`legacy/schedule?size=${size}&page=${page}&member_id=${member_id}`);

export const anonymousGetLegacyScheduleList = (size, page) => BasicGet(`legacy/schedule?size=${size}&page=${page}`);
export const anonymousGetLegacyScheduleListByLeaderId = (size, page, leader_id) => BasicGet(`legacy/schedule?size=${size}&page=${page}&leader_id=${leader_id}`);
export const anonymousGetLegacyScheduleListByMemberId = (size, page, member_id) => BasicGet(`legacy/schedule?size=${size}&page=${page}&member_id=${member_id}`);