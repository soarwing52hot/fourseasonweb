import { BasicGet, AddFilters, BasicPost, TokenGet, TokenPost, TokenPut, TokenPatch, TokenDownloadFile } from "@/Utilities/requestUtils";

// Activity
export const getActivity = id => BasicGet(`/activities/info/${id}`);

const activityListKey = {
  title: "title__icontains",
  trip_date_start: "trip_date_start__gte",
  trip_date_end: "trip_date_end__lte",
  leader: "leaders__membership_number",
  membership_number: "participants__membership_number",
  page: "page"
};
export const getActivityList = data => BasicGet(AddFilters("/activities/info/?", activityListKey, data));
export const getActivityListWithToken = data => TokenGet(AddFilters("/activities/info/?", activityListKey, data));

export const tokenGetActivityRecord = data => TokenGet(AddFilters("/activities/record/?", activityListKey, data));

const memberLogKey = {
  id: "activity__id",
  page: "page"
};
export const getMemberLog = data => BasicGet(AddFilters("/activities/memberLog/?", memberLogKey, data));

export const getActivityWithUser = id => TokenGet(`/activities/info/${id}/`);

export const postActivity = data => TokenPost(`/activities/info/`, data);
export const patchActivity = (id, data) => TokenPatch(`/activities/info/${id}/`, data);
export const getInsurance = (id, fileName) => TokenDownloadFile(`/activities/insurance/${id}`, fileName);
export const getNameList = (id, fileName) => TokenDownloadFile(`/activities/nameList/${id}`, fileName);

// Charts
export const getLeaderChart = year => TokenGet(`/activities/leaderChart/${year}`);
export const getParticipantChart = year => TokenGet(`/activities/participantChart/${year}`);
export const getActivityCount = year => TokenGet(`/activities/activityCount/${year}`);
export const getActivityParticipantCount = year => TokenGet(`/activities/activityParticipantCount/${year}`);
// Participant
export const updateActivitiesPaymentStatus = (id, data) => TokenPut(`/activities/ActivitiesPayment/${id}/`, data);
export const registerActivitiesParticipant = (id, data) => TokenPut(`/activities/register/${id}/`, data);

export const postSendEmail = data => TokenPost(`/activities/sendEmail/`, data);

// Options
export const getActivitieStampList = () => BasicGet(`/activities/options/stamps/`);
export const getTypeOptions = () => BasicGet(`/activities/options/types/`);
export const getRequirementOptions = () => BasicGet(`/activities/options/requirements/`);
