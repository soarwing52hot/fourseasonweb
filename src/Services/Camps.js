import { BasicGet, BasicPost, TokenGet, TokenPost, TokenPut, TokenPatch, TokenDownloadFile, TokenUploadForm } from "@/Utilities/requestUtils";

//camp
export const getCamp = id => BasicGet(`/camps/info/${id}/`);
export const postCamp = data => TokenPost(`/camps/info/`, data);
export const patchCamp = (id, data) => TokenPatch(`/camps/info/${id}/`, data);
export const getCampByDate = (dateStart, dateEnd) => BasicGet(`/camps/info/?trip_date_start__gte=${dateStart}&trip_date_end__lte=${dateEnd}`);
export const getInsurance = (id, fileName) => TokenDownloadFile(`/camps/insurance/${id}`, fileName);

export const getCampRecordByCitizenID = citizen_id => TokenGet(`/camps/record?citizen_id__iexact=${citizen_id}`);

//camp staff
export const getCampStaff = id => BasicGet(`/camps/staff/${id}`);
export const patchCampStaff = (id, data) => TokenPatch(`/camps/staff/${id}/`, data);
export const getStaffExcel = (id, fileName) => TokenDownloadFile(`/camps/staffExcel/${id}`, fileName);
export const getParticipantExcel = (id, fileName) => TokenDownloadFile(`/camps/participantExcel/${id}`, fileName);

//camp participant
export const getCampParticipant = id => BasicGet(`/camps/participant/${id}`);
export const postCampParticipant = data => BasicPost(`/camps/participant/`, data);
export const getCampParticipantByCampId = id => BasicGet(`/camps/participant/?camp__id__iexact=${id}`);

//camp payment
export const getCampPaymentByParticipantId = id => BasicGet(`/camps/getCampPaymentByParticipantId/${id}`);
export const postCampPayment = data => BasicPost(`camps/payment/`, data);
export const updateCampPaymentStatus = (id, data) => TokenPut(`/camps/payment/${id}/`, data);

export const getCampTypes = () => BasicGet(`/camps/types/`);
export const getCampRecord = membershipNumber => BasicGet(`/camps/record/${membershipNumber}?page=1/`);
export const downloadBrochure = campType => BasicGet(`camps/brochure/${campType}/`);
export const uploadBrochure = data => TokenUploadForm(`camps/brochure/`, data);
