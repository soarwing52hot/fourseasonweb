import { useState } from "react";

export default function usePagination() {
  const [pages, setPages] = useState({ prev: null, next: null });

  function setNewPages(data) {
    let prev;
    if (data.previous != null) {
      prev = new URL(data.previous).searchParams.get("page");
      if (!prev) {
        prev = 1;
      }
    }
    let next;
    if (data.next != null) {
      next = new URL(data.next).searchParams.get("page");
    }
    let pageData = { prev: prev, next: next };
    setPages(pageData);
  }

  return { pages, setNewPages };
}
