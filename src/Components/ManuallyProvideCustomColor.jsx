import { createTheme, ThemeProvider } from "@mui/material/styles";

const theme = createTheme({
  palette: {
    ochre: {
      main: "#E3D026",
      light: "#E9DB5D",
      dark: "#A29415",
      contrastText: "#242105"
    },
    grey: {
      main: "#808080",
      light: "#018175",
      dark: "#018175",
      contrastText: "#fff"
    },
    primary: {
      main: "#018175",
      light: "#018175",
      dark: "#018175",
      contrastText: "#fff"
    },
    secondary: {
      main: "#10B68D",
      light: "#ff7961",
      dark: "#ba000d",
      contrastText: "#000"
    },
    third: {
      main: "#94DD8B",
      light: "#ff7961",
      dark: "#ba000d",
      contrastText: "#fff"
    },
    forth: {
      main: "#FEE3B2",
      light: "#ff7961",
      dark: "#ba000d",
      contrastText: "#000"
    }
  }
});

export default function ManuallyProvideCustomColor({ children }) {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
}
