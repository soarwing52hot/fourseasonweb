import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { useDispatch } from "react-redux";
import { setUserByToken } from "@/Store/userSlice";

Init.propTypes = {
  children: PropTypes.node.isRequired
};

export default function Init({ children }) {
  const dispatch = useDispatch();
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    dispatch(setUserByToken());
    setLoading(false);
  }, []);

  if (loading) {
    return <></>;
  }
  return <>{children}</>;
}
