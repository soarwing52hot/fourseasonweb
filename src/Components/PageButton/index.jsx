import PropTypes from "prop-types";
import { Grid, Button } from "@mui/material";

export default function PageButton({ pages, func }) {
  PageButton.propTypes = {
    pages: PropTypes.object,
    func: PropTypes.func
  };

  return (
    <Grid container>
      <Grid item>
        {pages?.prev ? (
          <Button
            variant="contained"
            onClick={() => {
              func(pages?.prev);
            }}
          >
            上一頁
          </Button>
        ) : null}
      </Grid>
      <Grid item>
        {pages?.next ? (
          <Button
            variant="contained"
            onClick={() => {
              func(pages?.next);
            }}
          >
            下一頁
          </Button>
        ) : null}
      </Grid>
    </Grid>
  );
}
