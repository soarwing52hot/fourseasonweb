import PropTypes from "prop-types";
import { Button, Dialog, DialogTitle, DialogContent, DialogActions } from "@mui/material";

import IconButton from "@mui/material/IconButton";
import CloseIcon from "@mui/icons-material/Close";

export default function BasicConfirm(props) {
  BasicConfirm.propTypes = {
    action: PropTypes.func,
    content: PropTypes.string,
    modalShow: PropTypes.bool,
    title: PropTypes.string
  };

  function closeModal(state = false) {
    props.action(state);
  }

  return (
    <Dialog open={props.modalShow} onClose={closeModal} fullWidth>
      <DialogTitle sx={{ m: 0, p: 2 }}>{props.title}</DialogTitle>
      <IconButton
        aria-label="close"
        onClick={closeModal}
        sx={{
          position: "absolute",
          right: 8,
          top: 8,
          color: theme => theme.palette.grey[500]
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent dividers>{props.content}</DialogContent>

      <DialogActions>
        <Button color="error" variant="outlined" onClick={() => closeModal(false)}>
          取消
        </Button>
        <Button color="success" variant="contained" onClick={() => closeModal(true)}>
          確認
        </Button>
      </DialogActions>
    </Dialog>
  );
}
