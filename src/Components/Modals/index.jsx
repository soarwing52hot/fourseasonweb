import BasicConfirm from "./BasicConfirm";
import ModalSendEmail from "./ModalSendEmail";
import confirmationModalTemplate from "./ConfirmationModalTemplate";

export { BasicConfirm, confirmationModalTemplate, ModalSendEmail };
