import PropTypes from "prop-types";
import { useRef } from "react";
import { Button, Dialog, DialogTitle, DialogContent, DialogActions, TextField } from "@mui/material";

export default function ModalSendEmail(props) {
  ModalSendEmail.propTypes = {
    action: PropTypes.func,
    modalShow: PropTypes.bool,
    title: PropTypes.string,
    defaultValue: PropTypes.object
  };

  const form = useRef({});

  function closeModal(state = false) {
    let ele = form.current.elements;
    let data = {
      subject: ele["subject"].value,
      message: ele["message"].value
    };
    props.action(state, data);
  }

  return (
    <Dialog open={props.modalShow} onClose={() => closeModal(false)} fullWidth>
      <DialogTitle>{props.title}</DialogTitle>
      <DialogContent>
        <form noValidate ref={form}>
          <h5>主旨</h5>
          <TextField fullWidth id="subject" name="subject" variant="outlined" defaultValue={props.defaultValue.title} />
          <h5>內文</h5>
          <TextField fullWidth id="message" message="title" variant="outlined" rows={4} multiline defaultValue={props.defaultValue.content} />
        </form>
      </DialogContent>
      <DialogActions>
        <Button color="success" variant="contained" onClick={() => closeModal(true)}>
          確認
        </Button>
        <Button color="secondary" variant="outlined" onClick={() => closeModal(false)}>
          取消
        </Button>
      </DialogActions>
    </Dialog>
  );
}
