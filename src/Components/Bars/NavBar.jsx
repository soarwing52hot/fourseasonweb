import { useState, useRef, useMemo, useEffect } from "react";
import { useNavigate, Link } from "react-router-dom";
import { ImgLogo } from "@/Style/images";
import { AppBar, List, ListItem, ListItemButton, ListItemText, Collapse, Drawer, Paper, Popper, MenuList } from "@mui/material";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";

import Container from "@mui/material/Container";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Unstable_Grid2";
import MenuItem from "@mui/material/MenuItem";

import ClickAwayListener from "@mui/material/ClickAwayListener";

import Grow from "@mui/material/Grow";

import ListItemIcon from "@mui/material/ListItemIcon";
import MenuIcon from "@mui/icons-material/Menu";
import LoginIcon from "@mui/icons-material/Login";
import Logout from "@mui/icons-material/Logout";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";

import { useSelector, useDispatch } from "react-redux";
import { ImgLogoLight } from "@/Style/images";
import * as ROUTES from "@/constants/routes";
import { logout } from "@/Store/userSlice";

const drawerWidth = "80vw";

export default function NavBar() {
  const history = useNavigate();
  const user = useSelector(state => state.user);
  const dispatch = useDispatch();

  // Menu的錨點
  const [anchorElNav, setAnchorElNav] = useState(null);
  // 是否顯示
  const [showNav, setShowNav] = useState(false);
  // 樹狀的狀態紀錄
  const [menuOpenState, setMenuOpenState] = useState({});
  // 桌面板的時候menu的內容
  const [currentPage, setCurrentPage] = useState({});

  const isSecretary = user.group.includes("Secretary");
  const isFinance = user.group.includes("Finance");
  const isLeader = user.group.includes("Leader");
  const isStaff = [isSecretary, isLeader, isFinance].some(e => e == true);
  const pages = useMemo(() => {
    return [
      {
        name: "關於四季",
        auth: true,
        items: [
          { name: "組織章程", route: ROUTES.ABOUT_ARTICLES_OF_ASSOCIATION },
          { name: "歷史沿革", route: ROUTES.ABOUT_HISTORY },
          { name: "網站歷史", route: ROUTES.ABOUT_DEV_HISTORY },
          { name: "溯溪知識", route: ROUTES.ABOUT_KNOWLEDGE },
          { name: "聯絡我們", route: ROUTES.ABOUT_CONTACT_US }
        ]
      },
      {
        name: "隊伍與營隊",
        auth: true,
        items: [
          { name: "會內活動", route: ROUTES.ACTIVITY_LIST },
          { name: "歷史查詢", route: ROUTES.HISTORICAL_ACTIVITY_LIST },
          { name: "營隊報名", route: ROUTES.CAMP_LIST },
          { name: "舊官網活動", route: ROUTES.LEGACY_SCHEDULE_LIST },
          { name: "緊急聯絡資訊", route: ROUTES.EMERGENCY_CONTACT }
        ]
      },
      {
        name: "會員中心",
        auth: user.isLoggedin,
        items: [
          { name: "公告", route: ROUTES.ANNOUNCEMENT_LIST },
          { name: "修改會員資料", route: ROUTES.USER_EDIT.replace(":id", user.membership_number) },
          { name: "修改會員密碼", route: ROUTES.USER_PASSWORD.replace(":id", user.membership_number) },
          { name: "新年度會費繳款填寫", route: ROUTES.FINANCE_PAYMENT_FORM },
          { name: "我的活動紀錄", route: ROUTES.USER_ACTIVITY_HISTORY.replace(":id", user.membership_number) }
        ]
      },
      {
        name: "幹部群組",
        auth: isStaff,
        items: [
          {
            name: "秘書長",
            auth: isSecretary,
            defaultOpen: true,
            items: [
              { name: "設定幹部屬性", route: ROUTES.CHANGE_GROUP },
              { name: "會員資料總覽", route: ROUTES.MEMBER_DATA },
              { name: "營隊簡章上傳", route: ROUTES.UPLOAD_BROCHURE },
              { name: "建立營隊", route: ROUTES.NEW_CAMP },
              { name: "帶隊王統計", route: ROUTES.LEADER_CHART },
              { name: "出隊王統計", route: ROUTES.PARTICIPANT_CHART },
              { name: "開隊數量統計", route: ROUTES.ACTIVITY_CHART },
              { name: "出隊人數統計", route: ROUTES.ACTIVITY_PARTICIPANT_CHART }
            ]
          },
          { name: "財務", auth: isFinance, defaultOpen: true, items: [{ name: "匯款確認", route: ROUTES.FINANCE_PAYMENT_LIST }] },
          {
            name: "領隊",
            auth: isLeader,
            defaultOpen: true,
            items: [
              { name: "我的行程", route: ROUTES.MY_ACTIVITY },
              { name: "開新隊伍", route: ROUTES.NEW_ACTIVITY }
            ]
          }
        ]
      }
    ];
  }, [user]);

  useEffect(() => {
    let temp = {};
    pages.forEach(e => parseMenuDefaultState(e, temp));
    setMenuOpenState(temp);
  }, []);

  function parseMenuDefaultState(page, temp) {
    if (page.defaultOpen) {
      temp[page.name] = true;
    }

    if (page.items) {
      page.items.forEach(e => parseMenuDefaultState(e, temp));
    }
  }
  const LogoutButton = () => {
    dispatch(logout());
    closeMenu();
    setTimeout(() => {
      history("/Home");
    }, 300);
  };

  function LoginButton() {
    closeMenu();
    setTimeout(() => {
      history("User/Login");
    }, 300);
  }

  function closeMenu() {
    setAnchorElNav(null);
    setShowNav(false);
  }

  const handleClick = page => {
    if (page.items) {
      const colName = page.name;
      let temp = Object.assign({}, menuOpenState);
      temp[colName] = !menuOpenState[colName];
      setMenuOpenState(temp);
    } else {
      setShowNav(false);
      // 等到關閉的動作結束後才跳頁
      setTimeout(() => {
        history(page.route);
      }, 300);
    }
  };

  function handleOpenNavMenu() {
    setShowNav(true);
  }

  function navItem(page, i = 0) {
    return (
      <div key={`${page.name}-main-div`}>
        {page.auth != false && (
          <>
            <ListItemButton onClick={() => handleClick(page)} key={`${page.name}-btn`} sx={{ pl: i * 2 }}>
              <ListItemText primary={page.name} key={`${page.name}-text`} />
              {page.items && menuOpenState[page.name] && <ExpandLess />}
              {page.items && !menuOpenState[page.name] && <ExpandMore />}
            </ListItemButton>
            {page.items && (
              <>
                <Collapse in={menuOpenState[page.name] ? true : false} timeout="auto" unmountOnExit key={`${page.name}-collapse`}>
                  <List component="div" disablePadding key={`${page.name}-collapse-div`}>
                    {page.items?.map(it => navItem(it, i + 1))}
                  </List>
                </Collapse>
              </>
            )}
          </>
        )}
      </div>
    );
  }

  function fullPageNaveItem(page) {
    return (
      <div key={`${page.name}-main-div`}>
        {page.auth != false && (
          <>
            <Button key={`${page.name}-btn`} onMouseOver={event => handleToggle(event, page)} sx={{ my: 2, color: "rgb(137, 231, 145)", display: "block" }}>
              {page.name}
            </Button>
          </>
        )}
      </div>
    );
  }

  function handleToggle(event, page) {
    setAnchorElNav(event.currentTarget);
    setCurrentPage(page);
    setShowNav(true);
    handleClick(page);
  }

  function handleClose() {
    setShowNav(false);
  }

  return (
    <AppBar position="sticky" sx={{ bgcolor: "rgb(33, 37, 41)" }}>
      <Container maxWidth="xl">
        <Toolbar disableGutters>
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton size="large" onClick={handleOpenNavMenu} color="third">
              <MenuIcon />
            </IconButton>
            <nav>
              <Toolbar
                sx={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "flex-end",
                  px: [1]
                }}
              >
                <Drawer
                  open={showNav}
                  onClose={closeMenu}
                  ModalProps={{
                    keepMounted: true // Better open performance on mobile.
                  }}
                  sx={{
                    display: { xs: "block", md: "none" }
                  }}
                >
                  <Box sx={{ textAlign: "center", m: 1, width: { xs: "80vw" } }}>
                    <img
                      src={ImgLogo}
                      style={{
                        width: 100,
                        height: 100
                      }}
                    />
                  </Box>
                  <List>
                    {pages.map(e => navItem(e, 1))}
                    {user.isLoggedin ? (
                      <ListItemButton onClick={LogoutButton} key="logout-btn">
                        <ListItemIcon>
                          <Logout fontSize="small" />
                        </ListItemIcon>
                        <ListItemText primary="登出" />
                      </ListItemButton>
                    ) : (
                      <ListItemButton onClick={LoginButton} key="login-btn">
                        <ListItemIcon>
                          <LoginIcon fontSize="small" />
                        </ListItemIcon>
                        <ListItemText primary="登入" />
                      </ListItemButton>
                    )}
                  </List>
                </Drawer>
              </Toolbar>
            </nav>
          </Box>

          <Typography
            variant="img"
            noWrap
            component="a"
            href="/"
            sx={{
              mr: 5,
              display: "flex",
              flexGrow: { xs: 1, md: 0 }
            }}
          >
            <img src={ImgLogoLight} alt="logo" height="30" />
          </Typography>
          <Box sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
            {pages.map(fullPageNaveItem)}
            <Popper open={showNav && anchorElNav != null} anchorEl={anchorElNav} role={undefined} placement="bottom-start" transition disablePortal className="menu-popper">
              {({ TransitionProps, placement }) => (
                <Grow
                  {...TransitionProps}
                  style={{
                    transformOrigin: placement === "bottom-start" ? "left top" : "left bottom"
                  }}
                >
                  <Paper>
                    <ClickAwayListener onClickAway={handleClose}>
                      <MenuList autoFocusItem={menuOpenState[currentPage.name]}>{currentPage.items.map(e => navItem(e, 1))}</MenuList>
                    </ClickAwayListener>
                  </Paper>
                </Grow>
              )}
            </Popper>
          </Box>
          <Box sx={{ display: { xs: "none", md: "flex" } }}>
            {user.isLoggedin ? (
              <>
                <Grid container spacing={2}>
                  <Grid xs="auto">
                    <Typography variant="body1" className="welcome-member">
                      歡迎！ 會員：{user.name}
                    </Typography>
                  </Grid>
                  <Grid xs="auto">
                    <Button onClick={LogoutButton} variant="contained" color="secondary">
                      登出
                    </Button>
                  </Grid>
                </Grid>
              </>
            ) : (
              <Button onClick={LoginButton} variant="contained" color="success">
                登入
              </Button>
            )}
          </Box>
        </Toolbar>
      </Container>
    </AppBar>
  );
}
