import { getCookie, setToken } from "@/Utilities/cookie";
import { jwtDecode } from "jwt-decode";
import { refreshToken } from "@/Services/User";

var AuthTokenName = "jwt";

function setRefreshToken(token) {
  setToken(token, "refresh");
}

function getUserToken() {
  return getCookie("jwt");
}

function UserIsLoggedIn() {
  let token = getCookie(AuthTokenName);

  if (token) {
    const jwtData = jwtDecode(token);
    if (Date.now() >= jwtData["exp"] * 1000) {
      return false;
    }

    return true;
  }
  return false;
}

function TokenExpired() {
  let token = getCookie(AuthTokenName);

  if (!token) {
    // 只有在過期的時候可以refresh 根本沒有token的時候就是重新登入了
    return true;
  }
  const jwtUser = jwtDecode(token);
  let exp = jwtUser.exp;
  const currentDate = new Date();
  const timestamp = currentDate.getTime();
  return exp * 1000 < timestamp;
}

async function RefreshToken() {
  let token = getCookie("refresh");

  let data = { refresh: token };
  let res = await refreshToken(data);

  setToken(res.data.access, AuthTokenName);

  return res.data.access;
}

export { getUserToken, setRefreshToken, UserIsLoggedIn, TokenExpired, RefreshToken };
