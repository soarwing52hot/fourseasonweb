import { ImgFacebook } from "@/Style/images";
import { Grid } from "@mui/material";

export default function Footer() {
  return (
    <footer className="title-bar footer-container">
      <h2>聯絡我們</h2>
      <Grid container>
        <Grid item xs={12} sm={6}>
          <p>本會會址：新北市板橋區縣民大道三段187巷2號3樓</p>
          <p style={{ display: "inline-block" }}>
            <a href="https://www.shecodes.io/athena/1613-how-to-place-image-and-text-on-the-same-line-in-html" target="_blank" rel="noreferrer">
              <img src={ImgFacebook} height={20} width={20} />
              <span>四季溯溪之友</span>
            </a>
          </p>
          <br />
          <p style={{ display: "inline-block" }}>
            <a href="https://www.shecodes.io/athena/1613-how-to-place-image-and-text-on-the-same-line-in-html" target="_blank" rel="noreferrer">
              <img src={ImgFacebook} height={20} width={20} />
              <span>四季溯溪會員天地</span>
            </a>
          </p>
        </Grid>
        <Grid item xs={12} sm={6}>
          <p>
            理事長信箱：<a href="mailto: director@4season.org.tw">director@4season.org.tw</a>
          </p>
          <p>
            秘書長信箱：<a href="mailto: secretary@4season.org.tw">secretary@4season.org.tw</a>
          </p>
          <p>
            財務&入會申請信箱：<a href="mailto: financial@4season.org.tw">financial@4season.org.tw</a>
          </p>
          <p>四季溯溪統一編號：18434892</p>
        </Grid>
      </Grid>
    </footer>
  );
}
