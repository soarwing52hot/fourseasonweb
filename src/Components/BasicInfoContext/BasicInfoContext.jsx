import { createContext, useContext, useEffect, useState } from "react";
import { getActivitieStampList, getTypeOptions, getRequirementOptions } from "@/Services/Activities";
import { getAnnouncementTypes } from "@/Services/Announcement";

const BasicInfoContext = createContext({
  stampNameMap: null,
  stamps: [],
  requirementOptions: [],
  activityTypes: []
});

const useBasicInfo = () => useContext(BasicInfoContext);

export { useBasicInfo };

const BasicInfoProvider = ({ children }) => {
  const [stampNameMap, setStampNameMap] = useState(new Map());
  const [stamps, setStamps] = useState([]);
  const [activityTypes, setActivityTypes] = useState([]);
  const [requirementOptions, setRequirementOptions] = useState([]);
  const [announcementTypes, setAnnouncementTypes] = useState([]);

  useEffect(() => {
    Promise.all([getStampMap(), getTypes(), getRequirements(), getTypesOfAnnouncement()]);
  }, []);

  async function getStampMap() {
    let res = await getActivitieStampList();
    setStamps(res.data.results);
    let stampArray = res.data.results;
    let map = new Map();
    stampArray.forEach(e => map.set(e.id, e.name));
    setStampNameMap(map);
  }

  async function getTypes() {
    let res = await getTypeOptions();
    setActivityTypes(res.data);
  }

  async function getTypesOfAnnouncement() {
    let res = await getAnnouncementTypes();
    setAnnouncementTypes(res.data);
  }

  async function getRequirements() {
    let res = await getRequirementOptions();
    setRequirementOptions(res.data);
  }

  return <BasicInfoContext.Provider value={{ stampNameMap, requirementOptions, activityTypes, stamps, announcementTypes }}>{children}</BasicInfoContext.Provider>;
};

export default BasicInfoProvider;
