import { Grid, Card, CardActionArea, Typography, CardContent, Chip, Divider, CardHeader } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import { stampMap } from "@/Utilities/Maps";
import { useBasicInfo } from "@/Components/BasicInfoContext/BasicInfoContext";

export default function ActivityCard({ activity }) {
  ActivityCard.propTypes = { activity: PropTypes.object };
  const history = useNavigate();
  const { stampNameMap } = useBasicInfo();
  const stampNamesResult = calStampNamesResult();

  function calStampNamesResult() {
    if (!stampNameMap.size) {
      return [];
    }
    let stampNames = [];
    activity?.stamp?.forEach(stamp => {
      let name = stampNameMap.get(stamp);
      stampNames.push(name);
    });

    return stampNames;
  }

  const stampImage = e => (
    <Grid item key={`${activity.id}-${e}-box`} sx={{ m: 1 }} xs={1}>
      <img className="m-1" src={stampMap.get(e)} alt="stamp" key={`${activity.id}-${e}`} style={{ height: "40px" }} />
    </Grid>
  );

  function leaderInfo(e) {
    return (
      <Typography key={`${activity.id} ${e.membership_number}`} variant="p">
        {e.username}
      </Typography>
    );
  }

  function defineChip() {
    if (activity.register_available === true) {
      return <Chip color="success" label="活動可報名" />;
    }

    return <Chip color="primary" label="不開放報名" />;
  }

  return (
    <Grid item xs={1} md={1} sx={{ my: 2 }}>
      <Card style={{ height: "100%" }}>
        <CardActionArea onClick={() => history(`../Activity/Detail/${activity.id}`)}>
          <CardHeader title={<b>{activity.title}</b>} action={activity.is_emergency && <Chip color="error" label="緊急召集" sx={{ my: 1 }} />} />
          <CardContent>
            <Divider />
            <Grid container spacing={2}>
              {stampNamesResult.map(stampImage)}
            </Grid>
            <Grid container spacing={2} rowSpacing={1}>
              <Grid item xs={4}>
                <b>活動日期</b>
              </Grid>
              <Grid item xs={8}>
                {moment(activity.trip_date_start).format("YYYY-MM-DD")} ~ {moment(activity.trip_date_end).format("YYYY-MM-DD")}
              </Grid>
              <Grid item xs={4}>
                <b>活動狀態</b>
              </Grid>
              <Grid item xs={8}>
                {defineChip()}
              </Grid>
              <Grid item xs={4}>
                <b>領隊群</b>
              </Grid>
              <Grid item xs={8}>
                {activity.leaders.map(leaderInfo)}
              </Grid>
              <Grid item xs={4}>
                <b>活動簡介</b>
              </Grid>
              <Grid item xs={8}>
                {activity.content}
              </Grid>
            </Grid>
          </CardContent>
        </CardActionArea>
      </Card>
    </Grid>
  );
}
