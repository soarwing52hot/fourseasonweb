import { useCallback, useEffect } from "react";
import moment from "moment-timezone";
import PropTypes from "prop-types";
import { Grid } from "@mui/material";
import { DatePicker } from "@mui/x-date-pickers/DatePicker";

export default function SearchBar({ DateSearch, setDateSearch, searchAction }) {
  SearchBar.propTypes = {
    DateSearch: PropTypes.object,
    setDateSearch: PropTypes.func,
    searchAction: PropTypes.func
  };

  const setDate = useCallback(() => {
    let today = moment();
    let temp = { dateStart: today.format("YYYY-MM-DD"), dateEnd: today.format(`YYYY-12-31`) };
    setDateSearch(temp);
  }, []);

  useEffect(() => {
    setDate();
  }, []);

  useEffect(() => {
    searchAction();
  }, [DateSearch]);

  return (
    <Grid container spacing={2}>
      <Grid item xs={12} md={6} className="m-2">
        <DatePicker
          label="開始日期"
          value={moment(DateSearch?.dateStart)}
          slotProps={{ textField: { fullWidth: true } }}
          onChange={newValue =>
            setDateSearch(prevState => ({
              ...prevState,
              dateStart: newValue
            }))
          }
        />
      </Grid>
      <Grid item xs={12} md={6} className="m-2">
        <DatePicker
          label="結束日期"
          value={moment(DateSearch?.dateEnd)}
          slotProps={{ textField: { fullWidth: true } }}
          onChange={newValue =>
            setDateSearch(prevState => ({
              ...prevState,
              dateEnd: newValue
            }))
          }
        />
      </Grid>
    </Grid>
  );
}
