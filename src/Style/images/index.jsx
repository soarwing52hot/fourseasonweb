import ImgLogo from "./logo_512.png";
import ImgLogoLight from "./logo.png";
import ImgSecretary from "./Sec.png";
import ImgChairman from "./Chair.png";
import ImgFinance from "./Fin.png";
import ImgSecHelper from "./FinH.png";
import ImgProfile from "./profile.webp";
import ImgCover from "./Cover.webp";
import Img1 from "./Img1.png";
import Img2 from "./Img2.jpg";
import ImgFacebook from "./facebook.png";

export { ImgLogo, ImgLogoLight, ImgFinance, ImgSecretary, ImgChairman, ImgSecHelper, ImgProfile, ImgCover, Img1, Img2, ImgFacebook };
