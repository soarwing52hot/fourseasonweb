FROM node:lts as build

WORKDIR /app
COPY package*.json ./
RUN npm set strict-ssl false
RUN npm ci

COPY . .
RUN echo $(ls -1 /app/src)
RUN npm run build

FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf

CMD sed -i -e 's/$PORT/'"$PORT"'/g' /etc/nginx/conf.d/default.conf && nginx -g 'daemon off;'